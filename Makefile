UID := 1000
GID := 1000

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  install_php      > install php composer dependancies"
	@echo "  install_frontend > install frontend dependancies (js, css...)"
	@echo "  up               > build php image and start jdev-boarding containers for dev only (php + mailer)"
	@echo "  start            > start jdev-boarding containers"
	@echo "  restart          > restart jdev-boarding containers"
	@echo "  stop             > stop  and kill running jdev-boarding containers"
	@echo "  logs             > display jdev-boarding containers logs"
	@echo "  shell            > shell into php jdev-boarding container"
	@echo "  gendb            > generate the jdev-boarding database"
	@echo "  remove-pgdata    > remove the volume of database"
	@echo ""

install_php:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-e COMPOSER_CACHE_DIR=/dev/null \
	-v $(CURDIR):/project \
	-w /project jakzal/phpqa composer install --ignore-platform-reqs

install_frontend:
	@docker build -t jdev-bower conf-dev/npm && docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR):/project \
	-w /project jdev-bower bower install

up:
	@docker-compose up --build -d

start:
	@docker-compose up -d

restart: stop start

stop:
	@docker-compose kill
	@docker-compose rm -v --force

logs:
	@docker-compose logs -f -t

shell:
	@docker-compose exec php bash

gendb:
	@docker-compose exec php ./vendor/bin/doctrine orm:schema-tool:create

remove-pgdata:
	@docker volume rm jdev-boarding_pgdata
