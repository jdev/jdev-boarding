# JDEV BOARDING

## Introduction

Jdev-boarding est le logiciel utilisé pour les inscriptions et le suivi de l'évenement des JDEVs.

Le logiciel jdev-boarding est sous licence `CeCILL` (voir le fichier LICENCE à la racine du projet).

## Authors

Voici la liste des personnes ayant participées au projet jdev-boarding:

* `Chrystel Moreau` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)

# Guide d'installation

## Prérequis

Avant de vous lancer dans l'installation du logiciel jdev-boarding sur votre machine, vous devez vous assurer que les commandes suivantes sont installées sur votre ordinateur:

1. `make`
2. `docker`
3. `docker-compose`

Vous aurez également besoin d'une connexion Internet pour effectuer l'installation.

## La liste des commandes

Le fichier `Makefile` que vous pouvez trouver à la racine du projet contient l'ensemble des commandes disponibles et qui vous aiderons dans l'installation et le démarrage du logiciel.

Pour voir la liste des commandes possibles vous pouvez ouvrir un terminal et taper:

> make

**Attention**: Le fichier `docker-compose.yml` et les commandes du ` Makefile` ne doivent être utilisées que pour le dévloppement et non pas dans un mode production.

## Installation des dépendances

Pour effectuer l'installation des dépendances php et des dépendances `frontend` (js, css...), tapez:

> make install_php

L'outil `Composer.phar` va télécharger automatiquement les dépendances php qui sont listées dans le fichier `composer.json`. Les dépendances seront stockées dans un dossier `vendor` du projet.

> make install_frontend

L'outil `bower` va télécharger automatiquement les dépendances front (js, css) qui sont listées dans le fichier `bower.json`.
Les dépendandes seront stockées dans un dossier `public/bower_components` du projet.

Après cela jdev-boarding peut fonctionner.

## Lancement du jdev-boarding

Jdev-boarding est configuré pour fonctionner dans un environnement docker. Si vous ouvrez le fichier `docker-compose.yml` vous pouvez voir les conteneurs qui sont utilisés dans le mode développement.

`Makefile` fournit une liste de commandes pour builder et lancer les conteneurs:

> make up

**Note:** Cette commande utilise le fichier `docker-compose` pour fonctionner. Les opérations peuvent prendre quelques minutes car cela nécessite de télécharger des images sur le docker hub.

Vous pouvez lister les conteneurs qui tournent sur votre machine:

> docker-compose ps

Vous pouvez aussi afficher les logs des conteneurs lancés:

> make logs

## Accès

Vous poucez maintenant vous rendre sur votre navigateur pour afficher le boarding: 

> http://localhost:8080
