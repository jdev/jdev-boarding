<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => getenv('SLIM_DISPLAY_ERROR_DETAILS'),
        // View settings
        'view' => [
            'twig' => [
                'cache' => getenv('TWIG_CACHE'),
                'debug' => getenv('TWIG_DEBUG'),
                'auto_reload' => getenv('TWIG_AUTO_RELOAD'),
            ],
        ],
        // monolog settings
        'logger' => [
            'name' => getenv('LOGGER_NAME'),
            'path' => getenv('LOGGER_PATH'),
        ],
        // database settings (doctrine 2)
        'database' => [
            'path_proxy'  => getenv('DOCTRINE_PATH_PROXY'),
            'dev_mode'    => getenv('DOCTRINE_DEV_MODE'),
            'connection_options' => [
                'driver'   => getenv('DB_DRIVER'),
                'host'     => getenv('DB_HOST'), // cesamsidb (@LAM), db (@local)
                'user'     => getenv('DB_USER'),
                'password' => getenv('DB_PASSWORD'),
                'dbname'   => getenv('DB_DBNAME')  // jdevdb (dev)
            ]
        ],
        'mailer' => [
            'host' => getenv('MAILER_HOST'),  //localhost (dev), smtp.osupytheas.fr (prod)
            'port' => getenv('MAILER_PORT')  // 1025 (dev), 25 (prod)
        ],
        // JDEV settings
        'jdev' => [
            'label' => 'JDEV2020',
            'ville' => 'Rennes',
            'DR' => 'DR14',
            'url' => 'jdev2020.fr',
            'email' => 'jdev2020-inscription@services.cnrs.fr',
            'email_clo' => 'jdev2020-clo@services.cnrs.fr',
            'email_web' => 'chrystel.moreau@lam.fr,francois.agneray@lam.fr,jdev2020-inscription@services.cnrs.fr',
            'email_sponsor' => 'jdev2020-sponsor@services.cnrs.fr',
            'email_programme' => 'jdev2020-cp@services.cnrs.fr',
            'email_info' => 'jdev2020@services.cnrs.fr',
            'mailing_list' => 'https://listes.services.cnrs.fr/sympa/subscribe/jdev2020',
            'date_debut' => '2020-07-06',
            'date_fin' => '2020-07-10',
            'limite' => '2020-05-30',
            'ouverture' => '2020-02-24',            
            'inscription_j1' => '2020-02-29',
            'inscription_j2' => '2020-04-30',
            'inscription_j3' => '2020-06-23',
            'inscription_j4' => '2020-07-03',
            'j1' => '2020-07-06',
            'j2' => '2020-07-07',
            'j3' => '2020-07-08',
            'j4' => '2020-07-09',
            'j5' => '2020-07-10',
            // 0 version de base (sans inscription)
            // 1 version avec les inscriptions actives
            // 2 version avec le sondage actif
            // 3 version avec le parcours actif
            // 4 version durant les JDEVs
            // 5 apres les JDEVs
            'version' => 3
        ]
    ]
];
