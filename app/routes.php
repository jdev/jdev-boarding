<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// Routes
$app->get('/', App\Action\HomeAction::class)
    ->setName('homepage');

$app->get('/verif-email', App\Action\VerifEmailAction::class);

$app->get('/valid-email', App\Action\ValidEmailAction::class)
    ->add(App\Middleware\DatabaseAccessMiddleware::class);

$app->get('/programme', App\Action\ProgrammeAction::class)
    ->setName('programme');

$app->get('/contact', App\Action\ContactAction::class)
    ->setName('contact');

$app->get('/communication', App\Action\CommunicationAction::class)
    ->setName('communication');

$app->get('/info-acces', App\Action\InfoAccesAction::class)
    ->setName('info_acces');

$app->get('/info-hebergement', App\Action\InfoHebergementAction::class)
    ->setName('info_hebergement');

$app->get('/info-generales', App\Action\InfoGeneralesAction::class)
    ->setName('info_generales');

$app->map(['GET', 'POST'], '/inscription', App\Action\InscriptionAction::class)
    ->add(App\Middleware\DatabaseAccessMiddleware::class)
    ->setName('inscription');

$app->map(['GET', 'POST'],'/new-passwd', App\Action\NewPasswdAction::class)
    ->add(App\Middleware\DatabaseAccessMiddleware::class)
    ->setName('new_passwd');

$app->map(['GET', 'POST'],'/change-passwd', App\Action\ChangePasswdAction::class)
    ->add(App\Middleware\DatabaseAccessMiddleware::class)
    ->setName('change_passwd');

$app->map(['GET', 'POST'], '/connexion', App\Action\ConnexionAction::class)
    ->add(App\Middleware\DatabaseAccessMiddleware::class)
    ->setName('connexion');


// =============================================================
// Dashboard Generation Badge (Participant + Admin)
// =============================================================

$app->get('/dashboard-generate-badge', App\Action\DashboardGenerateBadgeAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('generate-badge');

$app->get('/dashboard-generate-badge-list', App\Action\DashboardGenerateBadgeListAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('generate-badge-list');


// =============================================================
// Dashboard Participant
// =============================================================

$app->get('/dashboard-home', App\Action\DashboardAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_home');

$app->get('/dashboard-participant-sondage', App\Action\DashboardParticipantSondageAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_sondage');

$app->map(['GET', 'POST'],'/dashboard-participant-sondage-formation', App\Action\DashboardParticipantSondageFormationAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_sondage_formation');

$app->get('/dashboard-participant-parcours', App\Action\DashboardParticipantParcoursAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_parcours');

$app->get('/dashboard-participant-sessions-refresh', App\Action\DashboardParticipantSessionsRefreshAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_sessions_refresh');

$app->map(['GET', 'POST'],'/dashboard-participant-parcours-formation', App\Action\DashboardParticipantParcoursFormationAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_parcours_formation');

$app->get('/dashboard-participant-agenda', App\Action\DashboardParticipantAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_agenda');

$app->map(['GET', 'POST'],'/dashboard-participant-presence', App\Action\DashboardParticipantPresenceAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_presence');

$app->map(['GET', 'POST'],'/dashboard-participant-edit-form', App\Action\DashboardParticipantEditFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_edit_form');

// =============================================================
// Dashboard Admin
// =============================================================

$app->get('/dashboard-admin', App\Action\DashboardAdminAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_home');

$app->get('/dashboard-admin-profil', App\Action\DashboardAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_profil');

$app->post('/admin-valid-role', App\Action\AdminValidRoleAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('admin_valid_role');

$app->get('/dashboard-admin-role-choix', App\Action\DashboardAdminRoleChoixAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('admin_role_choix');

$app->get('/dashboard-admin-inscription', App\Action\DashboardAdminInscriptionAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_inscription');

$app->map(['GET', 'POST'],'/dashboard-admin-emargement', App\Action\DashboardAdminEmargementAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_emargement');

$app->map(['GET', 'POST'],'/dashboard-admin-emargement-liste', App\Action\DashboardAdminEmargementListeAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_emargement-liste');

$app->get('/dashboard-admin-emargement-presence', App\Action\DashboardAdminEmargementPresenceAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_emargement_presence');


$app->get('/dashboard-admin-formation', App\Action\DashboardAdminFormationAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_formation');

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-formation-form', App\Action\DashboardAdminFormationFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_formation_form');

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-role-form', App\Action\DashboardAdminRoleFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_role_form');

$app->get('/dashboard-admin-preagenda', App\Action\DashboardAdminPreAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_preagenda');

$app->get('/dashboard-admin-sondage-stat', App\Action\DashboardAdminSondageStatAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_sondage_stat');

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-preagenda-form', App\Action\DashboardAdminPreAgendaFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_preagenda_form');

$app->get('/dashboard-admin-agenda', App\Action\DashboardAdminAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_agenda');

$app->get('/dashboard-admin-agenda-salle-choix', App\Action\DashboardAdminAgendaSalleChoixAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('admin_agenda_salle_choix');

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-agenda-form', App\Action\DashboardAdminAgendaFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_agenda_form');

$app->get('/dashboard-admin-organisme', App\Action\DashboardAdminOrganismeAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_organisme');

$app->get('/dashboard-admin-participant-gestion', App\Action\DashboardAdminParticipantGestionAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_participant_gestion');

$app->get('/dashboard-admin-participant-liste', App\Action\DashboardAdminParticipantListeAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_participant_liste');

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-participant-form', App\Action\DashboardAdminParticipantFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_participant_form');

$app->get('/dashboard-admin-participant-parcours', App\Action\DashboardAdminParticipantParcoursAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_participant_parcours');

$app->get('/dashboard-admin-paiement', App\Action\DashboardAdminPaiementAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_paiement');

$app->post('/admin-valid-paiement', App\Action\AdminValidPaiementAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('admin_valid_paiement');

$app->get('/dashboard-admin-salle', App\Action\DashboardAdminSalleAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_salle');

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-add-edit-salle', App\Action\DashboardAdminAddEditSalleAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_add_edit_salle');


$app->get('/dashboard-admin-thematique', App\Action\DashboardAdminThematiqueAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_thematique');


$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-thematique-form', App\Action\DashboardAdminThematiqueFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_thematique_form');


$app->get('/dashboard-admin-thematique-preagenda', App\Action\DashboardAdminThematiquePreAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_thematique-preagenda');

$app->get('/dashboard-admin-thematique-agenda', App\Action\DashboardAdminThematiqueAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_thematique-agenda');


$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/dashboard-admin-organisme-form', App\Action\DashboardAdminOrganismeFormAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_organisme_form');

$app->get('/dashboard-admin-session-preagenda', App\Action\DashboardAdminSessionPreAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_session-preagenda');

$app->get('/dashboard-admin-session-agenda', App\Action\DashboardAdminSessionAgendaAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_session-agenda');

$app->get('/dashboard-admin-annuler-session', App\Action\DashboardAdminAnnulerSessionAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_annuler-session');

$app->map(['GET', 'POST'],'/dashboard-admin-participant-parcours-formation', App\Action\DashboardAdminParticipantParcoursFormationAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_participant_parcours_formation');

$app->map(['GET', 'POST'],'/dashboard-admin-gen-code-formation', App\Action\DashboardAdminGenCodeFormationAction::class)
    ->add(App\Middleware\TokenMiddleware::class)
    ->setName('dashboard_admin_gen_code_formation');
