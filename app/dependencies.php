<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// DIC configuration
$container = $app->getContainer();

// Twig
$container['view'] = function ($c) {
    $settings = $c->get('settings');
    $view = new Slim\Views\Twig(__DIR__ . '/templates', $settings['view']['twig']);
    // Add extensions
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
    $view->addExtension(new Twig\Extension\DebugExtension());
    return $view;
};

// Doctrine 2 Entity Manager
$container['em'] = function ($c) {
    $settings = $c->get('settings');
    $metadata = $settings['database'];
    $c = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
        array('app/src/Entity'),
        $metadata['dev_mode']
    );
    $c->setProxyDir($metadata['path_proxy']);
    if ($metadata['dev_mode']) {
        $c->setAutoGenerateProxyClasses(true);
    } else {
        $c->setAutoGenerateProxyClasses(false);
    }
    return \Doctrine\ORM\EntityManager::create($metadata['connection_options'], $c);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['logger']['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// SwiftMailer
$container['mailer'] = function ($c) {
    $settings = $c->get('settings');
    $transport = \Swift_SmtpTransport::newInstance($settings['mailer']['host'], $settings['mailer']['port']);
    return \Swift_Mailer::newInstance($transport);
};

// -----------------------------------------------------------------------------
// Middleware factories
// -----------------------------------------------------------------------------

$container['App\Middleware\TokenMiddleware'] = function ($c) {
    return new App\Middleware\TokenMiddleware($c->get('logger'), $c->get('settings'));
};

$container['App\Middleware\DatabaseAccessMiddleware'] = function ($c) {
    return new App\Middleware\DatabaseAccessMiddleware($c->get('logger'), $c->get('em'));
};
