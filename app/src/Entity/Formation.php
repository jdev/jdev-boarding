<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="formation")
 */
class Formation
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @Column(type="string", unique=true)
     **/
    private $nom;
    
    /**
     * @ManyToOne(targetEntity="Thematique")
     * @JoinColumn(name="id_thematique", referencedColumnName="id")
     */
    private $thematique;
    
    /**
     * @Column(type="string")
     **/
    private $type;
    
    /**
     * @Column(type="string", nullable=true)
     **/
    private $titre;
    
    /**
     * @Column(type="integer", nullable=true)
     **/
    private $nb_intervenant;
    
    /**
     * @Column(type="string")
     **/
    private $intervenant;
    
    /**
     * @Column(type="integer")
     **/
    private $quota;

    /**
     * @Column(type="boolean", name="confirme")
     **/
    private $confirme;
    
    /**
     * @OneToMany(targetEntity="Agenda", mappedBy="formation")
     */
    private $agendas;
    
    /**
     * @OneToMany(targetEntity="PreAgenda", mappedBy="formation")
     */
    private $preagendas;
    
    /**
     * @OneToMany(targetEntity="ParticipantSondage", mappedBy="formation")
     */
    private $participants;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getNom()
    {
        return $this->nom;
    }
    
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    
    public function getThematique()
    {
        return $this->thematique;
    }
    
    public function setThematique(Thematique $thematique)
    {
        $this->thematique = $thematique;
    }
    
    public function getType()
    {
        return $this->type;
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    
    public function getTitre()
    {
        return $this->titre;
    }
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }
    
    public function getNbIntervenant()
    {
        return $this->nb_intervenant;
    }
    
    public function setNbIntervenant($nb_intervenant)
    {
        $this->nb_intervenant = $nb_intervenant;
    }

    public function getIntervenant()
    {
        return $this->intervenant;
    }
    
    public function setIntervenant($intervenant)
    {
        $this->intervenant = $intervenant;
    }

    public function getQuota()
    {
        return $this->quota;
    }
    
    public function setQuota($quota)
    {
        $this->quota = $quota;
    }
    
    public function getConfirme()
    {
        return $this->confirme;
    }

    public function setConfirme($confirme)
    {
        $this->confirme = $confirme;
    }
    
    public function getAgendas()
    {
        return $this->agendas;
    }
    
    public function getPreAgendas()
    {
        return $this->preagendas;
    }
    
    public function getParticipants()
    {
        return $this->participants;
    }
}
