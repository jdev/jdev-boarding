<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="salle")
 */
class Salle
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @Column(type="string")
     **/
    private $nom;
    
    /**
     * @Column(type="integer")
     **/
    private $etage;
    
    /**
     * @Column(type="string")
     **/
    private $aile;
    
    /**
     * @Column(type="string")
     **/
    private $wifi;
   
    /**
     * @Column(type="integer")
     **/
    private $reseau;
    
    /**
     * @Column(type="boolean")
     **/
    private $videoprojecteur;
    /**
     * @Column(type="string")
     **/
    private $description;
    
    /**
     * @Column(type="integer", name="quota_officiel")
     **/
    private $quotaOfficiel;
    
    /**
     * @Column(type="integer", name="quota_physique")
     **/
    private $quotaPhysique;
    
    
    /**
     * @OneToMany(targetEntity="Agenda", mappedBy="salle")
     */
    private $agendas;
    
    /**
     * @OneToMany(targetEntity="PreAgenda", mappedBy="salle")
     */
    private $preagendas;
    
        
    public function getId()
    {
        return $this->id;
    }
    
    public function getNom()
    {
        return $this->nom;
    }
    
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    
    public function getEtage()
    {
        return $this->etage;
    }
    
    public function setEtage($etage)
    {
        $this->etage = $etage;
    }
    
    public function getAile()
    {
        return $this->aile;
    }
    
    public function setAile($aile)
    {
        $this->aile = $aile;
    }
    
    public function getWifi()
    {
        return $this->wifi;
    }
    
    public function setWifi($wifi)
    {
        $this->wifi = $wifi;
    }
    
    public function getReseau()
    {
        return $this->reseau;
    }
    
    public function setReseau($reseau)
    {
        $this->reseau = $reseau;
    }
    
    public function getVideoprojecteur()
    {
        return $this->videoprojecteur;
    }
    
    public function setVideoprojecteur($videoprojecteur)
    {
        $this->videoprojecteur = $videoprojecteur;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getQuotaOfficiel()
    {
        return $this->quotaOfficiel;
    }
    
    public function setQuotaOfficiel($quotaOfficiel)
    {
        $this->quotaOfficiel = $quotaOfficiel;
    }
    
    public function getQuotaPhysique()
    {
        return $this->quotaPhysique;
    }
    
    public function setQuotaPhysique($quotaPhysique)
    {
        return $this->quotaPhysique = $quotaPhysique;
    }
    
    public function getAgendas()
    {
        return $this->agendas;
    }
    
    public function getPreAgendas()
    {
        return $this->preagendas;
    }
}
