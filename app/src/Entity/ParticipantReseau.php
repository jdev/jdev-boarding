<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="participant_x_reseau")
 */
class ParticipantReseau
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Participant", inversedBy="reseaux")
     * @JoinColumn(name="id_participant", referencedColumnName="id")
     */
    private $participant;
    
    /**
     * @ManyToOne(targetEntity="Reseau")
     * @JoinColumn(name="id_reseau", referencedColumnName="id")
     */
    private $reseau;

    public function getId()
    {
        return $this->id;
    }

    public function getParticipant()
    {
        return $this->participant;
    }

    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }

    public function getReseau()
    {
        return $this->reseau;
    }

    public function setReseau($reseau)
    {
        $this->reseau = $reseau;
    }
}
