<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="participant")
 */
class Participant
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @Column(type="string")
     **/
    private $nom;

    /**
     * @Column(type="string")
     **/
    private $prenom;
    
    /**
     * @Column(type="string")
     **/
    private $email;
    
    /**
     * @Column(type="string")
     **/
    private $password;
    
    /**
     * @Column(type="string")
     **/
    private $statut;

    /**
     * @Column(type="string")
     **/
    private $emploi;

    /**
     * @Column(type="string")
     **/
    private $unite;

    /**
     * @Column(type="string")
     **/
    private $role;

    /**
     * @Column(type="string")
     **/
    private $region;

    /**
     * @Column(type="string", name="type_inscription")
     **/
    private $typeInscription;

    /**
     * @Column(type="string", name="regime_alimentaire", nullable=true)
     **/
    private $regimeAlimentaire;

    /**
     * @Column(type="boolean", name="mobilite_reduite", nullable=true)
     **/
    private $mobiliteReduite;

    /**
     * @Column(type="boolean", name="code_wifi", nullable=true)
     **/
    private $codeWifi;

    /**
     * @Column(type="boolean", name="mailing_list")
     **/
    private $mailingList;

    /**
     * @Column(type="boolean", name="evenement_social")
     **/
    private $evenementSocial;

    /**
     * @Column(type="datetime", name="date_inscription")
     **/
    private $dateInscription;

    /**
     * @Column(type="datetime", name="date_activation", nullable=true)
     **/
    private $dateActivation;

    /**
     * @Column(type="string", name="cle_email")
     **/
    private $cleEmail;

    /**
     * @Column(type="boolean", name="email_valide")
     **/
    private $emailValide;

    /**
     * @Column(type="boolean", name="clo_valide")
     **/
    private $cloValide;

    /**
     * @Column(type="boolean", name="acces_valide")
     **/
    private $accesValide;

    /**
     * @Column(type="boolean", name="pass_prepaye")
     **/
    private $passPrepaye;

    /**
     * @Column(type="boolean", name="present", nullable=true)
     **/
    private $present;
    
    /**
     * @Column(type="string", name="role_si", nullable=true)
     */
    private $roleSI;

    /**
     * @Column(type="string", name="role_orga", nullable=true)
     * @ManyToOne(targetEntity="Role")
     * @JoinColumn(name="id_role", referencedColumnName="id")
     */
    private $roleOrga;
    
    /**
     * @ManyToOne(targetEntity="Organisme")
     * @JoinColumn(name="id_organisme", referencedColumnName="id")
     */
    private $organisme;

    /**
     * @OneToMany(targetEntity="ParticipantReseau", mappedBy="participant")
     */
    private $reseaux;

    /**
     * @OneToMany(targetEntity="ParticipantCommunaute", mappedBy="participant")
     */
    private $communautes;

    /**
     * @OneToMany(targetEntity="ParticipantContribution", mappedBy="participant")
     */
    private $contributions;

    /**
     * @OneToMany(targetEntity="ParticipantPreAgenda", mappedBy="participant")
     */
    private $preagendas;

    /**
     * @OneToMany(targetEntity="ParticipantAgenda", mappedBy="participant")
     */
    private $agendas;

    /**
     * @OneToMany(targetEntity="ParticipantSondage", mappedBy="participant")
     */
    private $sondages;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }
    
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    
    public function getPrenom()
    {
        return $this->prenom;
    }
    
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function getPassword()
    {
        return $this->password;
    }
    
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getStatut()
    {
        return $this->statut;
    }

    public function setStatut($statut)
    {
        $this->statut = $statut;
    }
    
    public function getEmploi()
    {
        return $this->emploi;
    }

    public function setEmploi($emploi)
    {
        $this->emploi = $emploi;
    }

    public function getUnite()
    {
        return $this->unite;
    }

    public function setUnite($unite)
    {
        $this->unite = $unite;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;
    }

    public function getTypeInscription()
    {
        return $this->typeInscription;
    }

    public function setTypeInscription($typeInscription)
    {
        $this->typeInscription = $typeInscription;
    }

    public function getRegimeAlimentaire()
    {
        return $this->regimeAlimentaire;
    }

    public function setRegimeAlimentaire($regimeAlimentaire)
    {
        $this->regimeAlimentaire = $regimeAlimentaire;
    }

    public function getMobiliteReduite()
    {
        return $this->mobiliteReduite;
    }

    public function setMobiliteReduite($mobiliteReduite)
    {
        $this->mobiliteReduite = $mobiliteReduite;
    }

    public function getCodeWifi()
    {
        return $this->codeWifi;
    }

    public function setCodeWifi($codeWifi)
    {
        $this->codeWifi = $codeWifi;
    }

    public function getMailingList()
    {
        return $this->mailingList;
    }

    public function setMailingList($mailingList)
    {
        $this->mailingList = $mailingList;
    }

    public function getEvenementSocial()
    {
        return $this->evenementSocial;
    }

    public function setEvenementSocial($evenementSocial)
    {
        $this->evenementSocial = $evenementSocial;
    }

    public function getDateInscription()
    {
        return $this->dateInscription;
    }
    
    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;
    }

    public function getDateActivation()
    {
        return $this->dateActivation;
    }
    
    public function setDateActivation($dateActivation)
    {
        $this->dateActivation = $dateActivation;
    }

    public function getCleEmail()
    {
        return $this->cleEmail;
    }

    public function setCleEmail($cleEmail)
    {
        $this->cleEmail = $cleEmail;
    }

    public function getEmailValide()
    {
        return $this->emailValide;
    }

    public function setEmailValide($emailValide)
    {
        $this->emailValide = $emailValide;
    }

    public function getCloValide()
    {
        return $this->cloValide;
    }

    public function setCloValide($cloValide)
    {
        $this->cloValide = $cloValide;
    }
    
    public function getAccesValide()
    {
        return $this->accesValide;
    }

    public function setAccesValide($accesValide)
    {
        $this->accesValide = $accesValide;
    }

    public function getPassPrepaye()
    {
        return $this->passPrepaye;
    }

    public function setPassPrepaye($passPrepaye)
    {
        $this->passPrepaye = $passPrepaye;
    }

    public function getPresent()
    {
        return $this->present;
    }

    public function setPresent($present)
    {
        $this->present = $present;
    }

    public function getRoleSI()
    {
        return $this->roleSI;
    }

    public function setRoleSI($roleSI)
    {
        $this->roleSI = $roleSI;
    }

    public function getRoleOrga()
    {
        return $this->roleOrga;
    }

    public function setRoleOrga($roleOrga)
    {
        $this->roleOrga = $roleOrga;
    }

    
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getReseaux()
    {
        return $this->reseaux;
    }

    public function getCommunautes()
    {
        return $this->communautes;
    }

    public function getContributions()
    {
        return $this->contributions;
    }
        
    public function getRoles()
    {
        return $this->roles;
    }

    public function getPreAgendas()
    {
        return $this->preagendas;
    }
    
    public function getAgendas()
    {
        return $this->agendas;
    }
    
    public function getSondages()
    {
        return $this->sondages;
    }
}
