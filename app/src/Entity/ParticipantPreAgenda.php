<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="participant_x_preagenda")
 */
class ParticipantPreAgenda
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Participant", inversedBy="preagendas")
     * @JoinColumn(name="id_participant", referencedColumnName="id")
     */
    
    private $participant;
    
    /**
     * @ManyToOne(targetEntity="PreAgenda", inversedBy="participants")
     * @JoinColumn(name="id_agenda", referencedColumnName="id")
     */
    private $preagenda;
    
    /**
     * @Column(type="datetime", name="date_inscription")
     **/
    private $dateInscription;
    
    
    //public function __construct(Participant $participant, PreAgenda $preagenda)
    //{
    //    $this->participant = $participant;
    //    $this->agenda = $agenda;
    //}
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getParticipant()
    {
        return $this->participant;
    }

    public function setParticipant(Participant $participant)
    {
        $this->participant = $participant;
    }
    
    public function getPreAgenda()
    {
        return $this->preagenda;
    }
    
    public function setPreAgenda(PreAgenda $preagenda)
    {
        $this->preagenda = $preagenda;
    }

    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;
    }
}
