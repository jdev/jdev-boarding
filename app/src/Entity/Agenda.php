<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="agenda")
 */
class Agenda implements \JsonSerializable
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
        
    /**
     * @Column(type="integer", name="display", nullable=true)
     */
    private $display;
    
    /**
     * @Column(type="datetime", name="date_debut")
     **/
    private $dateDebut;
    
    /**
     * @Column(type="datetime", name="date_fin")
     **/
    private $dateFin;

    /**
     * @Column(type="string", name="code_formation", nullable=true)
     **/
    private $codeFormation;
    
    /**
     * @ManyToOne(targetEntity="Formation", inversedBy="agendas")
     * @JoinColumn(name="id_formation", referencedColumnName="id")
     */
    private $formation;
    
    /**
     * @ManyToOne(targetEntity="Salle", inversedBy="agendas")
     * @JoinColumn(name="id_salle", referencedColumnName="id")
     */
    private $salle;
    
    
    /**
     * @OneToMany(targetEntity="ParticipantAgenda", mappedBy="agenda")
     */
    private $participants;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getDisplay()
    {
        return $this->display;
    }
        
    public function setDisplay($display)
    {
        $this->display = $display;
    }
        
    public function getDateDebut()
    {
        return $this->dateDebut;
    }
    
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }
    
    public function getDateFin()
    {
        return $this->dateFin;
    }
    
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    public function getCodeFormation()
    {
        return $this->codeFormation;
    }

    public function setCodeFormation($codeFormation)
    {
        $this->codeFormation = $codeFormation;
    }
    
    public function getFormation()
    {
        return $this->formation;
    }
    
    public function setFormation(Formation $formation)
    {
        $this->formation = $formation;
    }
    
    public function getSalle()
    {
        return $this->salle;
    }
    
    public function setSalle(Salle $salle)
    {
        return $this->salle = $salle;
    }
    
    public function getParticipants()
    {
        return $this->participants;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'date_formation' => $this->dateDebut->format('d M Y A'),
            'debut_formation' => $this->dateDebut->format('H:i:s'),
            'fin_formation' => $this->dateFin->format('H:i:s'),
            'nom_formation' => $this->formation->getNom(),
            'quota_formation' => $this->formation->getQuota(),
            'salle' => $this->salle->getNom()
        );
    }
}
