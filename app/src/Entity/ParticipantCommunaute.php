<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="participant_x_communaute")
 */
class ParticipantCommunaute
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Participant", inversedBy="communautes")
     * @JoinColumn(name="id_participant", referencedColumnName="id")
     */
    private $participant;
    
    /**
     * @ManyToOne(targetEntity="Communaute")
     * @JoinColumn(name="id_communaute", referencedColumnName="id")
     */
    private $communaute;

    public function getId()
    {
        return $this->id;
    }

    public function getParticipant()
    {
        return $this->participant;
    }

    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }

    public function getCommunaute()
    {
        return $this->communaute;
    }

    public function setCommunaute($communaute)
    {
        $this->communaute = $communaute;
    }
}
