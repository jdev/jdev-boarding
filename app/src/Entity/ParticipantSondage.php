<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="participant_x_formation")
 */
class ParticipantSondage
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Participant", inversedBy="sondages")
     * @JoinColumn(name="id_participant", referencedColumnName="id")
     */
    private $participant;
    
    /**
     * @ManyToOne(targetEntity="Formation", inversedBy="participants")
     * @JoinColumn(name="id_formation", referencedColumnName="id")
     */
    private $formation;
    
    /**
     * @Column(type="datetime", name="date_inscription")
     **/
    private $dateInscription;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getParticipant()
    {
        return $this->participant;
    }

    public function setParticipant(Participant $participant)
    {
        $this->participant = $participant;
    }
    
    public function getFormation()
    {
        return $this->formation;
    }
    
    public function setFormation(Formation $formation)
    {
        $this->formation = $formation;
    }

    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;
    }
}
