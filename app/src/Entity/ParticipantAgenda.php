<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="participant_x_agenda")
 */
class ParticipantAgenda
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Participant", inversedBy="agendas")
     * @JoinColumn(name="id_participant", referencedColumnName="id")
     */
    private $participant;
    
    /**
     * @ManyToOne(targetEntity="Agenda", inversedBy="participants")
     * @JoinColumn(name="id_agenda", referencedColumnName="id")
     */
    private $agenda;
    
    /**
     * @Column(type="datetime", name="date_inscription")
     **/
    private $dateInscription;
    
    /**
     * @Column(type="boolean", name="present", nullable=true)
     **/
    private $present;
    
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getParticipant()
    {
        return $this->participant;
    }

    public function setParticipant(Participant $participant)
    {
        $this->participant = $participant;
    }
    
    public function getAgenda()
    {
        return $this->agenda;
    }
    
    public function setAgenda(Agenda $agenda)
    {
        $this->agenda = $agenda;
    }

    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;
    }
    
    public function getPresent()
    {
        return $this->present;
    }

    public function setPresent($present)
    {
        $this->present = $present;
    }
}
