<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity;

/**
 * @Entity
 * @Table(name="preagenda")
 */
class PreAgenda
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    
    /**
     * @Column(type="integer", name="display", nullable=true)
     */
    private $display;
    
    /**
     * @Column(type="datetime", name="date_debut")
     **/
    private $dateDebut;
    
    /**
     * @Column(type="datetime", name="date_fin")
     **/
    private $dateFin;
    
    /**
     * @ManyToOne(targetEntity="Formation", inversedBy="preagendas")
     * @JoinColumn(name="id_formation", referencedColumnName="id")
     */
    private $formation;
    
    /**
     * @ManyToOne(targetEntity="Salle", inversedBy="preagendas")
     * @JoinColumn(name="id_salle", referencedColumnName="id")
     */
    private $salle;
    
    /**
     * @OneToMany(targetEntity="ParticipantPreAgenda", mappedBy="preagenda")
     */
    private $participants;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getDisplay()
    {
        return $this->display;
    }
        
    public function setDisplay($display)
    {
        $this->display = $display;
    }
    
    public function getDateDebut()
    {
        return $this->dateDebut;
    }
    
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }
    
    public function getDateFin()
    {
        return $this->dateFin;
    }
    
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }
    
    public function getFormation()
    {
        return $this->formation;
    }
    
    public function setFormation(Formation $formation)
    {
        $this->formation = $formation;
    }
    
    public function getSalle()
    {
        return $this->salle;
    }
    
    public function setSalle(Salle $salle)
    {
        return $this->salle = $salle;
    }
    
    public function getParticipants()
    {
        return $this->participants;
    }
}
