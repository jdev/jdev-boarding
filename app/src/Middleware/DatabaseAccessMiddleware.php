<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Middleware;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class DatabaseAccessMiddleware
{
    private $logger;
    private $em;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $next)
    {
        $this->logger->info("Database access middleware dispatched");

        try {
            $dql = "SELECT o FROM App\Entity\Organisme o";
            $query = $this->em->createQuery($dql);
            $query->getResult();
        } catch (Exception $e) {
             $this->logger->alert('Database connection problem !');
        }

        $response = $next($request, $response);
        return $response;
    }
}
