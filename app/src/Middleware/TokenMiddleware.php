<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Middleware;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class TokenMiddleware
{
    private $logger;
    private $settings;

    public function __construct(LoggerInterface $logger, $settings)
    {
        $this->logger = $logger;
        $this->settings = $settings;
    }
    public function __invoke(Request $request, Response $response, $next)
    {
        $this->logger->info("Token middleware dispatched");

        // Récupération du token string dans le param token de l'url' de la requete HTTP
        $params = $request->getQueryParams();
        if (!isset($params['token'])) {
            return $response->withStatus(401);
        }
        
        // Conversion string vers objet
        $token = (new Parser())->parse((string) $params['token']);

        // Validation du token
        $data = new ValidationData();
        $data->setIssuer($this->settings['jdev']['url']);
        $data->setAudience($this->settings['jdev']['url']);
        if (!$token->validate($data)) {
            return $response->withStatus(401);
        }

        // Verification de la signature du token
        $signer = new Sha256();
        if (!$token->verify($signer, 'testing')) {
            return $response->withStatus(401);
        }

        $response = $next(
            $request->withAttribute('email', $token->getClaim('email'))
                    ->withAttribute('roleSI', $token->getClaim('roleSI')),
            $response
        );
        return $response;
    }
}
