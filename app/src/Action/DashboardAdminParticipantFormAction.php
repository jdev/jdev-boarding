<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminParticipantFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if (($roleSI != 'admin') && ($roleSI != 'clo')) {
            return $response->withStatus(401);
        }

        if ($request->isDelete()) {
            $participant = $this->em->find('App\Entity\Participant', $params['id']);
            $this->deleteAllSondages($participant);
            $this->deleteAllPreAgendas($participant);
            $this->deleteAllAgendas($participant);
            $this->deleteAllContributions($participant);
            $this->deleteAllReseaux($participant);
            $this->deleteAllCommunautes($participant);
            $this->em->remove($participant);
            $this->em->flush();
        }
    
        if ($request->isGet()) {
            $organismesRepository = $this->em->getRepository('App\Entity\Organisme');
            $organismes = $organismesRepository->findAll();

            $reseauxRepository = $this->em->getRepository('App\Entity\Reseau');
            $reseaux1 = $reseauxRepository->findBy(array('display' => 1));
            $reseaux2 = $reseauxRepository->findBy(array('display' => 2));
            $reseaux3 = $reseauxRepository->findBy(array('display' => 3));

            $communauteRepository = $this->em->getRepository('App\Entity\Communaute');
            $communaute1 = $communauteRepository->findBy(array('display' => 1));
            $communaute2 = $communauteRepository->findBy(array('display' => 2));
            $communaute3 = $communauteRepository->findBy(array('display' => 3));

            $contributionsRepository = $this->em->getRepository('App\Entity\Contribution');
            $contributions = $contributionsRepository->findAll();
            
            $a = [
                'token' => $token,
                'role_si' => $roleSI,
                'jdev' => $this->settings['jdev'],
                'organismes' => $organismes,
                'reseaux1' => $reseaux1,
                'reseaux2' => $reseaux2,
                'reseaux3' => $reseaux3,
                'communaute1' => $communaute1,
                'communaute2' => $communaute2,
                'communaute3' => $communaute3,
                'contributions' => $contributions
            ];
            
            if (array_key_exists('id', $params)) {
                $participant = $this->em->find('App\Entity\Participant', $params['id']);
                $a['participant'] = $participant;
            } else {
                return $response->withStatus(400);
            }

            if (array_key_exists('present', $params)) {
                $valid = $params['present'];
                if ($valid == 'true') {
                    $participant->setPresent(true);
                }
                if ($valid == 'false') {
                    $participant->setPresent(false);
                }
                $this->em->flush();
                $this->view->render($response, 'dashboard_admin_participant_gestion.twig', $a);
            } else {
                $this->view->render($response, 'dashboard_admin_participant_form.twig', $a);
            }
        }
    
        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();
            $participant = $this->em->find('App\Entity\Participant', $parsedBody['participant_id']);
            $participant->setNom($parsedBody['nom']);
            $participant->setPrenom($parsedBody['prenom']);
            $participant->setStatut($parsedBody['statut']);
            $participant->setEmploi($parsedBody['emploi']);
            $this->setRole($participant, $parsedBody);
            $organisme = $this->em->find('App\Entity\Organisme', $parsedBody['organisme']);
            $this->setOrganisme($participant, $organisme);
            $participant->setUnite($parsedBody['unite']);
            $participant->setRegion($parsedBody['region']);
            $participant->setRegimeAlimentaire($parsedBody['regime_alimentaire']);
            // Validation de l'adresse email du participant
            if (isset($parsedBody['email_valide']) && $parsedBody['email_valide'] == 'on') {
                $participant->setEmailValide(true);
            }
            // Si le participant est une personne à mobilité reduite
            if (isset($parsedBody['mobilite_reduite']) && $parsedBody['mobilite_reduite'] == 'on') {
                $participant->setMobiliteReduite(true);
            } else {
                $participant->setMobiliteReduite(false);
            }
            // Si le participant demande un code wi-fi
            if (isset($parsedBody['acces_wifi']) && $parsedBody['acces_wifi'] == 'on') {
                $participant->setCodeWifi(true);
            } else {
                $participant->setCodeWifi(false);
            }
            if (isset($parsedBody['mailing_list']) && $parsedBody['mailing_list'] == 'on') {
                $participant->setMailingList(true);
            } else {
                $participant->setMailingList(false);
            }
            if (isset($parsedBody['evenement_social']) && $parsedBody['evenement_social'] == 'on') {
                $participant->setEvenementSocial(true);
            } else {
                $participant->setEvenementSocial(false);
            }
            
            $this->deleteAllContributions($participant);
            $this->createContributions($parsedBody, $participant);
            $this->deleteAllReseaux($participant);
            $this->createReseaux($parsedBody, $participant);
            $this->deleteAllCommunautes($participant);
            $this->createCommunautes($parsedBody, $participant);
            $this->em->flush();
            $response = $response->write('Participant id=' . $participant->getId() . ' modifié')->withStatus(201);
        }
        
        return $response;
    }

    private function setOrganisme($participant, $organisme)
    {
        if ($participant->getOrganisme()->getId() != $organisme->getId()) {
            if ($participant->getPassPrepaye()) {
                $participant->setPassPrepaye(false);
                $pOrganisme = $this->em->find('App\Entity\Organisme', $participant->getOrganisme()->getId());
                $pOrganisme->setNbLibres($pOrganisme->getNbLibres() + 1);
            }

            if ($organisme->getNbLibres() > 0 && $participant->getRole() == 'Participant') {
                $participant->setPassPrepaye(true);  // Le paiement à été pris en charge par l'organisme
                $organisme->setNbLibres($organisme->getNbLibres() - 1);
            }

            $participant->setOrganisme($organisme);
        }
    }

    private function setRole($participant, $parsedBody)
    {
        if ($participant->getRole() != $parsedBody['role']) {
            if ($parsedBody['role'] == 'Participant' || $parsedBody['role'] == 'Accompagnant') {
                $participant->setCloValide(true); // True si participant ou accompagnant
            } else {
                $participant->setCloValide(false); // False sinon; Besoin d'une activation d'un admin
            }
            $participant->setRole($parsedBody['role']);
        }
    }

    private function deleteAllSondages($participant)
    {
        foreach ($participant->getSondages() as $sondage) {
            $this->em->remove($sondage);
        }
        $this->em->flush();
    }
    
    private function deleteAllAgendas($participant)
    {
        foreach ($participant->getAgendas() as $agenda) {
            $this->em->remove($agenda);
        }
        $this->em->flush();
    }
    
    private function deleteAllPreAgendas($participant)
    {
        foreach ($participant->getPreAgendas() as $preagenda) {
            $this->em->remove($preagenda);
        }
        $this->em->flush();
    }
        
    
    private function deleteAllContributions($participant)
    {
        foreach ($participant->getContributions() as $contribution) {
            $this->em->remove($contribution);
        }
        $this->em->flush();
    }
    
    private function createContributions($parsedBody, $participant)
    {
        for ($i = 1; $i <= 7; $i++) {
            if (isset($parsedBody['contribution_' . $i]) && $parsedBody['contribution_' . $i] == 'on') {
                $contribution = $this->em->find('App\Entity\Contribution', $i);
                $participantContribution = new \App\Entity\ParticipantContribution();
                $participantContribution->setParticipant($participant);
                $participantContribution->setContribution($contribution);
                $this->em->persist($participantContribution);
            }
        }
    }
    
    private function deleteAllReseaux($participant)
    {
        foreach ($participant->getReseaux() as $reseau) {
            $this->em->remove($reseau);
        }
        $this->em->flush();
    }

    private function createReseaux($parsedBody, $participant)
    {
        for ($i = 1; $i <= 39; $i++) {
            if (isset($parsedBody['reseau_' . $i]) && $parsedBody['reseau_' . $i] == 'on') {
                $reseau = $this->em->find('App\Entity\Reseau', $i);
                $participantReseau = new \App\Entity\ParticipantReseau();
                $participantReseau->setParticipant($participant);
                $participantReseau->setReseau($reseau);
                $this->em->persist($participantReseau);
            }
        }
    }

    private function deleteAllCommunautes($participant)
    {
        foreach ($participant->getCommunautes() as $communaute) {
            $this->em->remove($communaute);
        }
        $this->em->flush();
    }

    private function createCommunautes($parsedBody, $participant)
    {
        for ($i = 1; $i <= 16; $i++) {
            if (isset($parsedBody['communaute_' . $i]) && $parsedBody['communaute_' . $i] == 'on') {
                $communaute = $this->em->find('App\Entity\Communaute', $i);
                $participantCommunaute = new \App\Entity\ParticipantCommunaute();
                $participantCommunaute->setParticipant($participant);
                $participantCommunaute->setCommunaute($communaute);
                $this->em->persist($participantCommunaute);
            }
        }
    }
}
