<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminParticipantListeAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        $role = $params['role'];
        
        if (($roleSI != 'clo') && ($roleSI != 'admin') && ($roleSI != 'clo_admin')) {
            return $response->withStatus(401);
        }

        $participants = $this->getParticipants($role);

        
        $this->view->render($response, 'dashboard_admin_participant_liste.twig', [
            'page'  => 'dashboard-admin-participant-liste',
            'token' => $token,
            'role_si' => $roleSI,
            'role' => $role,
            'participants' => $participants,
            'jdev' => $this->settings['jdev']
        ]);
    
        return $response;
    }

    private function getParticipants($type)
    {
        $dql = "SELECT p as participant FROM App\Entity\Participant p";
        $dql .= " WHERE p.emailValide='t' and p.role='$type' ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
