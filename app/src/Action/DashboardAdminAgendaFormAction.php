<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminAgendaFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }
    
        if ($request->isGet()) {
            $formations = $this->getFormations();
            $agenda=null;
            
            $a = [
                'page'  => 'dashboard',
                'token' => $token,
                'role_si' => $roleSI,
                'formations' => $formations,
                'jdev' => $this->settings['jdev']
            ];
            
            if (array_key_exists('agenda_id', $params)) {
                $agenda = $this->em->find('App\Entity\Agenda', $params['agenda_id']);
                $a['agenda'] = $agenda;
                $salle = $this->getSalleById(0);
                $salles = array_merge(array($salle), $this->getSalles($agenda->getDateDebut()->format('Y-m-d H:i:s'), $agenda->getDateFin()->format('Y-m-d H:i:s'), $agenda->getFormation()->getQuota()));
                $a['salles'] = $salles;
            }
            
            if (array_key_exists('quota', $params)) {
                $parsedBody = $request->getParsedBody();
                $debut = date_create_from_format('d/m/Y H:i', $params['jour'] . ' ' . $params['debut']);
                $fin = date_create_from_format('d/m/Y H:i', $params['jour'] . ' ' . $params['fin']);
                $quota = $params['quota'];
                if ($agenda) {
                    $id_salle = $agenda->getSalle()->getId();
                    $salle = $this->getSalleById($id_salle);
                    $salles = array_merge(array($salle), $this->getSalles($debut->format('Y-m-d H:i:s'), $fin->format('Y-m-d H:i:s'), $quota));
                } else {
                    $salles = $this->getSalles($debut->format('Y-m-d H:i:s'), $fin->format('Y-m-d H:i:s'), $quota);
                }
                
                //$salle = $this->getSalleById($id_salle);
                //$salles = array_merge(array($salle), $this->getSalles($debut->format('Y-m-d H:i:s'), $fin->format('Y-m-d H:i:s'),$quota));
                $response = $response->write(json_encode($salles))->withHeader('Content-type', 'application/json');
                return $response;
            } else {
                $this->view->render($response, 'dashboard_admin_agenda_form.twig', $a);
            }
        }

        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
    
            $formation = $this->em->find('App\Entity\Formation', $parsedBody['formation']);
            $debut = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['debut']);
            $fin = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['fin']);
            $salle = $this->em->find('App\Entity\Salle', $parsedBody['salle']);
            $code = $formation->getNom() . '-' . mt_rand(1000, 9999);
                
            $agenda = new \App\Entity\Agenda();
            $agenda->setFormation($formation);
            $agenda->setSalle($salle);
            $agenda->setDateDebut($debut);
            $agenda->setDateFin($fin);
            $agenda->setDisplay($parsedBody['ordre']);
            $agenda->setCodeFormation($code);
            $this->em->persist($agenda);
            $this->em->flush();

            $response = $response->write('Nouvel agenda enregistré')->withStatus(201);
        }
    
        
        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();
    
            $formation = $this->em->find('App\Entity\Formation', $parsedBody['formation']);
            $salle = $this->em->find('App\Entity\Salle', $parsedBody['salle']);
    
            $debut = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['debut']);
            $fin = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['fin']);
    
            $agenda = $this->em->find('App\Entity\Agenda', $parsedBody['agenda_id']);
            $agenda->setFormation($formation);
            $agenda->setSalle($salle);
            $agenda->setDateDebut($debut);
            $agenda->setDateFin($fin);
            $agenda->setDisplay($parsedBody['ordre']);
            $this->em->flush();
        
            $response = $response->write('Agenda n°' . $agenda->getId() . ' modifié')->withStatus(201);
        }
    
        if ($request->isDelete()) {
            $agenda = $this->em->find('App\Entity\Agenda', $params['agenda_id']);
            $this->em->remove($agenda);
            $this->em->flush();
            $response = $response->write('Agenda id: ' . $params['agenda_id'] . ' supprimé')->withStatus(200);
        }
        
        return $response;
    }

    private function getFormations()
    {
        $dql = "SELECT f FROM App\Entity\Formation f LEFT JOIN f.agendas a where f.confirme='t' AND a.dateDebut IS NULL ORDER BY f.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getOneFormation($id_formation)
    {
        $dql = "SELECT f FROM App\Entity\Formation f where f.id=$id_formation";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    
    private function getSalles($debut, $fin, $quota)
    {
        $dql  = "SELECT distinct s FROM App\Entity\Salle s LEFT JOIN  s.agendas a  ";
        $dql .= "WHERE s.quotaPhysique >= $quota AND s.id NOT IN  ";
        $dql .= " ( select s1.id FROM App\Entity\Salle s1 LEFT JOIN s1.agendas a1 where  ";
        $dql .= "   ( a1.dateDebut > '$debut'  and (a1.dateDebut < '$fin' or a1.dateFin < '$fin')  ) OR ";
        $dql .= "   ( '$debut'  > a1.dateDebut and ('$debut' < a1.dateFin or '$fin' < a1.dateFin)  ) OR ";
        $dql .= "   ( a1.dateDebut = '$debut'  and (a1.dateFin is not null) ) ) ";
        $dql .= " ORDER BY s.quotaPhysique";
        $query = $this->em->createQuery($dql);
        return $query->getArrayResult();
    }

    private function getSalleById($id)
    {
        $dql = "SELECT s FROM App\Entity\Salle s WHERE s.id = $id";
        $query = $this->em->createQuery($dql);
        return $query->getArrayResult()[0];
        //return $query->getArrayResult();
    }
}
