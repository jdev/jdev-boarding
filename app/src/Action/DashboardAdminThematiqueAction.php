<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminThematiqueAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }

        $thematiques = $this->getThematiques();

        $this->view->render($response, 'dashboard_admin_thematique.twig', [
            'page'  => 'dashboard-admin-thematique',
            'token' => $token,
            'role_si' => $roleSI,
            'thematiques' => $thematiques,
            'jdev' => $this->settings['jdev']
        ]);

        return $response;
    }

    private function getThematiques()
    {
        $dql = "SELECT t FROM App\Entity\Thematique t ORDER BY t.label";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
