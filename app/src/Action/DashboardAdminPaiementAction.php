<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminPaiementAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
            $this->view = $view;
            $this->logger = $logger;
            $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'clo_admin') && ($roleSI != 'admin')) {
            return $response->withStatus(401);
        }

        $participants = $this->getParticipants();
        

        $this->view->render($response, 'dashboard_admin_paiement.twig', [
            'page'  => 'dashboard-admin-paiement',
            'token' => $token,
            'role_si' => $roleSI,
            'participants' => $participants
        ]);
                
        return $response;
    }

    private function getParticipants()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = 'Participant' and p.emailValide='t' ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
