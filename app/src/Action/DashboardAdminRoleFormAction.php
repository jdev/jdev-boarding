<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminRoleFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
     
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }
    
        if ($request->isGet()) {
            $a = [
                'page'  => 'dashboard',
                'token' => $token,
                'role_si' => $roleSI,
                'jdev' => $this->settings['jdev']
            ];
            if (array_key_exists('role_id', $params)) {
                $role = $this->em->find('App\Entity\Role', $params['role_id']);
                $a['role'] = $role;
            }
            
            $this->view->render($response, 'dashboard_admin_role_form.twig', $a);
        }
    
        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
            $role = new \App\Entity\Role();
            $role->setLabel($parsedBody['label']);
            $this->em->persist($role);
            $this->em->flush();
            $response = $response->write('Nouveau role enregistré')->withStatus(201);
        }
    
        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();
            $role = $this->em->find('App\Entity\Role', $parsedBody['role_id']);
            $role->setLabel($parsedBody['label']);
            $this->em->flush();
            $response = $response->write('Role n°' . $role->getId() . ' modifié')->withStatus(201);
        }
    
        if ($request->isDelete()) {
            $role = $this->em->find('App\Entity\Role', $params['role_id']);
            $this->em->remove($role);
            $this->em->flush();
            $response = $response->write('Role id: ' . $params['role_id'] . ' supprimée')->withStatus(200);
        }
        
        return $response;
    }
}
