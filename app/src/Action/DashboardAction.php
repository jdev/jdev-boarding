<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $email = $request->getAttribute('email');
        $participant = $this->getParticipant($email);
        $roleSI = $request->getAttribute('roleSI');
        
        $a = [
            'token' => $token,
            'participant' => $participant,
            'role_si' => $roleSI,
            'jdev' => $this->settings['jdev']
        ];
        
        if (($roleSI == 'admin') || ($roleSI == 'clo') || ($roleSI == 'clo_pgm')) {
            $this->view->render($response, 'dashboard_admin_profil.twig', $a);
        } else {
            $this->view->render($response, 'dashboard_home.twig', $a);
        }
        return $response;
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
}
