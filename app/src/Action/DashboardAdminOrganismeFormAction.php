<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminOrganismeFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard organisme form page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }
    
        if ($request->isGet()) {
            $a = [
                'page'  => 'dashboard',
                'token' => $token,
                'role_si' => $roleSI,
                'jdev' => $this->settings['jdev']
            ];
            if (array_key_exists('organisme_id', $params)) {
                $organisme = $this->em->find('App\Entity\Organisme', $params['organisme_id']);
                $a['organisme'] = $organisme;
            };
            
            $this->view->render($response, 'dashboard_admin_organisme_form.twig', $a);
        }
    
        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
            $organisme = new \App\Entity\Organisme();
            $organisme->setLabel($parsedBody['label']);
            $organisme->setNbPlaces($parsedBody['nb_places']);
            $organisme->setNbLibres($parsedBody['nb_places']);
            $this->em->persist($organisme);
            $this->em->flush();
            $response = $response->write('Nouvel organisme enregistré')->withStatus(201);
        }
    
        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();
            $organisme = $this->em->find('App\Entity\Organisme', $parsedBody['organisme_id']);
            $organisme->setLabel($parsedBody['label']);
            $organisme->setNbPlaces($parsedBody['nb_places']);
            $organisme->setNbLibres($parsedBody['nb_libres']);
            $this->em->flush();
            $response = $response->write('Organisme n°' . $organisme->getId() . ' modifié')->withStatus(201);
        }
    
        if ($request->isDelete()) {
            $organisme = $this->em->find('App\Entity\Organisme', $params['organisme_id']);
            $this->em->remove($organisme);
            $this->em->flush();
            $response = $response->write('Organisme id: ' . $params['organisme_id'] . ' supprimé')->withStatus(200);
        }
        
        return $response;
    }
}
