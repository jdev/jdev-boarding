<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardParticipantPresenceAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard participant presence action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $email = $request->getAttribute('email');
        $participant = $this->getParticipant($email);

        $agenda = $this->getAgenda($participant->getId());

        $a = [
            'page'  => 'dashboard_participant_presence',
            'token' => $token,
            'agenda' => $agenda,
            'jdev' => $this->settings['jdev']
        ];

        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();

            if (!empty($parsedBody['session']) && !empty($parsedBody['code_formation'])) {
                $idAgenda = $parsedBody['session'];
                $codeFormation = $parsedBody['code_formation'];
                $pa = $this->getParticipantAgenda($participant->getId(), $idAgenda);
                if (is_null($pa)) {
                    return $response->withStatus(400);
                }
                if ($pa[0]->getAgenda()->getCodeFormation() == $codeFormation) {
                    $pa[0]->setPresent(true);
                    $a['confirm'] = 'Votre présence vient d\'être confirmée pour la formation ' . $pa[0]->getAgenda()->getFormation()->getNom();
                    $this->em->persist($pa[0]);
                    $this->em->flush();
                } else {
                    $a['confirm'] = 'Erreur de session ou de code de formation. Votre présence n\'a pas pu être validée';
                }
            } else {
                $a['confirm'] = 'Le formulaire doit être rempli';
            }
        }

        $this->view->render($response, 'dashboard_participant_presence.twig', $a);
        return $response;
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }

    public function getAgenda($id)
    {
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa ";
        $dql .= "WHERE pa.participant = $id";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    public function getParticipantAgenda($idParticipant, $idAgenda)
    {
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa ";
        $dql .= "WHERE pa.participant = $idParticipant AND pa.agenda = $idAgenda";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
