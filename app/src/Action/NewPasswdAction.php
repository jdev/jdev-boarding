<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class NewPasswdAction
{
    private $view;
    private $logger;
    private $em;
    private $mailer;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $mailer, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("New Password page action dispatched");
        
        if ($request->isGet()) {
            $this->view->render($response, 'new_passwd.twig', [
                'page' => 'new-passwd',
                'jdev' => $this->settings['jdev']
            ]);
        }

        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();

            $error = false;
            $code = 200;
            $messages = array();

            if ($this->isEmptyField('email', $parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'Champ vide';
            }

            if (!$this->verifEmail($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'adresse email renseignée est incorrecte';
            }
    
            $participant = $this->getParticipant($parsedBody);
            if (!$participant) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'adresse email ne correspond à aucun participant';
            }
                
            if ($participant && !$participant->getEmailValide()) {
                $error = true;
                $code = 400;
                $messages[] = 'Votre compte n\'est pas actif';
            }

            if ($error) {
                $response = $response->write(implode('<br>', $messages))->withStatus($code);
            } else {
                $pwd = uniqid();
                $hash = password_hash($pwd, PASSWORD_DEFAULT);
                $participant->setPassword($hash);
                $this->em->flush();
                
                $this->sendEmail($pwd, $participant);
                
                $response = $response->write(json_encode(array('ok')))->withStatus($code);
            }
        }

        return $response;
    }

    private function isEmptyField($field, $parsedBody)
    {
        if (!isset($parsedBody[$field]) || empty($parsedBody[$field])) {
            return true;
        } else {
            return false;
        }
    }
    
    private function getParticipant($parsedBody)
    {
        if ($this->isEmptyField('email', $parsedBody)) {
            return false;
        }
        
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => strtolower($parsedBody['email'])));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
    
    private function verifEmail($parsedBody)
    {
        if ($this->isEmptyField('email', $parsedBody)) {
            return true;
        }
        
        if (filter_var($parsedBody['email'], FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
    
    private function sendEmail($password, $participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous venons de générer pour vous un nouveau mot de passe : ';
        $body .= $password . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'] ;
        
        $message = \Swift_Message::newInstance($this->settings['jdev']['label'] . ' nouveau mot de passe')
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail()])
            ->setBody($body);
        
        $this->mailer->send($message);
    }
}
