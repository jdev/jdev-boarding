<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

final class ConnexionAction
{
    private $view;
    private $logger;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Connexion page action dispatched");
        
        if ($request->isGet()) {
            $this->view->render($response, 'connexion.twig', [
                'page' => 'connexion',
                'jdev' => $this->settings['jdev']
            ]);
        }
        
        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
            
            $error = false;
            $code = 200;
            $messages = array();
            
            foreach (array('email', 'password') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    $error = true;
                    $code = 400;
                    $messages[] = 'Champ ' . $a . ' vide';
                }
            }

            $participant = $this->getParticipant($parsedBody);
            if (!$participant) {
                $error = true;
                $code = 400;
                $messages[] = 'Mauvais couple email + password';
            } else {
                if (!$participant->getEmailValide()) {
                    $error = true;
                    $code = 400;
                    $messages[] = 'Le compte utilisateur renseigné n\'est pas actif';
                }

                if (!password_verify($parsedBody['password'], $participant->getPassword())) {
                    $error = true;
                    $code = 400;
                    $messages[] = 'Mauvais couple email + password';
                }
            }
            
            if ($error) {
                $response = $response->write(implode(PHP_EOL, $messages))->withStatus($code);
            } else {
                $token = $this->generateToken($participant->getEmail(), $participant->getRoleSI());
                $a = array('token' => (string) $token, 'roleSI' => $participant->getRoleSI(), 'orga' => $participant->getOrganisme()->getId());
                $response = $response->write(json_encode($a))->withHeader('Content-type', 'application/json');
            }
        }
        
        return $response;
    }
    
    private function isEmptyField($field, $parsedBody)
    {
        if (!isset($parsedBody[$field]) || empty($parsedBody[$field])) {
            return true;
        } else {
            return false;
        }
    }
    
    private function getParticipant($parsedBody)
    {
        if ($this->isEmptyField('email', $parsedBody)) {
            return false;
        }
        
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => strtolower($parsedBody['email'])));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
    
    private function generateToken($email, $roleSI)
    {
        // TODO: Augmenter la force de la signature du jeton
        $signer = new Sha256();
        $token = (new Builder())
            ->setIssuer($this->settings['jdev']['url']) // Emetteur du jeton
            ->setAudience($this->settings['jdev']['url']) // Recepteur du jeton
            ->setIssuedAt(time()) // Date à laquelle le jeton a été généré
            ->setNotBefore(time()) // Date à laquelle le jeton pourra être utilisé
            ->setExpiration(time() + 86400) // Date d'éxpiration du jeton
            ->set('email', $email) // Ajout l'email du participant au jeton
            ->set('roleSI', $roleSI)
            ->sign($signer, 'testing') // Fabrique une signature avec clé
            ->getToken(); // Retourne le jeton généré
        return $token;
    }
}
