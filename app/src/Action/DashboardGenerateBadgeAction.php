<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardGenerateBadgeAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Dashboard generate action dispatched");

        $params = $request->getQueryParams();
        $email = $params['email'];
        $participant = $this->getParticipant($email);

        if (!$participant) {
            return $response->write('Email invalide')->withStatus(400);
        }

        $role = strtolower($participant->getRole());
        if ($role == 'invité') {
            $role = 'invite';
        }

        // Parametres par defaut
        $width = 890;
        $height = 600;
        $pwd = getcwd();
        if (!strpos($pwd, 'public')) {
            $imagefile = './public/images/badge_' . $role . '.png';
            $fontroboto = './config/Roboto-Black.ttf';
            $fontrobotolight = './config/Roboto-Light.ttf';
            $fontTopSecret = './config/top_secret.ttf';
            $fontarial = './config/arial.ttf';
        } else {
            $imagefile = '../public/images/badge_' . $role . '.png';
            $fontroboto = '../config/Roboto-Black.ttf';
            $fontrobotolight = '../config/Roboto-Light.ttf';
            $fontTopSecret = '../config/top_secret.ttf';
            $fontarial = '../config/arial.ttf';
        }

        // Création de l'image badge
        $image = imagecreatefrompng($imagefile);
        $black = imagecolorallocate($image, 0, 0, 0);
        $white = imagecolorallocate($image, 255, 255, 255);
        $red = imagecolorallocate($image, 204, 0, 0);

        // Textes
        if ($participant->getEvenementSocial()) {
            imagettftext($image, 25, 20, 75, 350, $black, $fontTopSecret, 'Event');
        }
        imagettftext($image, 40, 0, 255, 100, $black, $fontroboto, ucfirst(strtolower($participant->getPrenom())));
        imagettftext($image, 40, 0, 255, 160, $black, $fontroboto, strtoupper($participant->getNom()));
        $roleString = $participant->getRole();
        if (($roleString == 'Sponsor') || ($roleString == 'Exposant')) {
            imagettftext($image, 29, 0, 255, 250, $black, $fontrobotolight, $participant->getUnite());
        } else {
            imagettftext($image, 29, 0, 255, 250, $black, $fontrobotolight, $participant->getOrganisme()->getLabel());
        }
        if ($roleString == 'Invité') {
            $roleString = 'Invite';
        }
        $roleLabel = '';
        $idRoleOrga = $participant->getRoleOrga();
        if ($idRoleOrga) {
            $role = $this->em->find('App\Entity\Role', $idRoleOrga);
            $roleLabel = $role->getLabel();
            $roleString .= ' - ' . $roleLabel;
        }
        if ($roleString == 'Participant' || $roleString == 'Accompagnant') {
                imagettftext($image, 35, 0, 400, 380, $black, $fontroboto, $roleString);
        } else {
            if ($roleLabel == 'Contributeur') {
                imagettftext($image, 33, 0, 210, 380, $red, $fontroboto, strtoupper($roleString));
            } else {
                imagettftext($image, 35, 0, 350, 380, $red, $fontroboto, strtoupper($roleString));
            }
        }

        // Copie l'image du badge dans un fichier temp
        $badgeFileName = 'temp' . uniqid() . '.png';
        imagepng($image, '/tmp/' . $badgeFileName);

        // Génére le PDF avec l'image à l'intérieur
        $pdf = new \FPDF('P', 'mm', 'A4');
        $pdf->AddPage();
        $pdf->Image('/tmp/' . $badgeFileName, null, null, 89);

        // Stockage du pdf en mémoire
        ob_start();
        $pdf->Output();
        $pdf_data = ob_get_contents();
        ob_end_clean();

        // Suppression du fichier temporaire
        unlink('/tmp/' . $badgeFileName);

        // Envoi du string du PDF dans la reponse slim
        return $response->write($pdf_data)->withHeader('Content-type', 'application/pdf');
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
}
