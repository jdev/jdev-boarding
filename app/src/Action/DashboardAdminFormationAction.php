<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminFormationAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard formation page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }
        
        $formations = $this->getFormations();
        $intervenants = $this->getIntervenants();
        
        $this->view->render($response, 'dashboard_admin_formation.twig', [
            'page'  => 'dashboard-admin-formation',
            'token' => $token,
            'role_si' => $roleSI,
            'formations' => $formations,
            'intervenants' => $intervenants,
            'jdev' => $this->settings['jdev']
        ]);
        
        return $response;
    }
    
    public function getFormations()
    {
        $dql = "SELECT a FROM App\Entity\Formation a ORDER BY a.nom DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getIntervenants()
    {
        $dql = "SELECT distinct p.nom,p.id,p.prenom FROM App\Entity\ParticipantContribution pc  LEFT JOIN  pc.participant p where pc.contribution != 5 ORDER BY p.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
