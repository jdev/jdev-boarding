<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class AdminValidRoleAction
{
    private $view;
    private $logger;
    private $em;
    private $mailer;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $mailer, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("admin valid role page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }

        if (!array_key_exists('email', $params)) {
            return $response->withStatus(400);
        }
        
        $participant = $this->getParticipant($params['email']);
        if (!$participant) {
            return $response->withStatus(400);
        }

        $participant->setCloValide(true);
        $participant->setAccesValide(true);
        $this->em->flush();
        $this->sendEmail($participant);
    
        return $response;
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => strtolower($email)));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }

    private function sendEmail($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Votre compte vient d\'être validé par un membre du comité d\'organisation des ' . $this->settings['jdev']['label'] . '.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance('Validation inscription ' . $this->settings['jdev']['label'])
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail()])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
}
