<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardSondageAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard-sondage page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $email = $request->getAttribute('email');
        $participant = $this->getParticipant($email);
        
        $a = [
            'page'  => 'dashboard-sondage',
            'token' => $token,
            'participant' => $participant,
            'jdev' => $this->settings['jdev']
        ];
        

        if (array_key_exists('jour', $params)) {
            $j = explode("-", $params['jour']);
            if ($params['debut'] == '08:00:00') {
                $a['jour'] = $j[2] . ' Juillet 2020 - Matin';
            }
            if ($params['debut'] == '13:00:00') {
                $a['jour'] = $j[2] . ' Juillet 2020 - Après-midi';
            }
                
            $agendas = $this->getAgendas($params['jour'], $params['debut'], $params['fin'], $participant->getId());
            $a['agendas'] = $agendas;

            $inscription = $this->getInscriptionAgenda($params['jour'], $params['debut'], $params['fin'], $participant->getId());
            $a['inscription'] = $inscription;
        }

        $this->view->render($response, 'dashboard_sondage.twig', $a);
        return $response;
    }
    
    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
    
    public function getAgendas($jour, $debut, $fin, $id)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT a,";
        $dql .= "(select count(p2) from App\Entity\ParticipantAgenda p2 where p2.agenda=a.id and p2.participant=$id) as check ";
        $dql .= "FROM App\Entity\Agenda a ";
        $dql .= "WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin'";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    
    public function getInscriptionAgenda($jour, $debut, $fin, $id)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT pa from App\Entity\ParticipantAgenda pa LEFT JOIN  pa.agenda a ";
        $dql .= "WHERE pa.participant=$id ";
        $dql .= "AND a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin'";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
