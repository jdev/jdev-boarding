<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardParticipantEditFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard edit profil form action dispatched");
        $params = $request->getQueryParams();
        $token = $params['token'];
        $email = $request->getAttribute('email');
        $participant = $this->getParticipant($email);

        if ($request->isGet()) {
            $organismesRepository = $this->em->getRepository('App\Entity\Organisme');
            $organismes = $organismesRepository->findAll();

            $reseauxRepository = $this->em->getRepository('App\Entity\Reseau');
            $reseaux1 = $reseauxRepository->findBy(array('display' => 1));
            $reseaux2 = $reseauxRepository->findBy(array('display' => 2));
            $reseaux3 = $reseauxRepository->findBy(array('display' => 3));

            $communauteRepository = $this->em->getRepository('App\Entity\Communaute');
            $communaute1 = $communauteRepository->findBy(array('display' => 1));
            $communaute2 = $communauteRepository->findBy(array('display' => 2));
            $communaute3 = $communauteRepository->findBy(array('display' => 3));

            $contributionsRepository = $this->em->getRepository('App\Entity\Contribution');
            $contributions = $contributionsRepository->findAll();
            
            $this->view->render($response, 'dashboard_participant_edit_form.twig', [
                'page'  => 'dashboard',
                'token' => $token,
                'participant' => $participant,
                'organismes' => $organismes,
                'reseaux1' => $reseaux1,
                'reseaux2' => $reseaux2,
                'reseaux3' => $reseaux3,
                'communaute1' => $communaute1,
                'communaute2' => $communaute2,
                'communaute3' => $communaute3,
                'contributions' => $contributions,
                'jdev' => $this->settings['jdev']
            ]);
        }

        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();

            $error = false;
            $code = 200;
            $messages = array();
            
            $mandatoryFields = array(
                'nom',
                'prenom',
                'emploi',
                'unite',
                'region'
            );

            foreach ($mandatoryFields as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    $error = true;
                    $code = 400;
                    $messages[] = 'Champ ' . $a . ' vide';
                }
            }

            if (!$this->verifEmploi($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'emploi ' . $parsedBody['emploi'] . ' selectionné n\'est pas valide';
            }

            if (!$this->verifRegion($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'La région ' . $parsedBody['region'] . ' selectionnée n\'est pas valide';
            }

            if ($error) {
                $response = $response->write(implode(PHP_EOL, $messages))->withStatus($code);
            } else {
                $participant = $this->editParticipant($participant, $parsedBody);
                $response = $response->write('Edition du participant ok')->withStatus(200);
            }
        }
        return $response;
    }

    private function isEmptyField($field, $parsedBody)
    {
        if (!isset($parsedBody[$field]) || empty($parsedBody[$field])) {
            return true;
        } else {
            return false;
        }
    }

    private function verifEmploi($parsedBody)
    {
        switch ($parsedBody['emploi']) {
            case 'Doctorant':
            case 'Technicien':
            case 'Ingénieur':
            case 'Chercheur':
            case 'Enseignant-Chercheur':
            case 'Post-Doctorant':
            case 'Stagiaire':
            case 'Autre':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }

    private function verifRegion($parsedBody)
    {
        switch ($parsedBody['region']) {
            case 'Auvergne-Rhône-Alpes':
            case 'Bourgogne-Franche-Comté':
            case 'Bretagne':
            case 'Centre-Val-de-Loire':
            case 'Corse':
            case 'Grand-Est':
            case 'Hauts-de-France':
            case 'Ile-de-France':
            case 'Nouvelle-Aquitaine':
            case 'Normandie':
            case 'Occitanie':
            case 'Outre-Mer':
            case 'Pays-de-la-Loire':
            case 'Provence-Alpes-Côte d\'Azur':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }

    private function editParticipant($participant, $parsedBody)
    {
        $participant->setNom($parsedBody['nom']);
        $participant->setPrenom($parsedBody['prenom']);
        $participant->setEmploi($parsedBody['emploi']);
        $participant->setUnite($parsedBody['unite']);
        $participant->setRegion($parsedBody['region']);
        // Si le participant veut être inscrit à la mailing list
        if (isset($parsedBody['mailing_list']) && $parsedBody['mailing_list'] == 'on') {
            $participant->setMailingList(true);
        } else {
            $participant->setMailingList(false);
        }

        $this->deleteAllContributions($participant);
        $this->createContributions($parsedBody, $participant);
        $this->deleteAllReseaux($participant);
        $this->createReseaux($parsedBody, $participant);
        $this->deleteAllCommunautes($participant);
        $this->createCommunautes($parsedBody, $participant);
        $this->em->flush();

        return $participant;
    }
    
    private function deleteAllContributions($participant)
    {
        foreach ($participant->getContributions() as $contribution) {
            $this->em->remove($contribution);
        }
        $this->em->flush();
    }
    
    private function createContributions($parsedBody, $participant)
    {
        for ($i = 1; $i <= 7; $i++) {
            if (isset($parsedBody['contribution_' . $i]) && $parsedBody['contribution_' . $i] == 'on') {
                $contribution = $this->em->find('App\Entity\Contribution', $i);
                $participantContribution = new \App\Entity\ParticipantContribution();
                $participantContribution->setParticipant($participant);
                $participantContribution->setContribution($contribution);
                $this->em->persist($participantContribution);
            }
        }
    }
    
    private function deleteAllReseaux($participant)
    {
        foreach ($participant->getReseaux() as $reseau) {
            $this->em->remove($reseau);
        }
        $this->em->flush();
    }

    private function createReseaux($parsedBody, $participant)
    {
        for ($i = 1; $i <= 39; $i++) {
            if (isset($parsedBody['reseau_' . $i]) && $parsedBody['reseau_' . $i] == 'on') {
                $reseau = $this->em->find('App\Entity\Reseau', $i);
                $participantReseau = new \App\Entity\ParticipantReseau();
                $participantReseau->setParticipant($participant);
                $participantReseau->setReseau($reseau);
                $this->em->persist($participantReseau);
            }
        }
    }

    private function deleteAllCommunautes($participant)
    {
        foreach ($participant->getCommunautes() as $communaute) {
            $this->em->remove($communaute);
        }
        $this->em->flush();
    }

    private function createCommunautes($parsedBody, $participant)
    {
        for ($i = 1; $i <= 16; $i++) {
            if (isset($parsedBody['communaute_' . $i]) && $parsedBody['communaute_' . $i] == 'on') {
                $communaute = $this->em->find('App\Entity\Communaute', $i);
                $participantCommunaute = new \App\Entity\ParticipantCommunaute();
                $participantCommunaute->setParticipant($participant);
                $participantCommunaute->setCommunaute($communaute);
                $this->em->persist($participantCommunaute);
            }
        }
    }
}
