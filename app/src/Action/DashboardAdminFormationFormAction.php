<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminFormationFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }
    
        if ($request->isGet()) {
            $thematiques = $this->getThematiques();
            $intervenants = $this->getIntervenants();
            
            $a = [
                'page'  => 'dashboard',
                'token' => $token,
                'role_si' => $roleSI,
                'thematiques' => $thematiques,
                'intervenants' => $intervenants,
                'jdev' => $this->settings['jdev']
            ];
            if (array_key_exists('formation_id', $params)) {
                $formation = $this->em->find('App\Entity\Formation', $params['formation_id']);
                $a['formation'] = $formation;
            };
            if (array_key_exists('duplicate', $params)) {
                $a['duplicate'] = true;
            };
            if (array_key_exists('quota', $params)) {
                return '' . $formation->getQuota();
            } else {
                $this->view->render($response, 'dashboard_admin_formation_form.twig', $a);
            }
        }
    
        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
            $thematique = $this->em->find('App\Entity\Thematique', $parsedBody['thematique']);
                    
            $formation = new \App\Entity\Formation();
            $formation->setNom($parsedBody['nom']);
            $formation->setThematique($thematique);
            $formation->setType($parsedBody['type']);
            $formation->setTitre($parsedBody['titre']);
            $formation->setNbIntervenant($parsedBody['nb_intervenant']);
            $formation->setIntervenant($parsedBody['intervenant']);
            $formation->setQuota($parsedBody['quota']);
            $formation->setConfirme($parsedBody['confirme']);
            $this->em->persist($formation);
            $this->em->flush();
        
            $response = $response->write('Nouvelle formation enregistrée')->withStatus(201);
        }
    
        if ($request->isPut()) {
            // ajouter test sur existance du nom de la formation ?
            $parsedBody = $request->getParsedBody();
            $thematique = $this->em->find('App\Entity\Thematique', $parsedBody['thematique']);
    
            $formation = $this->em->find('App\Entity\Formation', $parsedBody['formation_id']);
            $formation->setNom($parsedBody['nom']);
            $formation->setThematique($thematique);
            $formation->setType($parsedBody['type']);
            $formation->setTitre($parsedBody['titre']);
            $formation->setNbIntervenant($parsedBody['nb_intervenant']);
            $formation->setIntervenant($parsedBody['intervenant']);
            $formation->setQuota($parsedBody['quota']);
            $formation->setConfirme($parsedBody['confirme']);
            $this->em->flush();
        
            $response = $response->write('Formation n°' . $formation->getId() . ' modifiée')->withStatus(201);
        }
    
        // Ajouter supprimer dans Entity\Agenda !!!
        if ($request->isDelete()) {
            $formation = $this->em->find('App\Entity\Formation', $params['formation_id']);
            $this->em->remove($formation);
            $this->em->flush();
            
            $response = $response->write('Formation id: ' . $params['formation_id'] . ' supprimée')->withStatus(200);
        }
        
        return $response;
    }
    
    private function getThematiques()
    {
        $dql = "SELECT t FROM App\Entity\Thematique t ORDER BY t.label ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getIntervenants()
    {
        $dql = "SELECT distinct p.nom,p.id,p.prenom FROM App\Entity\ParticipantContribution pc  LEFT JOIN  pc.participant p where pc.contribution != 5 ORDER BY p.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
