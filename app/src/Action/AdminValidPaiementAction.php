<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class AdminValidPaiementAction
{
    private $view;
    private $logger;
    private $em;
    private $mailer;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $mailer, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("admin valid paiement page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $valid = $params['valid'];
        $roleSI = $request->getAttribute('roleSI');

        
        if (($roleSI != 'admin') && ($roleSI != 'clo_admin')) {
            return $response->withStatus(401);
        }
        
        if (!array_key_exists('email', $params)) {
            return $response->withStatus(400);
        }
            
        $participant = $this->getParticipant($params['email']);
        if (!$participant) {
            return $response->withStatus(400);
        }

        
        // Si invalidation d'une place pre-payée
        if ($valid == 'false') {
            $organisme = $this->getOrganisme($participant->getOrganisme()->getId());
            $participant->setAccesValide(false);
            $participant->setPassPrepaye(false);
            $organisme->setNbLibres($organisme->getNbLibres() + 1);
            $this->em->flush();
            $this->sendEmailInvalidation($participant);
            return $response;
        } else {
            // Si validation du paiment Azur-Colloque
            $participant->setAccesValide(true);
            $this->em->flush();
            $this->sendEmailPaiement($participant);
            return $response;
        }
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => strtolower($email)));
        if (isset($participant)) {
            return $participant;
        } else {
            return true;
        }
    }
    
    private function getOrganisme($orga_id)
    {
        $organisme = $this->em->getRepository('App\Entity\Organisme')->findOneBy(array('id' => $orga_id));
        if (isset($organisme)) {
            return $organisme;
        } else {
            return true;
        }
    }

    private function sendEmailInvalidation($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . ',' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Votre prise en charge vient d\'être invalidée par votre organisme.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Pour que votre inscription soit prise en compte, vous devez régler ';
        $body .= 'les frais d\'inscription sur la plate-forme Azur-Colloque: https://www.azur-colloque.fr/DR14/ .' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance('Invalidation place pré-payée ' . $this->settings['jdev']['label'])
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail(), $this->settings['jdev']['email']])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
    
    private function sendEmailPaiement($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous avons acté votre paiement auprès d\'Azur-Colloque pour les ' . $this->settings['jdev']['label'] . '.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Votre espace personnel JdevBoarding vous permettra de sélectionner et d\'obtenir les informations nécessaires pour suivre vos sessions.'. PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'En raison de la pandémie, le format des JDEV de cette édition est adapté : un premier temps fort en distanciel du 6 juillet au 10 juillet 2020.'; 
        $body .= PHP_EOL;
        $body .= 'Nous vous invitons à consulter les liens suivant pour plus d\'information :'. PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020/inscription#information-relative-%C3%A0-la-crise-sanitaire-du-covid-10' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020#agenda' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020#l-%C3%A9dition-connect%C3%A9e' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Cette nouvelle modalité vous offrira l\'avantage de pouvoir suivre plus largement le programme proposé. ';
        $body .= 'Celui-ci sera étalé temporellement. ' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance('Validation paiement inscription ' . $this->settings['jdev']['label'])
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail(), $this->settings['jdev']['email']])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
}
