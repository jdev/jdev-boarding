<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardGenerateBadgeListAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Dashboard generate action dispatched");

        $params = $request->getQueryParams();
        if ($params['role'] != 'contributeur') {
            $role = $this->verifRole($params['role']);

            if (!$role) {
                return $response->write('Role invalide')->withStatus(400);
            }
            if (array_key_exists('date', $params)) {
                $participants = $this->getParticipants($role, $params['date']);
            } else {
                $participants = $this->getParticipants($role);
            }
        } else {
            $participants = $this->getContributeurs();
        }
            
        // Génére le PDF
        $pdf = new \FPDF('P', 'mm', 'A4');
        $pdf->AddPage();

        $nbParticipants = count($participants);
        $z = 0;
        $cntForPage = 0;
        while ($z < $nbParticipants) {
            $y = $pdf->GetY();
            $badgeFileName1 = $this->createBadge($participants[$z]);
            $pdf->Image('/tmp/' . $badgeFileName1, 10, $y, 89);
            unlink('/tmp/' . $badgeFileName1);
            if ($z + 1 < $nbParticipants) {
                $badgeFileName2 = $this->createBadge($participants[$z + 1]);
                $pdf->Image('/tmp/' . $badgeFileName2, 99, $y, 89);
                unlink('/tmp/' . $badgeFileName2);
                $pdf->Ln(60);
                $cntForPage += 2;
                if ($cntForPage == 8) {
                    $pdf->AddPage();
                    $cntForPage = 0;
                }
            }
            $z += 2;
        }

        // Stockage du pdf en mémoire
        ob_start();
        $pdf->Output();
        $pdf_data = ob_get_contents();
        ob_end_clean();

        // Envoi du string du PDF dans la reponse slim
        return $response->write($pdf_data)->withHeader('Content-type', 'application/pdf');
    }

    private function createBadge($participant)
    {
        $role = strtolower($participant->getRole());
        if ($role == 'invité') {
            $role = 'invite';
        }

        // Parametres par defaut
        $width = 890;
        $height = 600;
        $pwd = getcwd();
        if (!strpos($pwd, 'public')) {
            $imagefile = './public/images/badge_' . $role . '.png';
            $fontroboto = './config/Roboto-Black.ttf';
            $fontrobotolight = './config/Roboto-Light.ttf';
            $fontTopSecret = './config/top_secret.ttf';
            $fontarial = './config/arial.ttf';
        } else {
            $imagefile = '../public/images/badge_' . $role . '.png';
            $fontroboto = '../config/Roboto-Black.ttf';
            $fontrobotolight = '../config/Roboto-Light.ttf';
            $fontTopSecret = '../config/top_secret.ttf';
            $fontarial = '../config/arial.ttf';
        }

        // Création de l'image badge
        $image = imagecreatefrompng($imagefile);
        $black = imagecolorallocate($image, 0, 0, 0);
        $white = imagecolorallocate($image, 255, 255, 255);
        $red = imagecolorallocate($image, 204, 0, 0);

        // Textes
        if ($participant->getEvenementSocial()) {
            imagettftext($image, 25, 20, 75, 350, $black, $fontTopSecret, 'Event');
        }
        imagettftext($image, 40, 0, 255, 100, $black, $fontroboto, ucfirst(strtolower($participant->getPrenom())));
        imagettftext($image, 40, 0, 255, 160, $black, $fontroboto, strtoupper($participant->getNom()));
        $roleString = $participant->getRole();
        
        if (($roleString == 'Sponsor') || ($roleString == 'Exposant')) {
            imagettftext($image, 29, 0, 255, 250, $black, $fontrobotolight, $participant->getUnite());
        } else {
            imagettftext($image, 29, 0, 255, 250, $black, $fontrobotolight, $participant->getOrganisme()->getLabel());
        }
        if ($roleString == 'Invité') {
            $roleString = 'Invite';
        }
        $roleLabel = '';
        $idRoleOrga = $participant->getRoleOrga();
        if ($idRoleOrga) {
            $role = $this->em->find('App\Entity\Role', $idRoleOrga);
            $roleLabel = $role->getLabel();
            $roleString .= ' - ' . $roleLabel;
        }
        if ($roleString == 'Participant' || $roleString == 'Accompagnant') {
            imagettftext($image, 35, 0, 400, 380, $black, $fontroboto, $roleString);
        } else {
            if ($roleLabel == 'Contributeur') {
                imagettftext($image, 33, 0, 210, 380, $red, $fontroboto, strtoupper($roleString));
            } else {
                imagettftext($image, 35, 0, 350, 380, $red, $fontroboto, strtoupper($roleString));
            }
        }

        // Copie l'image du badge dans un fichier temp
        $badgeFileName = 'temp' . uniqid() . '.png';
        imagepng($image, '/tmp/' . $badgeFileName);
        return $badgeFileName;
    }

    private function verifRole($role)
    {
        if ($role == 'organisateur') {
            return 'Organisateur';
        } elseif ($role == 'invite') {
            return 'Invité';
        } elseif ($role == 'sponsor') {
            return 'Sponsor';
        } elseif ($role == 'exposant') {
            return 'Exposant';
        } elseif ($role == 'participant') {
            return 'Participant';
        } elseif ($role == 'accompagnant') {
            return 'Accompagnant';
        } else {
            return false;
        }
    }

    private function getParticipants($role, $date = null)
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = '" . $role . "' and p.emailValide='t' ";
        if ($date) {
            $dql .= "and p.dateInscription >= '" . $date . " 00:00:00' ";
        }
        $dql .= "ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getContributeurs()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.roleOrga = '3' and p.emailValide='t' ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
