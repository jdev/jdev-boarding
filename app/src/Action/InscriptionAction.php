<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class InscriptionAction
{
    private $view;
    private $logger;
    private $em;
    private $mailer;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $mailer, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Inscription action dispatched");
        
        if ($request->isGet()) {
            $organismesRepository = $this->em->getRepository('App\Entity\Organisme');
            //$organismes = $organismesRepository->findAll(array('order'=>'label ASC'));
            $organismes = $organismesRepository->findBy(array(), array('label' => 'ASC'));

            $reseauxRepository = $this->em->getRepository('App\Entity\Reseau');
            $reseaux1 = $reseauxRepository->findBy(array('display' => 1));
            $reseaux2 = $reseauxRepository->findBy(array('display' => 2));
            $reseaux3 = $reseauxRepository->findBy(array('display' => 3));

            $communauteRepository = $this->em->getRepository('App\Entity\Communaute');
            $communaute1 = $communauteRepository->findBy(array('display' => 1));
            $communaute2 = $communauteRepository->findBy(array('display' => 2));
            $communaute3 = $communauteRepository->findBy(array('display' => 3));
            
            $contributionsRepository = $this->em->getRepository('App\Entity\Contribution');
            $contributions = $contributionsRepository->findAll();


            $this->view->render($response, 'inscription.twig', [
                'page' => 'inscription',
                'organismes' => $organismes,
                'reseaux1' => $reseaux1,
                'reseaux2' => $reseaux2,
                'reseaux3' => $reseaux3,
                'communaute1' => $communaute1,
                'communaute2' => $communaute2,
                'communaute3' => $communaute3,
                'contributions' => $contributions,
                'jdev' => $this->settings['jdev']
            ]);
        }
        
        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();

            $error = false;
            $code = 200;
            $messages = array();
            
            $mandatoryFields = array(
                'nom',
                'prenom',
                'email',
                'password',
                'statut',
                'emploi',
                'organisme',
                'unite',
                'role',
                'region',
                'type_inscription'
            );

            foreach ($mandatoryFields as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    $error = true;
                    $code = 400;
                    $messages[] = 'Champ ' . $a . ' vide';
                }
            }

            if (!$this->verifEmail($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'adresse email renseignée est incorrecte';
            }

            if (!$this->verifExistParticipant($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'adresse email . ' . $parsedBody['email'] . ' est déjà utilisée';
            }

            if (!$this->verifStatut($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'Le statut ' . $parsedBody['statut'] . ' sélectionné n\'est pas valide';
            }

            if (!$this->verifEmploi($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'emploi ' . $parsedBody['emploi'] . ' sélectionné n\'est pas valide';
            }

            if (!($organisme = $this->verifOrganisme($parsedBody))) {
                $error = true;
                $code = 400;
                $messages[] = 'L\'identifiant de l\'organisme ' . $parsedBody['organisme'] . ' sélectionné n\'est pas valide';
            }

            if (!$this->verifRole($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'Le rôle ' . $parsedBody['role'] . ' sélectionné n\'est pas valide';
            }

            if (!$this->verifRegion($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'La région ' . $parsedBody['region'] . ' sélectionnée n\'est pas valide';
            }

            if (!$this->verifTypeInscription($parsedBody)) {
                $error = true;
                $code = 400;
                $messages[] = 'Le type d\'inscription ' . $parsedBody['type_inscription'] . ' sélectionné n\'est pas valide';
            }

            if ($error) {
                $response = $response->write(implode(PHP_EOL, $messages))->withStatus($code);
            } else {
                $participant = $this->createParticipant($parsedBody, $organisme);
                $this->sendEmail($request, $participant);
                $response = $response->write('Nouvel utilisateur enregistré')->withStatus(201);
            }
        }
        
        return $response;
    }
    
    private function isEmptyField($field, $parsedBody)
    {
        if (!isset($parsedBody[$field]) || empty($parsedBody[$field])) {
            return true;
        } else {
            return false;
        }
    }

    private function verifEmail($parsedBody)
    {
        if ($this->isEmptyField('email', $parsedBody)) {
            return true;
        }
        
        if (filter_var($parsedBody['email'], FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    private function verifExistParticipant($parsedBody)
    {
        if ($this->isEmptyField('email', $parsedBody)) {
            return false;
        }
        
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => strtolower($parsedBody['email'])));
        if (isset($participant)) {
            return false;
        } else {
            return true;
        }
    }

    private function verifStatut($parsedBody)
    {
        switch ($parsedBody['statut']) {
            case 'ETUDIANT':
            case 'ACADEMIQUE':
            case 'INDUSTRIEL':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }

    private function verifEmploi($parsedBody)
    {
        switch ($parsedBody['emploi']) {
            case 'Doctorant':
            case 'Technicien':
            case 'Ingénieur':
            case 'Chercheur':
            case 'Enseignant-Chercheur':
            case 'Post-Doctorant':
            case 'Stagiaire':
            case 'Autre':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }

    private function verifOrganisme($parsedBody)
    {
        if ($this->isEmptyField('organisme', $parsedBody)) {
            return false;
        }

        $organisme = $this->em->find('App\Entity\Organisme', $parsedBody['organisme']);
        if (!isset($organisme)) {
            return false;
        } else {
            return $organisme;
        }
    }

    private function verifRole($parsedBody)
    {
        switch ($parsedBody['role']) {
            case 'Participant':
            case 'Individuel':
            case 'Organisateur':
            case 'Invité':
            case 'Exposant':
            case 'Sponsor':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }

    private function verifRegion($parsedBody)
    {
        switch ($parsedBody['region']) {
            case 'Auvergne-Rhône-Alpes':
            case 'Bourgogne-Franche-Comté':
            case 'Bretagne':
            case 'Centre-Val-de-Loire':
            case 'Corse':
            case 'Grand-Est':
            case 'Hauts-de-France':
            case 'Ile-de-France':
            case 'Nouvelle-Aquitaine':
            case 'Normandie':
            case 'Occitanie':
            case 'Outre-Mer':
            case 'Pays-de-la-Loire':
            case 'Provence-Alpes-Côte d\'Azur':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }

    private function verifTypeInscription($parsedBody)
    {
        switch ($parsedBody['type_inscription']) {
            case 'PASS COMPLET':
            case 'PASS SOUTIEN':
            case 'PASS LIBRE':
            case 'PASS INDIVIDUEL':
            case 'PASS ORGA':
            case 'PASS INVITE':
            case 'PASS SPONSOR':
            case 'PASS EXPOSANT':
                $bool = true;
                break;
            default:
                $bool = false;
        }
        return $bool;
    }
    
    private function createParticipant($parsedBody, $organisme)
    {
        $participant = new \App\Entity\Participant();

        $participant->setNom($parsedBody['nom']);
        $participant->setPrenom($parsedBody['prenom']);
        $participant->setEmail(strtolower($parsedBody['email']));
        $participant->setPassword(password_hash($parsedBody['password'], PASSWORD_DEFAULT));
        $participant->setStatut($parsedBody['statut']);
        $participant->setEmploi($parsedBody['emploi']);
        $participant->setOrganisme($organisme);
        $participant->setUnite($parsedBody['unite']);
        $participant->setRole($parsedBody['role']);
        $participant->setRegion($parsedBody['region']);
        $participant->setTypeInscription($parsedBody['type_inscription']);
        $participant->setRegimeAlimentaire($parsedBody['regime_alimentaire']);
        // Si le participant est une personne à mobilité reduite
        if (isset($parsedBody['mobilite_reduite']) && $parsedBody['mobilite_reduite'] == 'on') {
            $participant->setMobiliteReduite(true);
        } else {
            $participant->setMobiliteReduite(false);
        }
        // Si le participant demande un code wi-fi
        if (isset($parsedBody['acces_wifi']) && $parsedBody['acces_wifi'] == 'on') {
            $participant->setCodeWifi(true);
        } else {
            $participant->setCodeWifi(false);
        }
        // Si le participant veut être inscrit à la mailing list
        if (isset($parsedBody['mailing_list']) && $parsedBody['mailing_list'] == 'on') {
            $participant->setMailingList(true);
        } else {
            $participant->setMailingList(false);
        }
        // Si le participant veut se rendre à l'événement social
        if (isset($parsedBody['evenement_social']) && $parsedBody['evenement_social'] == 'on') {
            $participant->setEvenementSocial(true);
        } else {
            $participant->setEvenementSocial(false);
        }
        $participant->setDateInscription(new \DateTime("now"));
        $participant->setCleEmail(uniqid('', true)); // Clé activation pour verification email
        $participant->setEmailValide(false); // Attente URL activation
        if ($parsedBody['role'] == 'Participant' || $parsedBody['role'] == 'Individuel') {
            $participant->setCloValide(true); // True si participant ou accompagnant
        } else {
            $participant->setCloValide(false); // False sinon; Besoin d'une activation d'un admin
        }
        $participant->setAccesValide(false); // Nécessite le paiement AC des droits d'inscription ou place pré-payé (voir ValidEmailAction)
        $participant->setPassPrepaye(false); // Le participant n'est pas pris en charge par son organisme
        
        $this->em->persist($participant);
        $this->createContributions($parsedBody, $participant);
        $this->createReseaux($parsedBody, $participant);
        $this->createCommunautes($parsedBody, $participant);
        $this->em->flush();

        return $participant;
    }

    private function createContributions($parsedBody, $participant)
    {
        for ($i = 1; $i <= 7; $i++) {
            if (isset($parsedBody['contribution_' . $i]) && $parsedBody['contribution_' . $i] == 'on') {
                $contribution = $this->em->find('App\Entity\Contribution', $i);
                $participantContribution = new \App\Entity\ParticipantContribution();
                $participantContribution->setParticipant($participant);
                $participantContribution->setContribution($contribution);
                $this->em->persist($participantContribution);
            }
        }
    }
    
    private function createReseaux($parsedBody, $participant)
    {
        for ($i = 1; $i <= 39; $i++) {
            if (isset($parsedBody['reseau_' . $i]) && $parsedBody['reseau_' . $i] == 'on') {
                $reseau = $this->em->find('App\Entity\Reseau', $i);
                $participantReseau = new \App\Entity\ParticipantReseau();
                $participantReseau->setParticipant($participant);
                $participantReseau->setReseau($reseau);
                $this->em->persist($participantReseau);
            }
        }
    }

    private function createCommunautes($parsedBody, $participant)
    {
        for ($i = 1; $i <= 16; $i++) {
            if (isset($parsedBody['communaute_' . $i]) && $parsedBody['communaute_' . $i] == 'on') {
                $communaute = $this->em->find('App\Entity\Communaute', $i);
                $participantCommunaute = new \App\Entity\ParticipantCommunaute();
                $participantCommunaute->setParticipant($participant);
                $participantCommunaute->setCommunaute($communaute);
                $this->em->persist($participantCommunaute);
            }
        }
    }

    private function sendEmail($request, $participant)
    {
        $url  = $request->getUri()->getScheme() . '://' . $request->getUri()->getHost();
        if ($request->getUri()->getPort() != 80) {
            $url .= ':' . $request->getUri()->getPort();
        }
        $url .= '/valid-email?email=' . $participant->getEmail() . '&key=' . $participant->getCleEmail();

        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Afin d\'activer votre compte, veuillez cliquer sur le lien ';
        $body .= 'ci-dessous pour vérifier votre adresse e-mail : ' . PHP_EOL;
        $body .= $url . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance('Confirmation inscription aux ' . $this->settings['jdev']['label'])
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail()])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
}
