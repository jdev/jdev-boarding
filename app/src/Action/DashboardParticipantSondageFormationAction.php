<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardParticipantSondageFormationAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        $params = $request->getQueryParams();
        $token = $params['token'];
        $id_formation = (int) $params['formation'];
        $valid = $params['valid'];
        $email = $request->getAttribute('email');
        $participant = $this->getParticipant($email);
        $id_participant = (int)$participant->getId();
        
        // Suppression de la sélection à la formation
        if ($valid == 'false') {
            $inscription = $this->getInscriptionSondage($id_participant, $id_formation);

            $this->em->remove($inscription[0]);
            $this->em->flush();
        }
        // Sélection de la formation
        if ($valid == 'true') {
            $formation = $this->em->find('App\Entity\Formation', $id_formation);
            $inscription = new \App\Entity\ParticipantSondage();
            $inscription->setParticipant($participant);
            $inscription->setFormation($formation);
            date_default_timezone_set('UTC');
            $ladate = date_create_from_format('Y-m-d H:i:s', date("Y-m-d H:i:s"));
            $inscription->setDateInscription($ladate);

            $this->em->persist($inscription);
            $this->em->flush();
        }

        $this->view->render($response, 'dashboard_participant_sondage.twig', [
            'page'  => 'dashboard',
            'token' => $token,
            'participant' => $participant
        ]);
        return $response;
    }
    
    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
           
    public function getInscriptionSondage($id_participant, $id_formation)
    {
        $dql  = "SELECT ps from App\Entity\ParticipantSondage ps ";
        $dql .= "WHERE ps.participant=$id_participant ";
        $dql .= "AND ps.formation=$id_formation";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
