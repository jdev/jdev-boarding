<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminAnnulerSessionAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings, $mailer)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
        $this->mailer = $mailer;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin annuler session action dispatched");

        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm') && ($roleSI != 'clo')) {
            return $response->withStatus(401);
        }

        $params = $request->getQueryParams();
        $token = $params['token'];
        $agenda = $this->em->find('App\Entity\Agenda', $params['agenda_id']);

        $participants = $agenda->getParticipants();
        foreach ($participants as $participant) {
            $this->sendMail($participant->getParticipant(), $agenda);
        }
        $this->sendMailToClo($agenda);
        $this->deleteAllParticipantAgenda($params['agenda_id']);

        $this->em->remove($agenda);
        $this->em->flush();

        $a = [
            'token' => $token,
            'role_si' => $roleSI,
            'formation' => $agenda->getFormation()
        ];

        $this->view->render($response, 'dashboard_admin_annuler_session.twig', $a);
        return $response;
    }

    private function getAllParticipantAgenda($id)
    {
        $dql  = "SELECT pa from App\Entity\ParticipantAgenda pa LEFT JOIN  pa.agenda a ";
        $dql .= "WHERE a.formation=$id";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function deleteAllParticipantAgenda($id)
    {
        $dql = "DELETE FROM App\Entity\ParticipantAgenda pa WHERE pa.agenda = $id";
        $query = $this->em->createQuery($dql);
        return $query->execute();
    }

    private function sendMail($participant, $agenda)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'La session de formation ' . $agenda->getFormation()->getNom() . ' (' . $agenda->getDateDebut()->format('d-m-Y H:i') . ' - ' . $agenda->getDateFin()->format('d-m-Y H:i') . ') vient d\'être annulée.' . PHP_EOL;
        $body .= 'Vous pouvez vous inscrire dans une autre session de formation si vous le souhaitez ';
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance('[' . $this->settings['jdev']['label'] . '] Annulation formation ' . $agenda->getFormation()->getNom())
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail()])
            ->setBody($body);
            
        $this->mailer->send($message);
    }

    private function sendMailToClo($agenda)
    {
        $body  = 'Bonjour ' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'La formation ' . $agenda->getFormation()->getNom() . ' vient d\'être annulée.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance('[' . $this->settings['jdev']['label'] . '] Annulation formation ' . $agenda->getFormation()->getNom())
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([ $this->settings['jdev']['email'] ])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
}
