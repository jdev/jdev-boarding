<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminThematiqueAgendaAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }
        
        $a = [
            'page'  => 'dashboard-admin-thematique-agenda',
            'token' => $token,
            'role_si' => $roleSI,
            'jdev' => $this->settings['jdev']
        ];
        $a['thematique'] = $this->em->find('App\Entity\Thematique', $params['them_id']);

        $a['pa_am'][0] = $this->getAgendas($this->settings['jdev']['j1'], '08:00:00', '13:00:00');
        $a['pa_pm'][0] = $this->getAgendas($this->settings['jdev']['j1'], '12:00:00', '19:00:00');
        $a['pa_am'][1] = $this->getAgendas($this->settings['jdev']['j2'], '08:00:00', '13:00:00');
        $a['pa_pm'][1] = $this->getAgendas($this->settings['jdev']['j2'], '12:00:00', '19:00:00');
        $a['pa_am'][2] = $this->getAgendas($this->settings['jdev']['j3'], '08:00:00', '13:00:00');
        $a['pa_pm'][2] = $this->getAgendas($this->settings['jdev']['j3'], '12:00:00', '19:00:00');
        $a['pa_am'][3] = $this->getAgendas($this->settings['jdev']['j4'], '08:00:00', '13:00:00');
        $a['pa_pm'][3] = $this->getAgendas($this->settings['jdev']['j4'], '12:00:00', '19:00:00');
        $a['pa_am'][4] = $this->getAgendas($this->settings['jdev']['j5'], '08:00:00', '13:00:00');
        $a['pa_pm'][4] = $this->getAgendas($this->settings['jdev']['j5'], '12:00:00', '19:00:00');


        $this->view->render($response, 'dashboard_admin_thematique_agenda.twig', $a);
        return $response;
    }

    private function getAgendas($jour, $debut, $fin)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where pa.agenda=a.id) as nb_inscrits ";
        $dql .= "FROM App\Entity\Agenda a ";
        $dql .= " WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin' ORDER BY a.display, a.dateDebut ASC";

        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
