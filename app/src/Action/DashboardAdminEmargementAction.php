<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminEmargementAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin emargement action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'clo') && ($roleSI != 'admin')) {
            return $response->withStatus(401);
        }

        $participants = $this->getParticipants();
        $thematiques = $this->getThematiques();
        
        $a = [
            'page'  => 'dashboard-admin-emargement-gestion',
            'token' => $token,
            'role_si' => $roleSI,
            'participants' => $participants,
            'thematiques' => $thematiques,
            'jdev' => $this->settings['jdev']
        ];

        $a['pa_4_am'] = $this->getAgendas($this->settings['jdev']['j1'], '08:00:00', '13:00:00');
        $a['pa_4_pm'] = $this->getAgendas($this->settings['jdev']['j1'], '12:00:00', '19:00:00');
        $a['pa_5_am'] = $this->getAgendas($this->settings['jdev']['j2'], '08:00:00', '13:00:00');
        $a['pa_5_pm'] = $this->getAgendas($this->settings['jdev']['j2'], '12:00:00', '19:00:00');
        $a['pa_6_am'] = $this->getAgendas($this->settings['jdev']['j3'], '08:00:00', '13:00:00');
        $a['pa_6_pm'] = $this->getAgendas($this->settings['jdev']['j3'], '12:00:00', '19:00:00');
        $a['pa_7_am'] = $this->getAgendas($this->settings['jdev']['j4'], '08:00:00', '13:00:00');
        $a['pa_7_pm'] = $this->getAgendas($this->settings['jdev']['j4'], '12:00:00', '19:00:00');
        $a['pa_8_am'] = $this->getAgendas($this->settings['jdev']['j5'], '08:00:00', '13:00:00');
        $a['pa_8_pm'] = $this->getAgendas($this->settings['jdev']['j5'], '12:00:00', '19:00:00');
    
        $this->view->render($response, 'dashboard_admin_emargement.twig', $a);

        return $response;
    }

    public function getAgendas($jour, $debut, $fin)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where pa.agenda=a.id) as nb_inscrits ";
        $dql .= "FROM App\Entity\Agenda a ";
        $dql .= "WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin' ORDER BY a.display, a.dateDebut ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    private function getParticipants()
    {
        $dql = "SELECT p as participant, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where p.id=pa.participant) as inscrit ";
        $dql .= " FROM App\Entity\Participant p";
        $dql .= " WHERE p.emailValide='t' ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    private function getThematiques()
    {
        $dql = "SELECT t FROM App\Entity\Thematique t ORDER BY t.label";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
