<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }


    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'admin') && ($roleSI != 'clo_admin') && ($roleSI != 'clo') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }

        $role_orga = $this->getRoles();
        $organisateurs = $this->getOrganisteurs();
        $invites = $this->getInvites();
        $sponsors = $this->getSponsors();
        $exposants = $this->getExposants();
        $accompagnants = $this->getAccompagnants();
    
        $this->view->render($response, 'dashboard_admin_home.twig', [
            'page'  => 'dashboard-admin',
            'token' => $token,
            'role_si' => $roleSI,
            'role_orga' => $role_orga,
            'organisateurs' => $organisateurs,
            'invites' => $invites,
            'sponsors' => $sponsors,
            'exposants' => $exposants,
            'accompagnants' => $accompagnants,
            'jdev' => $this->settings['jdev']
        ]);
    
        return $response;
    }
    
    public function getRoles()
    {
        $dql = "SELECT a FROM App\Entity\Role a ORDER BY a.label DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getOrganisteurs()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = 'Organisateur' ORDER BY p.dateInscription DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getInvites()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = 'Invité' ORDER BY p.dateInscription DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getSponsors()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = 'Sponsor' ORDER BY p.dateInscription DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getExposants()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = 'Exposant' ORDER BY p.dateInscription DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    private function getAccompagnants()
    {
        $dql = "SELECT p FROM App\Entity\Participant p WHERE p.role = 'Accompagnant' ORDER BY p.dateInscription DESC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
