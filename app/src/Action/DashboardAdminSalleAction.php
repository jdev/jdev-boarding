<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminSalleAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin salle page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }

        $salles = $this->getSalles();

        $this->view->render($response, 'dashboard_admin_salle.twig', [
            'page'  => 'dashboard-admin-salle',
            'token' => $token,
            'role_si' => $roleSI,
            'salles' => $salles,
            'jdev' => $this->settings['jdev']
        ]);

        return $response;
    }

    private function getSalles()
    {
        $dql = "SELECT s FROM App\Entity\Salle s ORDER BY s.etage ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
