<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminEmargementAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        
        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }

        $thematiques = $this->getThematiques();

        $a = [
            'page'  => 'dashboard-admin-emargement',
            'token' => $token,
            'role_si' => $roleSI,
            'thematiques' => $thematiques
        ];
        if (array_key_exists('thematique_id', $params)) {
            $formations = $this->getFormations($params['thematique_id']);
            $response = $response->write(json_encode($formations))->withHeader('Content-type', 'application/json');
            return $response;
        } else {
            if (array_key_exists('formation_id', $params)) {
                $formation = $this->em->find('App\Entity\Formation', $params['formation_id']);
                $a['formation'] = $formation;
                $participants = $this->getAllParticipantAgenda();
                $a['participants'] = $participants;
            }
            $this->view->render($response, 'dashboard_admin_emargement.twig', $a);
            return $response;
        }
    }

    private function getThematiques()
    {
        $dql = "SELECT t FROM App\Entity\Thematique t ORDER BY t.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
       
    private function getFormations($them_id)
    {
        $dql = "SELECT f FROM App\Entity\Formation f where f.thematique=$them_id";
        $query = $this->em->createQuery($dql);
        return $query->getArrayResult();
    }

    public function getAllParticipantAgenda()
    {
        $dql  = "SELECT pa from App\Entity\ParticipantAgenda pa LEFT JOIN  pa.agenda a ";
        $dql .= "WHERE a.formation=18";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
