<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminEmargementPresenceAction
{
    private $view;
    private $logger;
    private $em;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if (($roleSI != 'admin') && ($roleSI != 'clo')  && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }
    
        $a = [
            'token' => $token,
            'role_si' => $roleSI
        ];
            
        if ((array_key_exists('formation', $params)) && (array_key_exists('pa_id', $params)) && (array_key_exists('a_id', $params))) {
            $formation = $this->em->find('App\Entity\Formation', $params['formation']);
            $a['formation'] = $formation;
            $inscription = $this->getInscriptionAgenda($params['pa_id'], $params['a_id']);
        } elseif (array_key_exists('agenda', $params)) {
            $formation = $this->em->find('App\Entity\Formation', $params['formation']);
            $a['formation'] = $formation;
            $participants = $params['participants'];
            $inscriptions = $this->getInscritsAgenda($params['agenda'], $participants);
            foreach ($inscriptions as $inscription) {
                if ($params['valider'] == 'true') {
                    $inscription->setPresent(true);
                }
                if ($params['valider'] == 'false') {
                    $inscription->setPresent(false);
                }
                $this->em->flush();
            }
        } else {
            return $response->withStatus(400);
        }
        
        if (array_key_exists('present', $params)) {
            if ($params['present'] == 'true') {
                $inscription[0]->setPresent(true);
            }
            if ($params['present'] == 'false') {
                $inscription[0]->setPresent(false);
            }
            $this->em->flush();
        }
        $this->view->render($response, 'dashboard_admin_emargement_liste.twig', $a);
        return $response;
    }
        
    public function getInscriptionAgenda($id_participant, $id_agenda)
    {
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa WHERE pa.participant=$id_participant AND pa.agenda=$id_agenda";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
        
        
    public function getInscritsAgenda($id_agenda, $participants)
    {
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa WHERE pa.participant in ($participants) AND pa.agenda=$id_agenda";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
