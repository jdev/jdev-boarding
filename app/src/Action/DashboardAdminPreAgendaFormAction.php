<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminPreAgendaFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
    
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm')) {
            return $response->withStatus(401);
        }
    
        if ($request->isGet()) {
            $formations = $this->getFormations();
            
            $a = [
                'page'  => 'dashboard',
                'token' => $token,
                'role_si' => $roleSI,
                'formations' => $formations,
                'jdev' => $this->settings['jdev']
            ];
            
            if (array_key_exists('preagenda_id', $params)) {
                $preagenda = $this->em->find('App\Entity\PreAgenda', $params['preagenda_id']);
                $formations = $this->getOneFormation($params['preagenda_id']);
                $a['preagenda'] = $preagenda;
            }
            
            if (array_key_exists('quota', $params)) {
                $parsedBody = $request->getParsedBody();
                $debut = date_create_from_format('d/m/Y H:i', $params['jour'] . ' ' . $params['debut']);
                $fin = date_create_from_format('d/m/Y H:i', $params['jour'] . ' ' . $params['fin']);
                $quota = $params['quota'];
                $salles = $this->getSalles($debut->format('Y-m-d H:i:s'), $fin->format('Y-m-d H:i:s'), $quota);
                $response = $response->write(json_encode($salles))->withHeader('Content-type', 'application/json');
                return $response;
            } else {
                $this->view->render($response, 'dashboard_admin_preagenda_form.twig', $a);
            }
        }
        
        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
    
            $formation = $this->em->find('App\Entity\Formation', $parsedBody['formation']);
            $debut = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['debut']);
            $fin = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['fin']);
            $salle = $this->em->find('App\Entity\Salle', $parsedBody['salle']);
                
            $preagenda = new \App\Entity\PreAgenda();
            $preagenda->setFormation($formation);
            $preagenda->setSalle($salle);
            $preagenda->setDateDebut($debut);
            $preagenda->setDateFin($fin);
            $preagenda->setDisplay($parsedBody['ordre']);
            $this->em->persist($preagenda);
            $this->em->flush();

            $response = $response->write('Nouvel preagenda enregistré')->withStatus(201);
        }
        
        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();
    
            $formation = $this->em->find('App\Entity\Formation', $parsedBody['formation']);
            $salle = $this->em->find('App\Entity\Salle', $parsedBody['salle']);
    
            $debut = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['debut']);
            $fin = date_create_from_format('d/m/Y H:i', $parsedBody['jour'] . ' ' . $parsedBody['fin']);
    
            $preagenda = $this->em->find('App\Entity\PreAgenda', $parsedBody['preagenda_id']);
            $preagenda->setFormation($formation);
            $preagenda->setSalle($salle);
            $preagenda->setDateDebut($debut);
            $preagenda->setDateFin($fin);
            $preagenda->setDisplay($parsedBody['ordre']);
            $this->em->flush();
        
            $response = $response->write('PreAgenda n°' . $preagenda->getId() . ' modifié')->withStatus(201);
        }
    
        if ($request->isDelete()) {
            $preagenda = $this->em->find('App\Entity\PreAgenda', $params['preagenda_id']);
            $this->em->remove($preagenda);
            $this->em->flush();
            $response = $response->write('PreAgenda id: ' . $params['preagenda_id'] . ' supprimé')->withStatus(200);
        }
        
        return $response;
    }
   
    private function getFormations()
    {
        $dql = "SELECT f FROM App\Entity\Formation f LEFT JOIN f.preagendas a where  a.dateDebut IS NULL ORDER BY f.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getOneFormation($id_formation)
    {
        $dql = "SELECT f FROM App\Entity\Formation f where f.id=$id_formation";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    private function getSalles($debut, $fin, $quota)
    {
        $dql  = "SELECT distinct s FROM App\Entity\Salle s LEFT JOIN  s.preagendas a  ";
        $dql .= "WHERE s.quotaPhysique >= $quota AND s.id NOT IN  ";
        $dql .= " ( select s1.id FROM App\Entity\Salle s1 LEFT JOIN s1.preagendas a1   ";
        $dql .= "   where a1.dateDebut between '$debut' and '$fin' and  a1.dateFin between '$debut' and '$fin' )";
        $dql .= " ORDER BY s.quotaPhysique";
        $query = $this->em->createQuery($dql);
        return $query->getArrayResult();
    }
}
