<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminAddEditSalleAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }


    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin add edit salle page action dispatched");
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }

        if ($request->isGet()) {
            $a = [
                'page'  => 'dashboard-admin-add-edit-salle',
                'token' => $token,
                'role_si' => $roleSI,
                'jdev' => $this->settings['jdev']
            ];
            if (array_key_exists('salle_id', $params)) {
                $salle = $this->em->find('App\Entity\Salle', $params['salle_id']);
                $a['salle'] = $salle;
            };
            $this->view->render($response, 'dashboard_admin_add_edit_salle.twig', $a);
        }

        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();

            $salle = new \App\Entity\Salle();
            $salle->setNom($parsedBody['nom']);
            $salle->setEtage($parsedBody['etage']);
            $salle->setAile($parsedBody['aile']);
            $salle->setWifi($parsedBody['wifi']);
            $salle->setReseau($parsedBody['reseau']);
            $salle->setVideoprojecteur($parsedBody['videoprojecteur']);
            $salle->setDescription($parsedBody['description']);
            $salle->setQuotaOfficiel($parsedBody['quota_officiel']);
            $salle->setQuotaPhysique($parsedBody['quota_physique']);
            $this->em->persist($salle);
            $this->em->flush();

            $response = $response->write('Nouvelle salle enregistrée')->withStatus(201);
        }

        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();

            $salle = $this->em->find('App\Entity\Salle', $parsedBody['salle_id']);
            $salle->setNom($parsedBody['nom']);
            $salle->setEtage($parsedBody['etage']);
            $salle->setAile($parsedBody['aile']);
            $salle->setWifi($parsedBody['wifi']);
            $salle->setReseau($parsedBody['reseau']);
            $salle->setVideoprojecteur($parsedBody['videoprojecteur']);
            $salle->setDescription($parsedBody['description']);
            $salle->setQuotaOfficiel($parsedBody['quota_officiel']);
            $salle->setQuotaPhysique($parsedBody['quota_physique']);
            $this->em->flush();

            $response = $response->write('Salle n°' . $salle->getId() . ' modifiée')->withStatus(201);
        }

        if ($request->isDelete()) {
            $salle = $this->em->find('App\Entity\Salle', $params['salle_id']);
            $this->em->remove($salle);
            $this->em->flush();
            $response = $response->write('Salle id: ' . $params['salle_id'] . ' supprimée')->withStatus(200);
        }
        
        return $response;
    }
}
