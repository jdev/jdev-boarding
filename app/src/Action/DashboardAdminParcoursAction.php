<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminAgendaAction
{
    private $view;
    private $logger;
    private $em;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard formation page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        
        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }
        
        $agendas = $this->getAgendas();
        
        $this->view->render($response, 'dashboard_admin_agenda.twig', [
            'page'  => 'dashboard-admin-agenda',
            'token' => $token,
            'agendas' => $agendas
        ]);
        
        return $response;
    }
    
    public function getAgendas()
    {
        $dql = "SELECT a FROM App\Entity\Agenda a ORDER BY a.dateDebut ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
