<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminSondageStatAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard-participant-sondage page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        $email = $request->getAttribute('email');
        $participant = $this->getParticipant($email);
        $thematiques = $this->getThematiques();
        $formations = $this->getFormations();
        $inscriptions = $this->getSondage();
        $inscrits = $this->getInscrits();
     
        $a = [
            'page'  => 'dashboard-participant-sondage',
            'token' => $token,
            'role_si' => $roleSI,
            'participant' => $participant,
            'thematiques' => $thematiques,
            'formations' => $formations,
            'inscriptions' => $inscriptions,
            'tot_inscrits' => count($inscrits),
            'jdev' => $this->settings['jdev']
        ];

        if (($roleSI === 'admin') || ($roleSI === 'clo') || ($roleSI === 'clo_pgm')) {
            $this->view->render($response, 'dashboard_admin_sondage_stat.twig', $a);
        }
        
        return $response;
    }
    
    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
    
    private function getThematiques()
    {
        $dql = "SELECT t FROM App\Entity\Thematique t ORDER BY t.id";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getFormations()
    {
        $dql = "SELECT f FROM App\Entity\Formation f ORDER BY f.thematique,f.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    public function getSondage()
    {
        $dql  = "SELECT ps from App\Entity\ParticipantSondage ps ";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    public function getInscrits()
    {
        $dql  = "SELECT distinct(ps.participant) from App\Entity\ParticipantSondage ps";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
