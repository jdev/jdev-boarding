<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminSessionAgendaAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin session agenda action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm') && ($roleSI != 'clo')) {
            return $response->withStatus(401);
        }
        
        $agenda = $this->em->find('App\Entity\Agenda', $params['agenda_id']);
        $participants = $agenda->getParticipants();
        $formation = $agenda->getFormation();
        $intervenants = $this->getIntervenants();

        $a = [
            'page'  => 'dashboard-admin-session-agenda',
            'token' => $token,
            'role_si' => $roleSI,
            'agenda' => $agenda,
            'formation' => $formation,
            'participants' => $participants,
            'intervenants' => $intervenants,
            'jdev' => $this->settings['jdev']
        ];
            
        $this->view->render($response, 'dashboard_admin_session_agenda.twig', $a);
        return $response;
    }
    
    
    private function getIntervenants()
    {
        $dql = "SELECT distinct p.nom,p.id,p.prenom FROM App\Entity\ParticipantContribution pc  LEFT JOIN  pc.participant p where pc.contribution != 5 ORDER BY p.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
