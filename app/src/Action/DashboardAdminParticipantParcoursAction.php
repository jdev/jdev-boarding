<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminParticipantParcoursAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        $intervenants = $this->getIntervenants();
    
        if (($roleSI != 'admin') && ($roleSI != 'clo')) {
            return $response->withStatus(401);
        }
    
        $a = [
            'token' => $token,
            'role_si' => $roleSI,
            'intervenants' => $intervenants,
            'jdev' => $this->settings['jdev']
        ];
            
        if (array_key_exists('id', $params)) {
            $participant = $this->em->find('App\Entity\Participant', $params['id']);
            $a['participant'] = $participant;
        } else {
            return $response->withStatus(400);
        }

        $a['pa_am'][0] = $this->getParcours($this->settings['jdev']['j1'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][0] = $this->getParcours($this->settings['jdev']['j1'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][1] = $this->getParcours($this->settings['jdev']['j2'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][1] = $this->getParcours($this->settings['jdev']['j2'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][2] = $this->getParcours($this->settings['jdev']['j3'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][2] = $this->getParcours($this->settings['jdev']['j3'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][3] = $this->getParcours($this->settings['jdev']['j4'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][3] = $this->getParcours($this->settings['jdev']['j4'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][4] = $this->getParcours($this->settings['jdev']['j5'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][4] = $this->getParcours($this->settings['jdev']['j5'], '12:00:00', '19:00:00', $participant->getId());


        if (array_key_exists('jour', $params)) {
            $agendas = $this->getAgendas($params['jour'], $params['debut'], $params['fin'], $participant->getId());
            $a['agendas'] = $agendas;
            $nbGt17 = $this->getGt($params['jour'], $participant->getId());
            $a['nbgt17'] = count($nbGt17);
            $inscription = $this->getInscription($params['jour'], $params['debut'], $params['fin'], $participant->getId());
            $nbInscriptions = count($inscription);
            if ($nbInscriptions > 0) {
                $a['inscription'] = $inscription[0];
            }
            $a['nb_inscriptions'] = $nbInscriptions;
            $a['jour'] = $params['jour'];
            $a['debut'] = $params['debut'];
            $a['fin'] = $params['fin'];
            if ($params['debut'] == '08:00:00') {
                $a['journee'] = ' - Matin';
            }
            if ($params['debut'] == '13:00:00') {
                $a['journee'] = ' - Après-midi';
            }
        }
        $this->view->render($response, 'dashboard_admin_participant_parcours.twig', $a);
        return $response;
    }
    
    //select *,
    // (select count(*) from participant_x_agenda pa where pa.id_agenda=a.id) as nb_inscrits,
    // (select count(*) from participant_x_agenda p2 where p2.id_agenda=a.id and p2.id_participant=100) as check from agenda a;
    public function getAgendas($jour, $debut, $fin, $id)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where pa.agenda=a.id) as nb_inscrits, ";
        $dql .= "(select count(p2) from App\Entity\ParticipantAgenda p2 where p2.agenda=a.id and p2.participant=$id) as check, ";
        $dql .= "(select count(p3) from App\Entity\ParticipantAgenda p3 LEFT JOIN  p3.agenda ag where p3.participant=$id and ag.dateDebut between '$date_debut' and '$date_fin' and  ag.dateFin between '$date_debut' and '$date_fin'  as inscrit ";
        $dql .= "FROM App\Entity\Agenda a ";
        $dql .= "WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin' ORDER BY a.display, a.dateDebut ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    public function getParcours($jour, $debut, $fin, $id)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . "  " . $fin;
        
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa LEFT JOIN pa.agenda a ";
        $dql .= "WHERE pa.participant=$id ";
        $dql .= "AND a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin' ORDER BY a.dateDebut ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
 
    public function getGt($jour, $id)
    {
        $date_debut = $jour . " 17:00:00";
        $date_fin = $jour . " 20:00:00";
        
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa LEFT JOIN pa.agenda a ";
        $dql .= "WHERE pa.participant=$id ";
        $dql .= "AND a.dateDebut between '$date_debut' and '$date_fin'";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    public function getInscription($jour, $debut, $fin, $id)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        $date_gtsoir = $jour . " 17:00:00";
        
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa LEFT JOIN pa.agenda a ";
        $dql .= "WHERE pa.participant=$id ";
        $dql .= "AND a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin'";
        $dql .= "AND a.dateDebut < '$date_gtsoir'";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
            
    private function getIntervenants()
    {
        $dql = "SELECT distinct p.nom,p.id,p.prenom FROM App\Entity\ParticipantContribution pc  LEFT JOIN  pc.participant p where pc.contribution != 5 ORDER BY p.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
