<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminGenCodeFormationAction
{
    private $logger;
    private $em;
    
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->em = $em;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin generation code formation");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        
        if (($roleSI != 'admin')) {
            return $response->withStatus(401);
        }
       
        $agendaRepository = $this->em->getRepository('App\Entity\Agenda');
        $agendas = $agendaRepository->findAll();
        foreach ($agendas as $agenda) {
            if (empty($agenda->getCodeFormation())) {
                $agenda->setCodeFormation(uniqid());
            }
        }
        $this->em->flush();
        
        return $response->write('ok');
    }

    public function getAgendas()
    {
        $dql  = "SELECT agenda ";
        $dql .= "FROM App\Entity\Agenda a ";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
