<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardParticipantSessionsRefreshAction
{
    private $logger;
    private $em;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard-participant-sessions-refresh page action dispatched");

        $params = $request->getQueryParams();
        $data = array();
        
        if (!array_key_exists('jour', $params) || !array_key_exists('debut', $params) || !array_key_exists('fin', $params)) {
            $agendas = $this->getAllAgendas();
            $inscrits = $this->getInscrits();
            $data['tot_inscrits'] = count($inscrits);
        } else {
            $agendas = $this->getAgendas($params['jour'], $params['debut'], $params['fin']);
        }
        $aAgendas = array();
        foreach ($agendas as $agenda) {
            $aAgendas[] = array(
                'id' => $agenda['agenda']->getId(),
                'nb_inscrits' => $agenda['nb_inscrits'],
                'quota' => $agenda['agenda']->getFormation()->getQuota()
            );
        }
        $data['agendas'] = $aAgendas;

        return $response->withJson($data);
    }

    private function getAgendas($jour, $debut, $fin)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where pa.agenda=a.id) as nb_inscrits ";
        $dql .= "FROM App\Entity\Agenda a ";
        $dql .= "WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin'";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    private function getAllAgendas()
    {
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where pa.agenda=a.id) as nb_inscrits ";
        $dql .= "FROM App\Entity\Agenda a ";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
            
    public function getInscrits()
    {
        $dql  = "SELECT distinct(pa.participant) from App\Entity\ParticipantAgenda pa";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
