<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminParticipantParcoursFormationAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $id_agenda = (int) $params['agenda'];
        $valid = $params['valid'];
        $roleSI = $request->getAttribute('roleSI');
        
        $id_participant = $params['id'];
        $participant = $this->em->find('App\Entity\Participant', $params['id']);

        
        // Suppression de l'inscription à la formation
        if ($valid == 'false') {
            $inscription = $this->getInscriptionAgenda($id_participant, $id_agenda);

            $this->em->remove($inscription[0]);
            $this->em->flush();
        }
        // Inscription à la formation
        if ($valid == 'true') {
            $allParticipantAgenda = $this->getParcours($params['jour'], $params['debut'], $params['fin'], $id_participant);
            $agenda = $this->getAgenda($id_agenda);
            if (count($allParticipantAgenda) < 1 || $agenda[0]->getFormation()->getType() == 'Groupe de Travail') {
                $inscription = new \App\Entity\ParticipantAgenda();
                $inscription->setParticipant($participant);
                $inscription->setAgenda($agenda[0]);
                date_default_timezone_set('UTC');
                $ladate = date_create_from_format('Y-m-d H:i:s', date("Y-m-d H:i:s"));
                $inscription->setDateInscription($ladate);

                $this->em->persist($inscription);
                $this->em->flush();
            }
        }
 
        $a = [
            'page'  => 'dashboard',
            'token' => $token,
            'participant' => $participant,
            
        ];
        
//        $a['pa_4_am'] = $this->getParcours('2017-07-04', '08:00:00', '13:00:00', $participant->getId());
//        $a['pa_4_pm'] = $this->getParcours('2017-07-04', '12:00:00', '19:00:00', $participant->getId());
//        $a['pa_5_am'] = $this->getParcours('2017-07-05', '08:00:00', '13:00:00', $participant->getId());
//        $a['pa_5_pm'] = $this->getParcours('2017-07-05', '12:00:00', '19:00:00', $participant->getId());
//        $a['pa_6_am'] = $this->getParcours('2017-07-06', '08:00:00', '13:00:00', $participant->getId());
//        $a['pa_6_pm'] = $this->getParcours('2017-07-06', '12:00:00', '19:00:00', $participant->getId());
//        $a['pa_7_am'] = $this->getParcours('2017-07-07', '08:00:00', '13:00:00', $participant->getId());
//        $a['pa_7_pm'] = $this->getParcours('2017-07-07', '12:00:00', '19:00:00', $participant->getId());

        // Attention chgt 2020 !!!! 
        $a['pa_am'][0] = $this->getParcours($this->settings['jdev']['j1'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][0] = $this->getParcours($this->settings['jdev']['j1'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][1] = $this->getParcours($this->settings['jdev']['j2'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][1] = $this->getParcours($this->settings['jdev']['j2'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][2] = $this->getParcours($this->settings['jdev']['j3'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][2] = $this->getParcours($this->settings['jdev']['j3'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][3] = $this->getParcours($this->settings['jdev']['j4'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][3] = $this->getParcours($this->settings['jdev']['j4'], '12:00:00', '19:00:00', $participant->getId());
        $a['pa_am'][4] = $this->getParcours($this->settings['jdev']['j5'], '08:00:00', '13:00:00', $participant->getId());
        $a['pa_pm'][4] = $this->getParcours($this->settings['jdev']['j5'], '12:00:00', '19:00:00', $participant->getId());
        
        $this->view->render($response, 'dashboard_participant_parcours.twig', $a);
        return $response;
    }
    
    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $email));
        if (isset($participant)) {
            return $participant;
        } else {
            return false;
        }
    }
    
        
    public function getInscriptionAgenda($id_participant, $id_agenda)
    {
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa WHERE pa.participant=$id_participant AND pa.agenda = $id_agenda";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getAgenda($id_agenda)
    {
        $dql  = "SELECT a FROM App\Entity\Agenda a WHERE a.id=$id_agenda";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    public function getParcours($jour, $debut, $fin, $id)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT pa FROM App\Entity\ParticipantAgenda pa LEFT JOIN pa.agenda a ";
        $dql .= "WHERE pa.participant=$id ";
        $dql .= "AND a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin'";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
