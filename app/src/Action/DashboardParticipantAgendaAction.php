<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardParticipantAgendaAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard formation page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        
        $thematiques = $this->getThematiques();
        $intervenants = $this->getIntervenants();

        $a = [
            'token' => $token,
            'role_si' => $roleSI,
            'thematiques' => $thematiques,
            'intervenants' => $intervenants,
            'jdev' => $this->settings['jdev']
        ];

        if (array_key_exists('jour', $params)) {
            $a['jour'] = $params['jour'];
            if ($params['debut'] == '08:00:00') {
                $a['journee'] = ' - Matin';
            }
            if ($params['debut'] == '13:00:00') {
                $a['journee'] = ' - Après-midi';
            }
                
            $agendas = $this->getAgendas($params['jour'], $params['debut'], $params['fin']);
            $a['agendas'] = $agendas;
        }

        $a['pa_am'][0] = $this->getAgendas($this->settings['jdev']['j1'], '08:00:00', '13:00:00');
        $a['pa_pm'][0] = $this->getAgendas($this->settings['jdev']['j1'], '12:00:00', '19:00:00');
        $a['pa_am'][1] = $this->getAgendas($this->settings['jdev']['j2'], '08:00:00', '13:00:00');
        $a['pa_pm'][1] = $this->getAgendas($this->settings['jdev']['j2'], '12:00:00', '19:00:00');
        $a['pa_am'][2] = $this->getAgendas($this->settings['jdev']['j3'], '08:00:00', '13:00:00');
        $a['pa_pm'][2] = $this->getAgendas($this->settings['jdev']['j3'], '12:00:00', '19:00:00');
        $a['pa_am'][3] = $this->getAgendas($this->settings['jdev']['j4'], '08:00:00', '13:00:00');
        $a['pa_pm'][3] = $this->getAgendas($this->settings['jdev']['j4'], '12:00:00', '19:00:00');
        $a['pa_am'][4] = $this->getAgendas($this->settings['jdev']['j5'], '08:00:00', '13:00:00');
        $a['pa_pm'][4] = $this->getAgendas($this->settings['jdev']['j5'], '12:00:00', '19:00:00');
        
        $this->view->render($response, 'dashboard_participant_agenda.twig', $a);
        return $response;
    }

    public function getAgendas($jour, $debut, $fin)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where pa.agenda=a.id) as nb_inscrits ";
        $dql .= "FROM App\Entity\Agenda a ";
        $dql .= "WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin' ORDER BY a.display, a.dateDebut ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    private function getThematiques()
    {
        $dql = "SELECT t FROM App\Entity\Thematique t ORDER BY t.label";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
            
    private function getIntervenants()
    {
        $dql = "SELECT distinct p.nom,p.id,p.prenom FROM App\Entity\ParticipantContribution pc  LEFT JOIN  pc.participant p where pc.contribution != 5 ORDER BY p.nom ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
