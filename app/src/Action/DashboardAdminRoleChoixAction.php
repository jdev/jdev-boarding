<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminRoleChoixAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("admin valid role page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }

        $participant = $this->getParticipant($params['email']);
        if (!$participant) {
            return $response->withStatus(400);
        }
        $roleId = $params['role_orga'];
        if ($roleId) {
            $role = $this->em->find('App\Entity\Role', $params['role_orga']);
            $participant->setRoleOrga($role->getId());
        } else {
            $participant->setRoleOrga(null);
        }
        $participant->setRoleSI($params['role_si']);
        
        $this->em->persist($participant);
        $this->em->flush();
        
        return $response;
    }

    private function getParticipant($email)
    {
        $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => strtolower($email)));
        if (isset($participant)) {
            return $participant;
        } else {
            return true;
        }
    }
}
