<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class ValidEmailAction
{
    private $view;
    private $logger;
    private $em;
    private $mailer;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $mailer, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Valid email action dispatched");

        $params = $request->getQueryParams();

        if (isset($params['key']) && isset($params['email'])) {
            $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $params['email']));
            if (isset($participant) && $participant->getEmailValide()) {
                $this->view->render($response, 'connexion.twig', [
                    'page' => 'connexion',
                    'info' => 'Votre compte ' . $this->settings['label'] . ' est déjà actif',
                    'jdev' => $this->settings['jdev']
                ]);
            } else if (isset($participant) && $participant->getCleEmail() == $params['key']) {
                // Gestion place pré-payée
                $organisme = $participant->getOrganisme();
                if ($organisme->getNbLibres() > 0 && $participant->getRole() == 'Participant') {
                    $participant->setAccesValide(true);  // Le compte est activé car il est pris en charge dans les pass prepayes
                    $participant->setPassPrepaye(true);  // Le paiement à été pris en charge par l'organisme
                    $organisme->setNbLibres($organisme->getNbLibres() - 1);
                }
                $participant->setEmailValide(true);
                $participant->setDateActivation(new \DateTime("now"));
                $this->em->flush();
                
                if (!$participant->getCloValide()) {
                    $this->sendEmailValidClo($participant);
                } elseif ($participant->getPassPrepaye()) {
                    $this->sendEmailPassPrepaye($participant);
                } else {
                    if ($participant->getRole() == 'Accompagnant') {
                        $this->sendEmailAccompagnant($participant);
                    } else {
                        $this->sendEmailAzurColloque($participant);
                    }
                }
                $this->view->render($response, 'connexion.twig', [
                    'page' => 'connexion',
                    'info' => 'E-mail validé. Vous pouvez maintenant vous connecter',
                    'jdev' => $this->settings['jdev']
                ]);
            } else {
                $this->view->render($response, 'valid_email.twig', [
                    'page' => 'valid_email',
                    'info' => 'Mauvais couple E-mail/Clé',
                    'jdev' => $this->settings['jdev']
                ]);
            }
        } else {
            $this->view->render($response, 'valid_email.twig', [
                'page'   => 'valid_email',
                'jdev' => $this->settings['jdev']
            ]);
        }

        return $response;
    }

    
    private function sendEmailValidClo($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . ' (' . $participant->getOrganisme()->getLabel() . '),' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous avons acté votre inscription aux ' . $this->settings['jdev']['label'] . ' en tant que ' . $participant->getRole() . '.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Votre inscription nécessite une validation complémentaire par un membre du comité d\'organisation.' . PHP_EOL;
        $body .= 'Vous recevrez très prochainement un e-mail pour vous prévenir de la validation de votre compte.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Si dans 3 jours, vous ne voyez pas la validation de votre inscription dans votre espace personnel sur ';
        $body .= 'https://' . $this->settings['jdev']['url'] . ', merci de nous contacter par email (' . $this->settings['jdev']['email'] . '). ';
        $body .= 'Votre espace personnel vous permettra de sélectionner vos sessions ultérieurement.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'] ;

        $message = \Swift_Message::newInstance($this->settings['jdev']['label'] . ' e-mail participant vérifié')
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail(), $this->settings['jdev']['email']])
            ->setBody($body);
            
        $this->mailer->send($message);
    }

    private function sendEmailPassPrepaye($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . ' (' . $participant->getOrganisme()->getLabel() . '),' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous avons bien acté votre inscription aux ' . $this->settings['jdev']['label'] . ' et nous vous en remercions.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Vos frais d\'inscription sont pris en charge par votre organisme (' . $participant->getOrganisme()->getLabel() . '), ';
        $body .= 'sous réserve de validation par ce dernier.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Si dans 3 jours, vous ne voyez pas la validation de votre inscription dans votre espace personnel sur ';
        $body .= 'https://' . $this->settings['jdev']['url'] . ' ou si vous vous désistez, merci de nous en informer par email (' . $this->settings['jdev']['email'] . '). ';
        $body .= 'Votre espace personnel JdevBoarding vous permettra de sélectionner et d\'obtenir les informations nécessaires pour suivre vos sessions.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'En raison de la pandémie, le format des JDEV de cette édition est adapté : un premier temps fort en distanciel du 6 juillet au 10 juillet 2020.'. PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous vous invitons à consulter les liens suivant pour plus d\'information : ' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020/inscription#information-relative-%C3%A0-la-crise-sanitaire-du-covid-10' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020#agenda' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020#l-%C3%A9dition-connect%C3%A9e' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Cette nouvelle modalité vous offrira l\’avantage de pouvoir suivre plus largement le programme proposé.' . PHP_EOL;
        $body .= 'Celui-ci sera étalé temporellement. ' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement,' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'] ;

        $message = \Swift_Message::newInstance($this->settings['jdev']['label'] . ' e-mail participant vérifié')
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail(), $this->settings['jdev']['email']])
            ->setBody($body);
            
        $this->mailer->send($message);
    }

    private function sendEmailAzurColloque($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . ' (' . $participant->getOrganisme()->getLabel() . '),' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous avons bien pris acte de votre demande d\'inscription aux ' . $this->settings['jdev']['label'] . '.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Pour que votre inscription soit prise en compte, vous devez régler ';
        $body .= 'les frais d\'inscription sur la plate-forme Azur-Colloque: https://www.azur-colloque.fr/' . $this->settings['jdev']['DR']  . '/ .' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Attention, la tarification varie en fonction de la date du paiement. Nous vous prions de privilégier ';
        $body .= 'le paiement par carte bancaire avant le 23 juin 2020. Votre inscription sera validée à l\'issue de votre paiement.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Si dans 3 jours, vous ne voyez pas la validation de votre inscription dans votre espace personnel sur ';
        $body .= 'https://' . $this->settings['jdev']['url'] . ' ou si vous vous désistez, merci de nous en informer par email (' . $this->settings['jdev']['email'] . '). ';
        $body .= 'Votre espace personnel JdevBoarding vous permettra de sélectionner et d\'obtenir les informations névcessaires pour suivre vos sessions une fois votre paiement pris en compte.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'En raison de la pandémie, le format des JDEV de cette édition est adapté : un premier temps fort en distanciel du 6 juillet au 10 juillet 2020' . PHP_EOL;
        $body .= 'Nous vous invitons à consulter les liens suivant pour plus d\'information :' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020/inscription#information-relative-%C3%A0-la-crise-sanitaire-du-covid-10' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020#agenda' . PHP_EOL;
        $body .= ' - http://devlog.cnrs.fr/jdev2020#l-%C3%A9dition-connect%C3%A9e' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Cette nouvelle modalité vous offrira l\’avantage de pouvoir suivre plus largement le programme proposé.' . PHP_EOL;
        $body .= 'Celui-ci sera étalé temporellement. ' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement,' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'] ;
        $message = \Swift_Message::newInstance($this->settings['jdev']['label'] . ' e-mail participant vérifié')
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail(), $this->settings['jdev']['email']])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
    
    private function sendEmailAccompagnant($participant)
    {
        $body  = 'Bonjour ' . $participant->getPrenom() . ' ' . $participant->getNom() . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous avons bien pris acte de votre demande d\'inscription en tant qu\'Accompagnant aux ' . $this->settings['jdev']['label']  . '.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Pour que votre inscription soit prise en compte, vous devez régler ';
        $body .= 'les frais d\'inscription sur la plate-forme Azur-Colloque: https://www.azur-colloque.fr/' . $this->settings['jdev']['DR']  . '/ .' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Nous vous prions de privilégier ';
        $body .= 'le paiement par carte bancaire avant le 23 juin 2020. Votre inscription sera validée à l\'issue de votre paiement.' . PHP_EOL;
        $body .= PHP_EOL;
        $body .= 'Bien cordialement' . PHP_EOL;
        $body .= 'Le comité d\'organisation des ' . $this->settings['jdev']['label'];

        $message = \Swift_Message::newInstance($this->settings['jdev']['label'] . ' e-mail participant vérifié')
            ->setFrom(['noreply@' . $this->settings['jdev']['url'] => $this->settings['jdev']['url']])
            ->setTo([$participant->getEmail(), $this->settings['jdev']['email']])
            ->setBody($body);
            
        $this->mailer->send($message);
    }
}
