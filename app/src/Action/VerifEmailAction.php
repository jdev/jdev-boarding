<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class VerifEmailAction
{
    private $view;
    private $logger;
    private $em;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Verif email action dispatched");
        
        if ($request->isGet()) {
            $bool = 'false';
            $params = $request->getQueryParams();

            if (isset($params['email'])) {
                $participant = $this->em->getRepository('App\Entity\Participant')->findOneBy(array('email' => $params['email']));
                if (!isset($participant)) {
                    $bool = 'true';
                }
            }

            $response = $response->write($bool)->withHeader('Content-type', 'application/json');
        }

        return $response;
    }
}
