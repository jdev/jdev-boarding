<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminParticipantGestionAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if (($roleSI != 'clo') && ($roleSI != 'admin')  && ($roleSI != 'clo_admin')) {
            return $response->withStatus(401);
        }

        $participants = $this->getParticipants();
        $invalides = $this->getInvalidParticipants();
        
        $this->view->render($response, 'dashboard_admin_participant_gestion.twig', [
            'page'  => 'dashboard-admin-participant-gestion',
            'token' => $token,
            'role_si' => $roleSI,
            'participants' => $participants,
            'invalides' => $invalides,
            'jdev' => $this->settings['jdev']
        ]);
    
        return $response;
    }

    private function getParticipants()
    {
        $dql = "SELECT p as participant, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantAgenda pa where p.id=pa.participant) as inscrit, ";
        $dql .= "(select count(pc) from App\Entity\ParticipantContribution pc where p.id=pc.participant and pc.contribution != 5) as contribution, ";
        $dql .= "(select count(pp) from App\Entity\ParticipantContribution pp where p.id=pp.participant and pp.contribution = 5) as poster ";
        $dql .= " FROM App\Entity\Participant p";
        $dql .= " WHERE p.emailValide='t' ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    private function getInvalidParticipants()
    {
        $dql = "SELECT p";
        $dql .= " FROM App\Entity\Participant p";
        $dql .= " WHERE p.emailValide='f' ORDER BY p.nom";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
