<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminThematiqueFormAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
 
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin thematique page action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');

        if ($roleSI != 'admin') {
            return $response->withStatus(401);
        }

        if ($request->isGet()) {
            $a = [
                'page'  => 'dashboard-admin-thematique-form',
                'token' => $token,
                'role_si' => $roleSI,
                'jdev' => $this->settings['jdev']
            ];
            if (array_key_exists('thematique_id', $params)) {
                $thematique = $this->em->find('App\Entity\Thematique', $params['thematique_id']);
                $a['thematique'] = $thematique;
            }
            $this->view->render($response, 'dashboard_admin_thematique_form.twig', $a);
        }

        if ($request->isPost()) {
            $parsedBody = $request->getParsedBody();
            $thematique = new \App\Entity\Thematique();
            $thematique->setNom($parsedBody['nom']);
            $thematique->setLabel($parsedBody['label']);
            $this->em->persist($thematique);
            $this->em->flush();
            $response = $response->write('Nouvelle thematique enregistrée')->withStatus(201);
        }

        if ($request->isPut()) {
            $parsedBody = $request->getParsedBody();
            $thematique = $this->em->find('App\Entity\Thematique', $parsedBody['thematique_id']);
            $thematique->setNom($parsedBody['nom']);
            $thematique->setLabel($parsedBody['label']);
            $this->em->flush();
            $response = $response->write('Thematique n°' . $thematique->getId() . ' modifiée')->withStatus(201);
        }

        if ($request->isDelete()) {
            $thematique = $this->em->find('App\Entity\Thematique', $params['thematique_id']);
            $this->em->remove($thematique);
            $this->em->flush();
            $response = $response->write('Thematique id: ' . $params['thematique_id'] . ' supprimée')->withStatus(200);
        }
        return $response;
    }
}
