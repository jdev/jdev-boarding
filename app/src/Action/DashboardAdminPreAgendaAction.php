<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminPreAgendaAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;
    
    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }
    
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard formation page action dispatched");
        
        $params = $request->getQueryParams();
        $token = $params['token'];
        $roleSI = $request->getAttribute('roleSI');
        
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm') && ($roleSI != 'clo')) {
            return $response->withStatus(401);
        }

        $a = [
            'token' => $token,
            'role_si' => $roleSI,
            'jdev' => $this->settings['jdev']
        ];

        if (array_key_exists('jour', $params)) {
            $j = explode("-", $params['jour']);
            $a['jour'] = $params['jour'];
            if ($params['debut'] == '08:00:00') {
                $a['journee'] = ' - Matin';
            }
            if ($params['debut'] == '13:00:00') {
                $a['journee'] = ' - Après-midi';
            }
                
            $preagendas = $this->getPreAgendas($params['jour'], $params['debut'], $params['fin']);
            $a['preagendas'] = $preagendas;
        }

        $a['pa_4_am'] = $this->getPreAgendas($this->settings['jdev']['j1'], '08:00:00', '13:00:00');
        $a['pa_4_pm'] = $this->getPreAgendas($this->settings['jdev']['j1'], '12:00:00', '19:00:00');
        $a['pa_5_am'] = $this->getPreAgendas($this->settings['jdev']['j2'], '08:00:00', '13:00:00');
        $a['pa_5_pm'] = $this->getPreAgendas($this->settings['jdev']['j2'], '12:00:00', '19:00:00');
        $a['pa_6_am'] = $this->getPreAgendas($this->settings['jdev']['j3'], '08:00:00', '13:00:00');
        $a['pa_6_pm'] = $this->getPreAgendas($this->settings['jdev']['j3'], '12:00:00', '19:00:00');
        $a['pa_7_am'] = $this->getPreAgendas($this->settings['jdev']['j4'], '08:00:00', '13:00:00');
        $a['pa_7_pm'] = $this->getPreAgendas($this->settings['jdev']['j4'], '12:00:00', '19:00:00');
        $a['pa_8_am'] = $this->getPreAgendas($this->settings['jdev']['j5'], '08:00:00', '13:00:00');
        $a['pa_8_pm'] = $this->getPreAgendas($this->settings['jdev']['j5'], '12:00:00', '19:00:00');
        $tot_nb_inscrits = $this->getNbInscriptions();
        $a['tot_nb_inscrits'] = $tot_nb_inscrits[0]['nb_inscrits'];
        
        $this->view->render($response, 'dashboard_admin_preagenda.twig', $a);
        
        return $response;
    }
    
    public function getNbInscriptions()
    {
        $dql = "select count(pa) as nb_inscrits from App\Entity\ParticipantPreAgenda pa";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
    
    public function getPreAgendas($jour, $debut, $fin)
    {
        $date_debut = $jour . " " . $debut;
        $date_fin = $jour . " " . $fin;
        $dql  = "SELECT a as agenda, ";
        $dql .= "(select count(pa) from App\Entity\ParticipantPreAgenda pa where pa.preagenda=a.id) as nb_inscrits ";
        $dql .= "FROM App\Entity\PreAgenda a ";
        $dql .= "WHERE a.dateDebut between '$date_debut' and '$date_fin' and  a.dateFin between '$date_debut' and '$date_fin' ORDER BY a.display,a.dateDebut ASC";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
