<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class InfoGeneralesAction
{
    private $view;
    private $logger;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Info generales page action dispatched");
        
        $this->view->render($response, 'info_generales.twig', [
            'page' => 'info_generales',
            'jdev' => $this->settings['jdev']
        ]);
        return $response;
    }
}
