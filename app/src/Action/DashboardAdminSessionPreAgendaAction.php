<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class DashboardAdminSessionPreAgendaAction
{
    private $view;
    private $logger;
    private $em;
    private $settings;

    public function __construct(Twig $view, LoggerInterface $logger, EntityManagerInterface $em, $settings)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("dashboard admin session pre-agenda action dispatched");

        $params = $request->getQueryParams();
        $token = $params['token'];
        $formation = $this->em->find('App\Entity\Formation', $params['formation_id']);
        $roleSI = $request->getAttribute('roleSI');

        
        if (($roleSI != 'admin') && ($roleSI != 'clo_pgm') && ($roleSI != 'clo')) {
            return $response->withStatus(401);
        }
        
        $a = [
            'page'  => 'dashboard-admin-emargement-liste',
            'token' => $token,
            'role_si' => $roleSI,
            'formation' => $formation,
            'jdev' => $this->settings['jdev']
        ];

        $participants = $this->getAllParticipantAgenda($params['formation_id']);
        $a['participants'] = $participants;
            
        $this->view->render($response, 'dashboard_admin_session_pre_agenda.twig', $a);
        return $response;
    }
    
    public function getAllParticipantAgenda($id)
    {
        $dql  = "SELECT pa from App\Entity\ParticipantPreAgenda pa LEFT JOIN  pa.preagenda a ";
        $dql .= "WHERE a.formation=$id";
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
