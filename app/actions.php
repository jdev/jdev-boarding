<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------
$container[App\Action\HomeAction::class] = function ($c) {
    return new App\Action\HomeAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\VerifEmailAction::class] = function ($c) {
    return new App\Action\VerifEmailAction($c->get('view'), $c->get('logger'), $c->get('em'));
};

$container[App\Action\ValidEmailAction::class] = function ($c) {
    return new App\Action\ValidEmailAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('mailer'), $c->get('settings'));
};

$container[App\Action\ProgrammeAction::class] = function ($c) {
    return new App\Action\ProgrammeAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\ContactAction::class] = function ($c) {
    return new App\Action\ContactAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\CommunicationAction::class] = function ($c) {
    return new App\Action\CommunicationAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\InfoAccesAction::class] = function ($c) {
    return new App\Action\InfoAccesAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\InfoHebergementAction::class] = function ($c) {
    return new App\Action\InfoHebergementAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\InfoGeneralesAction::class] = function ($c) {
    return new App\Action\InfoGeneralesAction($c->get('view'), $c->get('logger'), $c->get('settings'));
};

$container[App\Action\NewPasswdAction::class] = function ($c) {
    return new App\Action\NewPasswdAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('mailer'), $c->get('settings'));
};

$container[App\Action\ChangePasswdAction::class] = function ($c) {
    return new App\Action\ChangePasswdAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('mailer'), $c->get('settings'));
};

$container[App\Action\InscriptionAction::class] = function ($c) {
    return new App\Action\InscriptionAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('mailer'), $c->get('settings'));
};

$container[App\Action\ConnexionAction::class] = function ($c) {
    return new App\Action\ConnexionAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

// =============================================================
// Dashboard Participant + Admin
// =============================================================

$container[App\Action\DashboardGenerateBadgeAction::class] = function ($c) {
    return new App\Action\DashboardGenerateBadgeAction($c->get('view'), $c->get('logger'), $c->get('em'));
};

$container[App\Action\DashboardGenerateBadgeListAction::class] = function ($c) {
    return new App\Action\DashboardGenerateBadgeListAction($c->get('view'), $c->get('logger'), $c->get('em'));
};

// =============================================================
// Dashboard Participant
// =============================================================

$container[App\Action\DashboardAction::class] = function ($c) {
    return new App\Action\DashboardAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardParticipantSondageAction::class] = function ($c) {
    return new App\Action\DashboardParticipantSondageAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardParticipantSondageFormationAction::class] = function ($c) {
    return new App\Action\DashboardParticipantSondageFormationAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardParticipantParcoursAction::class] = function ($c) {
    return new App\Action\DashboardParticipantParcoursAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardParticipantSessionsRefreshAction::class] = function ($c) {
    return new App\Action\DashboardParticipantSessionsRefreshAction($c->get('logger'), $c->get('em'));
};

$container[App\Action\DashboardParticipantParcoursFormationAction::class] = function ($c) {
    return new App\Action\DashboardParticipantParcoursFormationAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardParticipantAgendaAction::class] = function ($c) {
    return new App\Action\DashboardParticipantAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardParticipantPresenceAction::class] = function ($c) {
    return new App\Action\DashboardParticipantPresenceAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminGenCodeFormationAction::class] = function ($c) {
    return new App\Action\DashboardAdminGenCodeFormationAction($c->get('logger'), $c->get('em'));
};

$container[App\Action\DashboardParticipantEditFormAction::class] = function ($c) {
    return new App\Action\DashboardParticipantEditFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

// =============================================================
// Dashboard Admin
// =============================================================
$container[App\Action\DashboardAdminAction::class] = function ($c) {
    return new App\Action\DashboardAdminAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\AdminValidRoleAction::class] = function ($c) {
    return new App\Action\AdminValidRoleAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('mailer'), $c->get('settings'));
};

$container[App\Action\DashboardAdminRoleChoixAction::class] = function ($c) {
    return new App\Action\DashboardAdminRoleChoixAction($c->get('view'), $c->get('logger'), $c->get('em'));
};

$container[App\Action\DashboardAdminInscriptionAction::class] = function ($c) {
    return new App\Action\DashboardAdminInscriptionAction($c->get('view'), $c->get('logger'), $c->get('em'));
};

$container[App\Action\DashboardAdminFormationAction::class] = function ($c) {
    return new App\Action\DashboardAdminFormationAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminFormationFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminFormationFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardAdminRoleFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminRoleFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminPreAgendaAction::class] = function ($c) {
    return new App\Action\DashboardAdminPreAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminSondageStatAction::class] = function ($c) {
    return new App\Action\DashboardAdminSondageStatAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardAdminPreAgendaFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminPreAgendaFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminAgendaAction::class] = function ($c) {
    return new App\Action\DashboardAdminAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminAgendaSalleChoixAction::class] = function ($c) {
    return new App\Action\DashboardAdminAgendaSalleChoixAction($c->get('view'), $c->get('logger'), $c->get('em'));
};


$container[App\Action\DashboardAdminAgendaFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminAgendaFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminOrganismeAction::class] = function ($c) {
    return new App\Action\DashboardAdminOrganismeAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminParticipantGestionAction::class] = function ($c) {
    return new App\Action\DashboardAdminParticipantGestionAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardAdminParticipantListeAction::class] = function ($c) {
    return new App\Action\DashboardAdminParticipantListeAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminParticipantFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminParticipantFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardAdminParticipantParcoursAction::class] = function ($c) {
    return new App\Action\DashboardAdminParticipantParcoursAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardAdminEmargementAction::class] = function ($c) {
    return new App\Action\DashboardAdminEmargementAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminEmargementListeAction::class] = function ($c) {
    return new App\Action\DashboardAdminEmargementListeAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminEmargementPresenceAction::class] = function ($c) {
    return new App\Action\DashboardAdminEmargementPresenceAction($c->get('view'), $c->get('logger'), $c->get('em'));
};


$container[App\Action\DashboardAdminPaiementAction::class] = function ($c) {
    return new App\Action\DashboardAdminPaiementAction($c->get('view'), $c->get('logger'), $c->get('em'));
};

$container[App\Action\AdminValidPaiementAction::class] = function ($c) {
    return new App\Action\AdminValidPaiementAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('mailer'), $c->get('settings'));
};

$container[App\Action\DashboardAdminSalleAction::class] = function ($c) {
    return new App\Action\DashboardAdminSalleAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminAddEditSalleAction::class] = function ($c) {
    return new App\Action\DashboardAdminAddEditSalleAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminThematiqueAction::class] = function ($c) {
    return new App\Action\DashboardAdminThematiqueAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminThematiqueFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminThematiqueFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminThematiquePreAgendaAction::class] = function ($c) {
    return new App\Action\DashboardAdminThematiquePreAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminThematiqueAgendaAction::class] = function ($c) {
    return new App\Action\DashboardAdminThematiqueAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminOrganismeFormAction::class] = function ($c) {
    return new App\Action\DashboardAdminOrganismeFormAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminSessionPreAgendaAction::class] = function ($c) {
    return new App\Action\DashboardAdminSessionPreAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};

$container[App\Action\DashboardAdminSessionAgendaAction::class] = function ($c) {
    return new App\Action\DashboardAdminSessionAgendaAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'));
};


$container[App\Action\DashboardAdminAnnulerSessionAction::class] = function ($c) {
    return new App\Action\DashboardAdminAnnulerSessionAction($c->get('view'), $c->get('logger'), $c->get('em'), $c->get('settings'), $c->get('mailer'));
};

$container[App\Action\DashboardAdminParticipantParcoursFormationAction::class] = function ($c) {
    return new App\Action\DashboardAdminParticipantParcoursFormationAction($c->get('view'), $c->get('logger'), $c->get('em'));
};