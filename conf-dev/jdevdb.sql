--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11
-- Dumped by pg_dump version 10.11

-- Started on 2020-01-27 14:49:19 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13792)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3919 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 1422170)
-- Name: agenda; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.agenda (
    id integer NOT NULL,
    id_formation integer,
    id_salle integer,
    date_debut timestamp(0) without time zone NOT NULL,
    date_fin timestamp(0) without time zone NOT NULL,
    display integer,
    code_formation character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.agenda OWNER TO jdev;

--
-- TOC entry 197 (class 1259 OID 1422174)
-- Name: agenda_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.agenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agenda_id_seq OWNER TO jdev;

--
-- TOC entry 198 (class 1259 OID 1422176)
-- Name: atelier; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.atelier (
    id integer NOT NULL,
    thematique_id integer,
    salle_id integer,
    nom character varying(255) NOT NULL,
    date_debut timestamp(0) without time zone NOT NULL,
    date_fin timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.atelier OWNER TO jdev;

--
-- TOC entry 199 (class 1259 OID 1422179)
-- Name: atelier_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.atelier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atelier_id_seq OWNER TO jdev;

--
-- TOC entry 200 (class 1259 OID 1422181)
-- Name: communaute; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.communaute (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    display integer
);


ALTER TABLE public.communaute OWNER TO jdev;

--
-- TOC entry 201 (class 1259 OID 1422184)
-- Name: communaute_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.communaute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.communaute_id_seq OWNER TO jdev;

--
-- TOC entry 202 (class 1259 OID 1422186)
-- Name: contribution; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.contribution (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    display integer
);


ALTER TABLE public.contribution OWNER TO jdev;

--
-- TOC entry 203 (class 1259 OID 1422189)
-- Name: contribution_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.contribution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contribution_id_seq OWNER TO jdev;

--
-- TOC entry 204 (class 1259 OID 1422191)
-- Name: formation; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.formation (
    id integer NOT NULL,
    id_thematique integer,
    nom character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    intervenant character varying(255) NOT NULL,
    quota integer NOT NULL,
    confirme boolean NOT NULL,
    titre character varying(255) DEFAULT NULL::character varying,
    nb_intervenant integer
);


ALTER TABLE public.formation OWNER TO jdev;

--
-- TOC entry 205 (class 1259 OID 1422198)
-- Name: formation_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.formation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formation_id_seq OWNER TO jdev;

--
-- TOC entry 206 (class 1259 OID 1422200)
-- Name: organisme; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.organisme (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    nbplaces integer NOT NULL,
    nblibres integer NOT NULL
);


ALTER TABLE public.organisme OWNER TO jdev;

--
-- TOC entry 207 (class 1259 OID 1422203)
-- Name: organisme_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.organisme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organisme_id_seq OWNER TO jdev;

--
-- TOC entry 208 (class 1259 OID 1422205)
-- Name: participant; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant (
    id integer NOT NULL,
    id_organisme integer,
    prenom character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    region character varying(255) NOT NULL,
    statut character varying(255) NOT NULL,
    emploi character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    date_inscription timestamp(0) without time zone NOT NULL,
    cle_email character varying(255) NOT NULL,
    email_valide boolean NOT NULL,
    clo_valide boolean NOT NULL,
    acces_valide boolean NOT NULL,
    type_inscription character varying(255) NOT NULL,
    mailing_list boolean NOT NULL,
    evenement_social boolean NOT NULL,
    pass_prepaye boolean NOT NULL,
    unite character varying(255) NOT NULL,
    role_si character varying(255) DEFAULT NULL::character varying,
    present boolean,
    role_orga character varying(255) DEFAULT NULL::character varying,
    regime_alimentaire character varying(255) DEFAULT NULL::character varying,
    mobilite_reduite boolean,
    code_wifi boolean,
    date_activation timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.participant OWNER TO jdev;

--
-- TOC entry 209 (class 1259 OID 1422215)
-- Name: participant_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_id_seq OWNER TO jdev;

--
-- TOC entry 210 (class 1259 OID 1422217)
-- Name: participant_x_agenda; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_agenda (
    id integer NOT NULL,
    id_participant integer,
    id_agenda integer,
    date_inscription timestamp(0) without time zone NOT NULL,
    present boolean
);


ALTER TABLE public.participant_x_agenda OWNER TO jdev;

--
-- TOC entry 211 (class 1259 OID 1422220)
-- Name: participant_x_agenda_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_agenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_agenda_id_seq OWNER TO jdev;

--
-- TOC entry 212 (class 1259 OID 1422222)
-- Name: participant_x_communaute; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_communaute (
    id integer NOT NULL,
    id_participant integer,
    id_communaute integer
);


ALTER TABLE public.participant_x_communaute OWNER TO jdev;

--
-- TOC entry 213 (class 1259 OID 1422225)
-- Name: participant_x_communaute_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_communaute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_communaute_id_seq OWNER TO jdev;

--
-- TOC entry 214 (class 1259 OID 1422227)
-- Name: participant_x_contribution; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_contribution (
    id integer NOT NULL,
    id_participant integer,
    id_contribution integer
);


ALTER TABLE public.participant_x_contribution OWNER TO jdev;

--
-- TOC entry 215 (class 1259 OID 1422230)
-- Name: participant_x_contribution_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_contribution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_contribution_id_seq OWNER TO jdev;

--
-- TOC entry 216 (class 1259 OID 1422232)
-- Name: participant_x_formation; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_formation (
    id integer NOT NULL,
    id_participant integer,
    id_formation integer,
    date_inscription timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.participant_x_formation OWNER TO jdev;

--
-- TOC entry 217 (class 1259 OID 1422235)
-- Name: participant_x_formation_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_formation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_formation_id_seq OWNER TO jdev;

--
-- TOC entry 218 (class 1259 OID 1422237)
-- Name: participant_x_inscription; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_inscription (
    id integer NOT NULL,
    id_participant integer,
    id_atelier integer,
    date_inscription timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.participant_x_inscription OWNER TO jdev;

--
-- TOC entry 219 (class 1259 OID 1422240)
-- Name: participant_x_inscription_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_inscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_inscription_id_seq OWNER TO jdev;

--
-- TOC entry 220 (class 1259 OID 1422242)
-- Name: participant_x_preagenda; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_preagenda (
    id integer NOT NULL,
    id_participant integer,
    id_agenda integer,
    date_inscription timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.participant_x_preagenda OWNER TO jdev;

--
-- TOC entry 221 (class 1259 OID 1422245)
-- Name: participant_x_preagenda_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_preagenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_preagenda_id_seq OWNER TO jdev;

--
-- TOC entry 222 (class 1259 OID 1422247)
-- Name: participant_x_reseau; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_reseau (
    id integer NOT NULL,
    id_participant integer,
    id_reseau integer
);


ALTER TABLE public.participant_x_reseau OWNER TO jdev;

--
-- TOC entry 223 (class 1259 OID 1422250)
-- Name: participant_x_reseau_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_reseau_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_reseau_id_seq OWNER TO jdev;

--
-- TOC entry 224 (class 1259 OID 1422252)
-- Name: participant_x_role; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.participant_x_role (
    id integer NOT NULL,
    id_participant integer,
    id_role integer
);


ALTER TABLE public.participant_x_role OWNER TO jdev;

--
-- TOC entry 225 (class 1259 OID 1422255)
-- Name: participant_x_role_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.participant_x_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_x_role_id_seq OWNER TO jdev;

--
-- TOC entry 226 (class 1259 OID 1422257)
-- Name: preagenda; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.preagenda (
    id integer NOT NULL,
    id_formation integer,
    id_salle integer,
    date_debut timestamp(0) without time zone NOT NULL,
    date_fin timestamp(0) without time zone NOT NULL,
    display integer
);


ALTER TABLE public.preagenda OWNER TO jdev;

--
-- TOC entry 227 (class 1259 OID 1422260)
-- Name: preagenda_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.preagenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.preagenda_id_seq OWNER TO jdev;

--
-- TOC entry 228 (class 1259 OID 1422262)
-- Name: reseau; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.reseau (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    display integer
);


ALTER TABLE public.reseau OWNER TO jdev;

--
-- TOC entry 229 (class 1259 OID 1422265)
-- Name: reseau_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.reseau_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reseau_id_seq OWNER TO jdev;

--
-- TOC entry 230 (class 1259 OID 1422267)
-- Name: role; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.role (
    id integer NOT NULL,
    label character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO jdev;

--
-- TOC entry 231 (class 1259 OID 1422270)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO jdev;

--
-- TOC entry 232 (class 1259 OID 1422272)
-- Name: salle; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.salle (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    etage integer NOT NULL,
    aile character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    quota_officiel integer NOT NULL,
    quota_physique integer NOT NULL,
    wifi character varying(255) NOT NULL,
    reseau integer NOT NULL,
    videoprojecteur boolean NOT NULL
);


ALTER TABLE public.salle OWNER TO jdev;

--
-- TOC entry 233 (class 1259 OID 1422278)
-- Name: salle_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.salle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.salle_id_seq OWNER TO jdev;

--
-- TOC entry 234 (class 1259 OID 1422280)
-- Name: thematique; Type: TABLE; Schema: public; Owner: jdev
--

CREATE TABLE public.thematique (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    label character varying(255) DEFAULT NULL::character varying NOT NULL
);


ALTER TABLE public.thematique OWNER TO jdev;

--
-- TOC entry 235 (class 1259 OID 1422287)
-- Name: thematique_id_seq; Type: SEQUENCE; Schema: public; Owner: jdev
--

CREATE SEQUENCE public.thematique_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.thematique_id_seq OWNER TO jdev;

--
-- TOC entry 3872 (class 0 OID 1422170)
-- Dependencies: 196
-- Data for Name: agenda; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.agenda (id, id_formation, id_salle, date_debut, date_fin, display, code_formation) FROM stdin;
168	68	12	2017-07-06 09:00:00	2017-07-06 10:30:00	40	594a355c5adf3
118	117	13	2017-07-06 11:00:00	2017-07-06 12:30:00	50	593fbcb970cf5
2	2	6	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a87b
3	3	25	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a8b6
4	4	7	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a9a2
70	89	10	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a9dc
32	32	18	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a651
33	33	16	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5aaff
34	35	17	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a967
35	36	9	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a83f
8	8	12	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a7fb
50	49	13	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5aa56
51	50	19	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5aac0
52	51	20	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a8f1
170	53	15	2017-07-04 10:00:00	2017-07-04 13:00:00	30	594a355c5a615
36	34	19	2017-07-05 10:00:00	2017-07-05 13:00:00	30	594a355c5a5d5
204	73	23	2017-07-05 09:00:00	2017-07-05 10:30:00	40	5947ab37efe78
207	123	16	2017-07-05 11:00:00	2017-07-05 12:30:00	50	5948ff1a3c036
208	154	8	2017-07-06 14:00:00	2017-07-06 17:30:00	20	5953bb872e0dd
116	81	23	2017-07-06 14:00:00	2017-07-06 15:30:00	40	594a355c5ac89
98	133	25	2017-07-06 16:00:00	2017-07-06 17:30:00	50	594a355c5ad43
31	29	20	2017-07-06 16:00:00	2017-07-06 17:30:00	50	594a355c5adb8
73	88	18	2017-07-05 14:00:00	2017-07-05 17:30:00	30	594a355c5a92c
62	70	13	2017-07-05 14:00:00	2017-07-05 15:30:00	40	594a355c5ad08
203	30	14	2017-07-05 17:30:00	2017-07-05 19:00:00	60	5943f4e2664ad
206	152	9	2017-07-05 17:30:00	2017-07-05 19:00:00	60	5947d56dc6fcb
135	85	11	2017-07-06 14:00:00	2017-07-06 15:30:00	40	594a355c5accc
138	139	13	2017-07-06 14:00:00	2017-07-06 15:30:00	50	594a355c5aecc
209	155	16	2017-07-06 17:30:00	2017-07-06 19:00:00	10	595c9e18ef078
171	82	10	2017-07-06 09:00:00	2017-07-06 10:30:00	40	594a355c5ad7e
202	27	23	2017-07-04 11:30:00	2017-07-04 13:00:00	50	59413080b3ed9
41	37	17	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5c47f
9	9	21	2017-07-05 09:00:00	2017-07-05 12:30:00	10	594a355c5c6b4
113	83	23	2017-07-06 11:00:00	2017-07-06 12:30:00	50	594a355c5c27b
42	38	20	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5cbd6
23	22	9	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5c336
67	69	12	2017-07-06 11:00:00	2017-07-06 12:30:00	50	594a355c5ca14
60	65	12	2017-07-05 14:00:00	2017-07-05 15:30:00	40	594a355c5bc14
43	41	9	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5cd18
5	5	11	2017-07-04 10:00:00	2017-07-04 13:00:00	20	594a355c5c2bf
160	144	23	2017-07-04 10:00:00	2017-07-04 11:30:00	40	594a355c5c0f1
68	86	11	2017-07-06 11:00:00	2017-07-06 12:30:00	50	594a355c5be12
137	138	25	2017-07-06 11:00:00	2017-07-06 12:30:00	50	594a355c5c99c
16	16	21	2017-07-06 14:00:00	2017-07-06 17:30:00	10	594a355c5c6f1
15	15	22	2017-07-06 14:00:00	2017-07-06 17:30:00	10	594a355c5c031
28	18	14	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5c3ab
29	25	10	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5c3e6
21	26	20	2017-07-06 14:00:00	2017-07-06 15:30:00	40	594a355c5bb29
48	47	12	2017-07-06 14:00:00	2017-07-06 15:30:00	40	594a355c5bb9e
1	150	17	2017-07-06 14:00:00	2017-07-06 15:30:00	40	5939cbcdb7e33
97	104	25	2017-07-06 14:00:00	2017-07-06 15:30:00	40	594a355c5b8cf
10	10	22	2017-07-05 09:00:00	2017-07-05 12:30:00	10	594a355c5c06b
55	60	10	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5cdd1
19	19	13	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5c2fa
54	59	6	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5cd96
38	40	9	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5c440
53	56	14	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5cd5b
102	128	18	2017-07-05 09:00:00	2017-07-05 10:30:00	40	594a355c5bf80
88	115	17	2017-07-05 09:00:00	2017-07-05 10:30:00	40	594a355c5bbd9
101	103	25	2017-07-05 09:00:00	2017-07-05 10:30:00	40	594a355c5bb63
39	44	15	2017-07-05 09:00:00	2017-07-05 10:30:00	40	594a355c5bc7f
125	119	16	2017-07-05 09:00:00	2017-07-05 10:30:00	40	594a355c5c130
132	124	17	2017-07-05 11:00:00	2017-07-05 12:30:00	50	594a355c5ca4f
103	129	18	2017-07-05 11:00:00	2017-07-05 12:30:00	50	594a355c5c0a6
72	99	23	2017-07-05 11:00:00	2017-07-05 12:30:00	50	594a355c5bf44
40	45	15	2017-07-05 11:00:00	2017-07-05 12:30:00	50	594a355c5c769
200	151	25	2017-07-05 11:00:00	2017-07-05 12:30:00	50	5940502cf2cdf
139	140	21	2017-07-04 14:00:00	2017-07-04 18:30:00	10	594a355c5c62f
77	98	23	2017-07-05 14:00:00	2017-07-05 15:30:00	40	594a355c5bab2
13	13	22	2017-07-05 14:00:00	2017-07-05 17:00:00	10	594a355c5c66a
14	14	21	2017-07-05 14:00:00	2017-07-05 17:30:00	10	594a355c5bfbb
24	23	7	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5c371
107	105	25	2017-07-05 14:00:00	2017-07-05 15:30:00	40	594a355c5baee
127	122	11	2017-07-05 14:00:00	2017-07-05 15:30:00	40	594a355c5ba6f
161	67	12	2017-07-05 16:00:00	2017-07-05 17:30:00	50	594a355c5cac4
78	101	23	2017-07-05 16:00:00	2017-07-05 17:30:00	50	594a355c5c9d8
108	106	25	2017-07-05 16:00:00	2017-07-05 17:30:00	50	594a355c5b889
95	121	11	2017-07-05 16:00:00	2017-07-05 17:30:00	50	594a355c5ca89
205	153	21	2017-07-05 17:30:00	2017-07-05 19:00:00	60	5947d54a37879
82	97	22	2017-07-05 17:30:00	2017-07-05 19:00:00	60	594a355c5be92
30	28	23	2017-07-05 17:30:00	2017-07-05 19:00:00	60	594a355c5becd
56	71	11	2017-07-05 17:30:00	2017-07-05 19:00:00	60	594a355c5bf08
115	80	12	2017-07-05 17:30:00	2017-07-05 19:00:00	60	594a355c5c72d
12	12	21	2017-07-06 09:00:00	2017-07-06 12:30:00	10	594a355c5bff6
11	11	22	2017-07-06 09:00:00	2017-07-06 12:30:00	10	594a355c5c5ec
106	131	13	2017-07-06 16:00:00	2017-07-06 17:30:00	50	594a355c5c7fa
83	100	17	2017-07-06 16:00:00	2017-07-06 17:30:00	50	594a355c5caff
136	46	12	2017-07-06 16:00:00	2017-07-06 17:30:00	50	594a355c5c7ba
120	135	25	2017-07-05 17:30:00	2017-07-05 19:00:00	60	594a355c5be56
117	84	23	2017-07-06 16:00:00	2017-07-06 17:30:00	50	594a355c5cb97
63	72	13	2017-07-05 16:00:00	2017-07-05 17:30:00	50	594a355c5c959
162	145	21	2017-07-07 09:00:00	2017-07-07 12:00:00	10	594a355c5c4ba
172	148	11	2017-07-06 09:00:00	2017-07-06 10:30:00	40	594a355c5cb3b
129	114	7	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d1a6
66	55	10	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5ce82
100	127	8	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d1e1
165	147	15	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d4d0
91	112	16	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d45b
128	110	14	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d3e5
112	76	18	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d2f9
111	74	19	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d2bd
96	102	6	2017-07-06 09:00:00	2017-07-06 12:30:00	20	594a355c5d16c
80	93	6	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5cf5c
75	94	9	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5d00d
164	146	7	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5d495
114	79	15	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5d334
121	75	16	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5d36f
133	136	18	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5d420
84	109	11	2017-07-06 16:00:00	2017-07-06 17:30:00	20	594a355c5d082
74	91	19	2017-07-06 14:00:00	2017-07-06 17:30:00	20	594a355c5cf97
18	20	21	2017-07-04 10:00:00	2017-07-04 13:00:00	20	594a355c5d580
20	21	22	2017-07-04 10:00:00	2017-07-04 13:00:00	20	594a355c5d5ba
6	6	14	2017-07-04 10:00:00	2017-07-04 13:00:00	20	594a355c5d5f4
71	96	7	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5cf20
85	108	20	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5d0bc
124	107	12	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5d3a9
25	24	11	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5d50a
69	90	8	2017-07-05 09:00:00	2017-07-05 12:30:00	20	594a355c5cee1
110	78	10	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5d27e
17	17	6	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5d545
109	77	8	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5d21b
90	111	17	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5d0f6
79	92	15	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5cfd1
59	58	20	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5ce47
58	57	19	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5ce0c
81	95	14	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5d047
92	113	16	2017-07-05 14:00:00	2017-07-05 17:30:00	20	594a355c5d131
\.


--
-- TOC entry 3874 (class 0 OID 1422176)
-- Dependencies: 198
-- Data for Name: atelier; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.atelier (id, thematique_id, salle_id, nom, date_debut, date_fin) FROM stdin;
\.


--
-- TOC entry 3876 (class 0 OID 1422181)
-- Dependencies: 200
-- Data for Name: communaute; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.communaute (id, label, display) FROM stdin;
1	GDR GPL	\N
2	GDR MADICS	\N
3	GDR RO	\N
4	GDR ASR	\N
5	GDR calcul	\N
6	GDR MAGIS	\N
7	Biologie	\N
8	Chimie	\N
9	Environnement et écologie	\N
10	SHS	\N
11	Science de l'information	\N
12	Ingénierie et système	\N
13	Mathématique	\N
14	Physique	\N
15	Phyique nucléaire et particulaire	\N
16	Science de l'univers	\N
17	GDS ecoinfo	\N
\.


--
-- TOC entry 3878 (class 0 OID 1422186)
-- Dependencies: 202
-- Data for Name: contribution; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.contribution (id, label, display) FROM stdin;
1	Contributeur - Présentation plénière (30-45min)	\N
2	Contributeur - Présentation thématique (30-45min)	\N
3	Contributeur - Porteur d'atelier (3h)	\N
5	Soumissionnaire de poster	\N
6	Contributeur - retour d'expérience GT (10min)	\N
4	Contributeur - Animateur de groupe de travail (1h30)	\N
7	Contributeur - Support d'atelier (3h)	\N
\.


--
-- TOC entry 3880 (class 0 OID 1422191)
-- Dependencies: 204
-- Data for Name: formation; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.formation (id, id_thematique, nom, type, intervenant, quota, confirme, titre, nb_intervenant) FROM stdin;
126	6	t6.a06	Atelier	-	20	f	Modéliser et développer son application avec le patron de conception MVC	\N
136	6	t6.a05	Atelier	Antoine Mottier	20	t	Construction d'applications basées sur des processus métiers avec Bonita BPM	\N
8	8	t8.ap01	Atelier Préparatoire	B Putigny (OMP) et Gabriel Hautreux (GENCI)	20	t	 Initiation à openMP pour la parallélisation à mémoire partagée et l'optimisation monocoeur par vectorisation (OpenMP4). 	\N
151	6	t6.gt09	Groupe de Travail	Julien Simon Amazon Web Services	50	t	DevOps sur AWS: livraison continue et outils de développement	\N
13	6	t6.p01	Plénière	Luc Saccavini / Sébastien Mosser / Christophe DENEUX / Philippe Krief / Olivier Prouvost / Jean Luc Parouty	400	t	Dialogue MOA - MOE: mythes et réalités / Informatisation orientée processus métiers / Convergence BPM-SOA / Démarche opensource pour un projet de recherche / Eclipse 4.0 / Blockchain	\N
152	1	t1.gt06	Groupe de Travail	Julien Simon	50	t	AWS IoT et AWS Greengrass, , deux services pour l’Internet des Objets	\N
53	8	t8.ap06	Atelier Préparatoire	Marc Gaucheron, Intel	20	t	OpenCL compiler for Intel FPGA. 	\N
150	4	t4.gt06	Groupe de Travail	Nicolas Carpi	50	t	Comment écrire correctement le fichier de configuration Docker pour déployer son application ?	\N
36	7	t7.ap05	Atelier Préparatoire	Gunter Roth NVIDIA	41	t	Initiation au DeepLearning (DIGITS/Caffe)	\N
14	7	t7.p01	Plénière	 Liva Ralaivola, Hachem Kadri / Sébastien Dejean et Laurent Risser / Balazs Kegl / Benoit Favre,  Thierry Artières / Romain Vuillemot	400	t	Historique, évolutions, tendances, ressources et outils de l'apprentissage / Apprentissage statistique / éco-système recherche par les données / Tuto Deep Learning / visu des données et python (dataviz, pandas)	\N
12	4	t4.p01	Plénière	Johan Moreau /  Alexandre Ancel / Claire Mouton / Frédéric Woelffel / Maurice Poncet	400	t	Présentation de la production logicielle, des outils et des pratiques / Présentation des VCS-DVCS et forges / Etat de l'art des Forges logicielles. Illustration par Gitlab / REX	\N
37	7	t7.a01	Atelier	Laurent Rissler (IMT, Toulouse)	21	t	Python pour l'apprentissage	\N
18	1	t1.a02	Atelier	Pierre Ficheux	20	t	Démonstrateur IoT (OS / MQTT / visualisation)	\N
19	1	t1.a03	Atelier	Arnaud Biganzoli	20	t	Python pour l'embarqué	\N
79	2	t2.a06	Atelier	Benjamin Cogrel	20	t	Interrogation efficace de bases de données relationnelles avec SPARQL et Ontop	\N
1	1	t1.ap01	Atelier Préparatoire	Karima Rafes (BorderCloud)	20	t	Prise en main de SPARQL avec Wikidata - ma première requête	\N
140	1	t0.p01	Plénière	-	600	t	Plénière - Ouverture/ IXP/GIX et info-structures de l’Internet/ Ingénierie des exigences: concepts, enjeux, fondamentaux/ Innovez à la vitesse de l’éclair avec AWS/ Fabrication additive mythe ou réalité ? /Tour d'horizon des thématiques	\N
35	7	t7.ap04	Atelier Préparatoire	Frédéric Pont/CRCT/ONCOPOLE/INSERM	20	t	Manipulation de données pour débutants en Julia	\N
147	6	t6.a03	Atelier	Marc Chambon	20	t	Concepts et applications de WebGL 2 et du langage GLSL (OpenGL Shading Language)	\N
127	6	t6.a04	Atelier	Sylvie Fiat	20	t	Portage de services sous docker	\N
41	7	t7.a05	Atelier	Balazs Kegl  (IN2P3,Paris-Saclay)	40	t	pydata (numpy, pandas, scikit-learn): RAMP (Rapid Analytics and Model Prototyping) - series temporelles - Prototypage rapide de modèles prédictifs dans l'environnement collaboratif du centre de données scientifiques de Paris-Saclay	\N
17	1	t1.a01	Atelier	-	20	t	Réaliser votre première application sous Android	\N
20	1	t1.a04	Atelier	Aguera	20	t	Développement sous Apple IOS : Première application avec Objective-C	\N
21	1	t1.a05	Atelier	Eric Duvieilbourg	20	t	Arduino + App Inventor + module Bluetooth	\N
22	1	t1.a06	Atelier	Eric Duvieilbourg	20	t	Interfaçage de cartes électroniques avec Python et interface en Qt	\N
33	7	t7.ap02	Atelier Préparatoire	Sébastien Dejean (IMT, Toulouse)	20	t	Manipulation de données pour débutants avec R	\N
23	1	t1.a07	Atelier	Schreiner	20	t	l’Internet des Objets sur la plate-forme FIT IoT-LAB	\N
24	1	t1.a08	Atelier	Eric Duvieilbourg	20	t	Mise en oeuvre de modules IoT, de acquisition à la diffusion des données-Arduino	\N
42	7	t7.a06	Atelier	Balazs Kegl 	20	t	RAMP (Rapid Analytics and Model Prototyping) - cas complexe (drug spectra ou HEP anomaly)	\N
146	7	t7.a07	Atelier	Sébastien Binet (CERN)	20	t	Intro aux outils pour les DataScience avec Go	\N
144	5	t5.gt14	Groupe de Travail	Raphael Ritz	50	t	Intéropérabilité des référentiels de données pour lier les ensembles de données variés	\N
135	2	t2.gt06	Groupe de Travail	Karima Rafes (BorderCloud)	50	t	Quelles sont les compétences professionnelles à développer par les administrateurs de systèmes d’information dédiés à la gestion des connaissances (formation, certification, …) ?	\N
148	2	t2.gt07	Groupe de Travail	Karima Rafes (BorderCloud)	50	t	Référencement des données pour la recherche : retour d’expériences du Center for Data Science de l’université Paris-Saclay	\N
149	2	t2.gt08	Groupe de Travail	A Phat LY (CNRS/LLL) & Yvan Stroppa (CNRS/Paul Painlevé)	50	t	Extraction d'information du web : illustration aux réseaux de chercheurs - La plateforme « The NetWork of Researchers»	\N
131	6	t6.gt06	Groupe de Travail	Fabien Amico (Treeptik) 	50	t	Des origines de DevOps à la mise en œuvre	\N
28	1	t1.gt03	Groupe de Travail	Sylvie Fiat	50	t	ReefTEMPS est un réseau de capteurs de température, pression et salinité dans le domaine côtier	\N
38	7	t7.a02	Atelier	Sébastien Dejean (IMT, Toulouse)	20	t	R pour l'apprentissage	\N
40	7	t7.a04	Atelier	Balazs Kegl   (IN2P3,Paris-Saclay)	40	t	pydata (numpy, pandas, scikit-learn) : RAMP (Rapid Analytics and Model Prototying) - reconnaissance d'image d'insecte -Prototypage rapide de modèles prédictifs dans l'environnement collaboratif du centre de données scientifiques de Paris-Saclay 	\N
11	1	t1.p01	Plénière	Pierre Ficheux / K. Drira /  Julien Vandaele /  Alexandre Abadie / Franck Barbier	400	t	Etat de l'Art des IoT et les systèmes M2M: IoT, M2M, cloud, Linux et IoT / REcherche / plate-forme d'expérimentations / OS IoT / dev. indus.	\N
52	8	t8.ap05	Atelier Préparatoire	B Putigny (OMP) et Gabriel Hautreux (GENCI)	20	t	Fusionner avec T8.AP01 ou il faut s'inscrire pour formation debutant en OpenMP 	\N
44	7	t7.gt01	Groupe de Travail	Sébastien Dejean (IMT, Toulouse) 	50	t	Préparation des données pour l'analyse statistique et le machine learning (mise en oeuvre avec R)	\N
54	8	t8.a01	Atelier	-	20	t	Roboconf, Orchestration de conteneurs pour l'intégration continue	\N
61	8	t8.a11	Atelier	-	20	t	portage d'un code scientifique	\N
153	7	t7.gt06	Groupe de Travail	Moshir Mikael/AWS	50	t	Services d’analyse et machine learning à portée de tous sur AWS	\N
15	5	t5.p01	Plénière	Alain Tchana/Olivier Sallou/Nicolas Larrousse/Christophe DENEUX, Bertrand ESCUDIE / Andry Andriatiana/ Thomas Baillet	400	t	Les infra numériques, les services de calcul et de données/infra logicielles et concepts/infras logicielle en SHS/ composants et agilité technique/ SOA-WOA-ROA / Portage Docker d'une archi SOA/ Event mining	\N
51	8	t8.ap04	Atelier Préparatoire	Laure Tavard  et Bruno Bzeznik / UMS GRICAD.	20	t	Installer et Débuter avec Nix gestionnaire de paquets “user level”.	\N
49	8	t8.ap02	Atelier Préparatoire	Arnaud Renard et Jean-Matthieu Etancelin / Romeo.	20	t	initiation Les bases d'openACC	\N
6	6	t6.a01	Atelier	Sébastien Binet	20	t	Initiation a la programmation concurrente avec Go	\N
56	8	t8.a03	Atelier	Arnauld Renard et Jean-Matthieu Etancelin / Romeo.	20	t	Comment Programmer les GPU (openMP, openACC, Cuda, openCL, par toolbox ou librairie)	\N
59	8	t8.a09	Atelier	Alexandre Dehne (INRA) et Martin Souchal (APC/Univ Paris 7).	20	t	Transporter ses applications parallèles avec les containers LXD et Singularity	\N
4	4	t4.ap01	Atelier Préparatoire	Claire Mouton	20	t	Gestionnaire de versions de code source Git (débutant)	\N
58	8	t8.a08	Atelier	Gabriel Hautreux/cellule veille technologique du  GENCI	20	t	Programmation memoire partagée hybride OpenMP4	\N
57	8	t8.a05	Atelier	Arnaud Renard et  Jean-Matthieu Etancelin / Romeo. 	20	t	Tutoriel parallèlisme hybride Multi GPU avec mpi et OpenACC	\N
47	7	t7.gt07	Groupe de Travail	Gunter Roth  NVIDIA	50	t	Rex sur Les librairies et toolbox de Deep Learning sur GPU: Digits, Caffee, Magma, Torch, …   - Conseils pour bien démarrer son projet Deep Learning 	\N
74	2	t2.a01	Atelier	Manuel Atencia	20	t	Modéliser les ontologies : cas d'application d'une ontologie pour l'annotation de photo	\N
75	2	t2.a02	Atelier	Jérôme David	20	t	Comment faire une application avec des données liées: annotation de photo en utilisant les technos du web sémantique	\N
76	2	t2.a03	Atelier	Kostantin Todorov	20	t	Alignement d'ontologies & interconnexion de données	\N
77	2	t2.a04	Atelier	Karima Rafes	20	t	Atelier SparQL, niveau avancé	\N
78	2	t2.a05	Atelier	Olivier Corby	20	t	STTL, un langage de transformation de graphes RDF basé sur SPARQL	\N
67	8	t8.gt06	Groupe de Travail	Laure Tavard et Bruno Bzeznik / UMS GRICAD.	50	t	NIX comme environnement HPC reproductible, résistant aux mises à jours et portable	\N
55	8	t8.a02	Atelier	Bertrand Putigny/IRAP/Observatoire Midi-Pyrénées.	20	t	Prise en main d'un outil Intel d'optimisation par vectorisation (Intel Advisor) avec des codes OpenMP	\N
88	4	t4.ap02	Atelier Préparatoire	Frédéric Woelffel	20	t	Pipeline gitlab / jenkins(travis) / docker	\N
89	4	t4.ap03	Atelier Préparatoire	 Alexandre Ancel	20	t	Docker	\N
90	4	t4.a01	Atelier	Claire Mouton	20	t	Git en solo et en équipe via une forge logicielle (GitHub)	\N
46	7	t7.gt04	Groupe de Travail	Tom Jorquera (LINAGORA) 	50	t	Un outil pour la transcription et la recommandation temps-réel durant une vidéo-conférence. Reconnaissance de la parole et machine learning : OpenPaaS	\N
50	8	t8.ap03	Atelier Préparatoire	Thierry Garcia	20	t	 Initiation MPI communication point à point par message: programmation et simulation parallèle à travers les communications entre machines	\N
2	2	t2.ap01	Atelier Préparatoire	Karima Rafes	25	t	Prise en main de SPARQL avec Wikidata - ma première requête	\N
45	7	t7.gt02	Groupe de Travail	Frederic Pont (CRCT/oncopole/Inserm) et L. Risser (IMT, Toulouse) 	50	t	Retour d'experience sur l'utilisation de perl, R, Julia, GO, Python, OpenCL en sciences médicales.	\N
5	5	t5.a01	Atelier	Nicolas Thouvenin	20	t	Comment interroger une API REST et exemple d' usage des données et des API ISTEX pour accéder à 18 millions des textes d’article ou d'ouvrage et 7500 revues scientifiques multidisciplinaires.	\N
60	8	t8.a10	Atelier	Thierry garcia (Li-PARAD) et Pierre SPITERI (IRIT).	20	t	portage d'un code centralisé sous MPI: comment porter un code séquentiel dans un environnement parallèle	\N
72	8	t8.gt11	Groupe de Travail	 Arthur Petitpierre / Amazon Web Service.	40	t	ANNULE : AWS Batch - Chercheurs, enseignants, venez découvrir comment faire pratiquement des calculs haute performance sans vous soucier du matériel	\N
48	7	t7.gt08	Groupe de Travail	-	50	t	Retours d'expérience par NVIDIA en deep learning	\N
32	7	t7.ap01	Atelier Préparatoire	Laurent Rissler  (IMT, Toulouse)	20	t	Initiation python	\N
86	7	t7.gt05	Groupe de Travail	Thomas Pellegrini (IRIT/UPS)	50	t	Architectures récentes de réseaux de neurones profonds, notion de saillance	\N
25	1	t1.a09	Atelier	Olivier Legoager	20	t	REST sous Android: utiliser l'API Android pour appeler un service Web de type REST	\N
9	2	t2.p01	Plénière	Manuel Atencia / Olivier Corby / Franck Michel / Kostantin Todorov / Viviane Boulétreau	400	t	Plénière - ingénierie et web des données : tuto linked data / Best Practices & Design Patterns / Intégrer des Sources de Données Hétérogènes / Données musicales / Data Persée .	\N
16	8	t8.p01	Plénière	E. Gondet/OBS-MIP, J.P Proux/GENCI, P.F Lavallée IDRIS/CNRS, P. SPiteri /IRIT, R. Dernat/Univ-montpellier,  A. Tchana/IRIT	400	t	La pyramide du calcul intensif / Panorama du parallèlisme / programmation parallèle asynchrone/ les containers dans le monde du calcul/ Sécurité dans les environnements/infrastructures multitenants	\N
145	1	t0.p02	Plénière	Patrick Moreau/ François Elie/ Luc Maranget/ Sébastien Binet/ Chrystel Moreau	600	t	valorisation du logiciel par le CNRS/ loi de Republique numerique et eco-systeme du dev/ Logiciel et informatique durable/ Machines parallèles à mémoire partagée, le point de vue du programmeur/  Go et la prog concurrente	\N
154	4	t4.ap03b	Atelier	Alexandre Ancel	20	t	Introduction à Docker (de la récupération d'images à la création de conteneurs personnalisés)	\N
155	8	t8.GT13	Groupe de Travail	Stefan Gaget MiN2Rien et Thierry Chaventré DEVLOG	40	t	Rencontre réseaux régionaux et antionaux de l'informatique 	\N
97	4	t4.gt01	Groupe de Travail	Claire Mouton	40	t	Où déposer mon code ? Forge privée ou service public ?	\N
81	2	t2.gt02	Groupe de Travail	Romain David (IMBE/CNRS) & Luc HOGIE (CNRS/I3S/Université de Nice Sophia Antipolis/Inria) & Anna COHEN NABEIRO (FRB/Ecoscope) & Sophie PAMERLON (GBIF France)	50	t	Outils, compétences et savoir faire nécessaire à la fouille de graphes	\N
82	2	t2.gt03	Groupe de Travail	Benjamin Cogrel (Post-doctorant, KRDB, Université Libre de Bozen-Bolzano)	50	t	Quelles technologies du Web Sémantique choisir pour son système d'information ?	\N
99	4	t4.gt03	Groupe de Travail	Anaïs Oberto	50	t	Quels outils pour développer le frontend ? Les frameworks JavaScript et autres solutions	\N
101	4	t4.gt05	Groupe de Travail	Johan Moreau	50	t	Gestion de configurations, une base pour l'orchestration	\N
98	4	t4.gt02	Groupe de Travail	Johan Moreau	50	t	Le micro-service, brique élémentaire du logiciel	\N
100	4	t4.gt04	Groupe de Travail	Johan Moreau	50	t	Quelle usine de développement pour son projet ? Comment mettre en place son atelier de génie logiciel	\N
103	6	t6.gt01	Groupe de Travail	Luc Saccavini	50	t	Le dialogue MOA et MOE	\N
105	3	t3.gt01	Groupe de Travail	Jean Claude André (animateur du débat)	50	t	Objets, base d'objets, licences, coûts, libre et fermé, politique des fournisseurs… mais @@@!!@@@ où allons nous ?	\N
106	3	t3.gt02	Groupe de Travail	_ 	50	t	Retours d'expériences autour de l'impression 3D	\N
116	5	t5.gt02	Groupe de Travail	Yvan Le Bras	50	t	Urbaniser/architecturer une infrastructure numérique, fédération de communauté de développement et fédérer une communauté d'utilisateur	\N
117	5	t5.gt03	Groupe de Travail	Stéphane Ribas	50	t	 Infrastructure numérique et communauté de développement 	\N
115	5	t5.gt01	Groupe de Travail	Olivier Sallou.	50	t	Openstack pour modéliser et tester son architecture SOA ou ROA	\N
70	8	t8.gt09	Groupe de Travail	Pierre Spiteri (IRIT) et Vincent Partimbene.	50	t	Programmation parallèle asynchrone, applications et modélisation mathématique	\N
65	8	t8.gt04	Groupe de Travail	 Jean-Marc Pierson/IRIT.	50	t	Convergence HPC/Cloud	\N
68	8	t8.gt07	Groupe de Travail	R. Dernat (Univ. Montpellier) M Souchal (IN2P3/CNRS) A.D Garcia (INRA).	50	t	Les containers pour le parallèlisme (Etat de l'art, REX et projets à venir).	\N
85	7	t7.gt03	Groupe de Travail	Laurent Risser (IMT) et Yves Auda (GET/Observatoire Midi-Pyrénées)	50	t	Préparation des données avec Pandas, Python Data Analysis Library.	\N
63	8	t8.gt02	Groupe de Travail	-	50	t	Retour d'une communauté structuré autour d'une E-Infrastructure. Architecture et Optimisation/Adéquation matériel	\N
66	8	t8.gt05	Groupe de Travail	-	50	t	Convergence calcul et données : comment organiser mes services de données et de calcul?	\N
26	1	t1.gt01	Groupe de Travail	Camps	50	t	IoT, M2M : infrastructure, Open plateforme, protocoles	\N
27	1	t1.gt02	Groupe de Travail	Marie-Claude Quidoz	50	t	Conception d'une tablette tout terrain organisée autour d'un Raspberry	\N
29	1	t1.gt04	Groupe de Travail	Arnauld Biganzoli	50	t	IoT - M2M : Mise en oeuvre avec microcontrôleurs	\N
31	1	t1.gt010	Groupe de Travail	Boujrad	50	t	Red Pitaya et Zinq	\N
30	1	t1.gt05	Groupe de Travail	Boujrad	50	t	Red Pitaya et Zinq	\N
80	2	t2.gt01	Groupe de Travail	Baptiste Laporte	50	t	Utiliser des vocabulaires contrôlés pour vérifier des données	\N
71	8	t8.gt10	Groupe de Travail	Thomas Palichata (GENCI)	50	t	Comment utiliser l'offre SIMSEO afin d'établir des partenariats labo-pme pour le développement et l'innovation grâce au calcul intensif	\N
62	8	t8.gt01	Groupe de Travail	-	50	t	Fusionné voir  T8.GT08 	\N
64	8	t8.gt03	Groupe de Travail	-	50	t	Fusionné voir  T8.GT08 	\N
69	8	t8.gt08	Groupe de Travail	D. Chamont, E. Courcelle, E. Gondet, A. Renard	50	t	Reproductivité des calculs à tous les étages de la pyramide de calcul, quels outils :LXC, LXD, containers docker sur GPU...	\N
118	5	t5.gt11	Groupe de Travail	Yvan Le Bras	50	t	Intégrer ses développements au sein d'une infrastructure	\N
119	5	t5.gt04	Groupe de Travail	Marie Farge	50	t	Stratégie de publication pour l'OpenScience (publication, code et données) 	\N
120	5	t5.gt08	Groupe de Travail	Yvan Le Bras	50	t	Comment protéger son jeu de données dans une infrastructure ouverte ?	\N
121	5	t5.gt06	Groupe de Travail	Patrick Bellot	50	t	Recherche textuelle	\N
122	5	t5.gt07	Groupe de Travail	Alexandre Delanoé	50	t	Les API pour la recherche et la fouille de documents scientifiques et techniques	\N
84	2	t2.gt05	Groupe de Travail	Sonia Guerin-Hamdi (CNRS/ISH) // A-Phat LY (CNRS/LLL) & Yvan STROPPA (CNRS/Paul Painlevé)	50	t	Extraction d'information du web : illustration aux réseaux de chercheurs // La plateforme « The NetWork of Researchers	\N
10	3	t3.p01	Plénière	Jean Claude André / Bertrand Viellerobe / Nicolas Lassabe / Romain Di Vozzo / Jérôme Maisonnasse	400	t	Les différentes technologies d’impression 3d ; leurs forces et leurs faiblesses / Le mouvement Fablabs et Techshops, la programmation de la matière et l'émergence des usines personnelles enfin expliqués 	\N
73	8	t8.gt12	Groupe de Travail	Etienne Gondet/OBS-MIP et Gabriel Hautreux/cellule de veille technologique du GENCI.	50	t	Passé à 17h30-19h00  en S112 avec T8.GT10 Nouvelles architectures parallèles: présentez votre code parallèle en 5 minutes et discutez de ses perspectives d’évolution.	\N
123	5	t5.gt05	Groupe de Travail	Nicolas Larrousse	50	t	La question de la pérennité des données de la recherche : les plateformes et les infrastructures	\N
124	5	t5.gt10	Groupe de Travail	Florentin Clouet	50	t	Mise en place d'une architecture robuste / Sécurité & scalabilité : Retour d’expérience sur la plate-forme Hypothèse	\N
130	6	t6.gt05	Groupe de Travail	Christophe Sauthier, Maxime Cottret	50	f	Retours d'expériences: OpenStack et autres outils DevOps OpenSource pour accélérer les développements	\N
125	5	t5.gt09	Groupe de Travail	LogIsland - Thomas Bailet	50	t	Quels usages pour la recherche du traitement temps réel de flux massifs d'événements	\N
133	6	t6.gt08	Groupe de Travail	Claire Mouton	50	t	Développement coopératif versus développement collaboratif: quels workflows choisir ?	\N
128	6	t6.gt03	Groupe de Travail	Robert Darimont	50	t	Approches basées sur des bonnes pratiques et canevas méthodologiques (focus sur Volere)	\N
138	5	t5.gt12	Groupe de Travail	Emmanuel Delage	50	t	Standardisation des méta-données pour l'interoperabilité des infrastructures: illustration par l'infrastructure européenne Epos et des observatoires virtuels	\N
139	6	t6.gt10	Groupe de Travail	Objectif Libre : Christophe Sauthier/Maxime Cottret et : Philippe Saby: Observatoire Midi-Pyrénées	50	t	Retours d'expériences: OpenStack et autres outils DevOps OpenSource pour accélérer les développements	\N
129	6	t6.gt04	Groupe de Travail	Robert Darimont	50	t	Techniques basées sur l’élaboration de modèles (focus sur KAOS-Objectiver)	\N
143	5	t5.gt13	Groupe de Travail	RDA : Françoise Génova et/ou François-André (OMPs)	50	t	les identifiants permanent, pourquoi, comment? La problématique de la citation des données (et des requêtes!)	\N
132	6	t6.gt07	Groupe de Travail	Christophe DENEUX, Bertrand ESCUDIE (Linagora)	50	f	!!! ANNULATION - NE PAS S'INSCRIRE !!! Batch/traitements par lots et SOA	\N
34	7	t7.ap03	Atelier Préparatoire	Tristan Colombo (Éditions Diamond, GNU/Linux Magazine France)	20	t	Les bases du calcul scientifique avec Python. Python pour le calcul scientifique	\N
91	4	t4.a02	Atelier	Romaric Duvignau	20	t	Fiabilité logicielle en Java (tests unitaires, objets factices, test boite blanche/noire, jUnit, dbUnit, jMockit)	\N
92	4	t4.a03	Atelier	Bertrand Estellon	20	t	Créer une application Angular/NodeJS	\N
93	4	t4.a04	Atelier	Frédéric Woelffel	20	t	L'utilisation des conteneurs dans votre outil d’intégration continue	\N
96	4	t4.a07	Atelier	Julien Maupetit	20	t	Tests et intégration continue en python	\N
102	6	t6.a02	Atelier	Sébastien Mosser	20	t	Serious game sur amélioration continue, l’estimation, l’acceptation, la planification	\N
108	5	t5.a03	Atelier	Alexandre Delanoë	20	t	Recherche d'Information à partir de plusieurs référentiels: fouille textuelle, constitution et cartographie d'un corpus, 	\N
3	3	t3.ap01	Atelier Préparatoire	-	20	t	Atelier sur l'impression 3D, venez vous essayer à l'impression filaire 3D en toute sécurité	\N
107	5	t5.a02	Atelier	Yannick Barborini	20	t	Usage des API de HAL, des méta-données et des données. Indexation et RI	\N
110	5	t5.a05	Atelier	David Pilato	20	t	Découverte d'elastic search : beats + elasticsearch + kibana	\N
111	5	t5.a06	Atelier	Neo4j Cédric Fauvet	20	t	TP BD orientée graph - Fouille de données et jointure	\N
112	5	t5.a07	Atelier	Philippe Saby et Objectif Libre	20	t	Openstack : configurer un tenant/configurer un IAAS	\N
113	5	t5.a08	Atelier	Linagora & J.P Lorré	20	t	Roboconf, Orchestration de conteneurs pour l'intégration continue. Encapsulation de composants logiciels ou de machines virtuelles pour leur liaison avec le Cloud	\N
114	5	t5.a09	Atelier	LogIsland & Thomas Bailet	20	t	Atelier Flux d'évenement massif: mettre en place une chaîne de traitement, collecte, acquisition, configurer, création et exécution d'un algo de matching learning, plugin	\N
94	4	t4.a05	Atelier	Mariana Andujar, Magali Contensin	26	t	Mesurer la qualité du code source en continu avec SonarQube	\N
95	4	t4.a06	Atelier	Julien Maupetit	21	t	Introduction à Django	\N
104	6	t6.gt02	Groupe de Travail	Sébastien Mosser	50	t	Qu'est ce qu'un bon processus métier?	\N
83	2	t2.gt04	Groupe de Travail	Adrien Desseigne (CNRS TGIR Huma-Num)	50	t	Utilisation des technologies du Web Sémantique pour l'urbanisation et l'ouverture d’une infrastructure de recherche	\N
109	5	t5.a04	Atelier	Treeptik	20	t	Déploiement avec Docker	\N
\.


--
-- TOC entry 3882 (class 0 OID 1422200)
-- Dependencies: 206
-- Data for Name: organisme; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.organisme (id, label, nbplaces, nblibres) FROM stdin;
2	INRA	50	12
3	INSERM	0	0
4	INP	0	0
5	IGN	0	0
9	UNIVERSITE (UMR, UMS)	0	0
10	UNIVERSITE (NON UMR, NON UMS)	0	0
11	SOCIETE PRIVEE	0	0
6	INRIA	30	11
19	AMU	30	8
8	IRD	17	0
12	IFSTTAR	0	0
13	INED	0	0
14	IRT	0	0
15	CEA	0	0
16	CNES	0	0
17	Rectorat	0	0
18	Autre	0	0
1	CNRS	140	0
7	IRSTEA	20	10
\.


--
-- TOC entry 3884 (class 0 OID 1422205)
-- Dependencies: 208
-- Data for Name: participant; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant (id, id_organisme, prenom, nom, email, password, region, statut, emploi, role, date_inscription, cle_email, email_valide, clo_valide, acces_valide, type_inscription, mailing_list, evenement_social, pass_prepaye, unite, role_si, present, role_orga, regime_alimentaire, mobilite_reduite, code_wifi, date_activation) FROM stdin;
114	19	test	Participant3	test.participant3@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ETUDIANT	Chercheur	Participant	2017-03-06 14:00:27	58bd6b7b4d48f6.67225081	t	t	f	PASS COMPLET	t	t	t	UMR	\N	\N	\N	Aucun	f	f	\N
107	1	test	Participant2	test.participant2@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Pays-de-la-Loire	ACADEMIQUE	Ingénieur	Participant	2017-03-06 10:04:37	58bd34357f4522.46805671	t	t	t	PASS COMPLET	t	t	t	UMR6004	\N	t	\N	Aucun	f	f	\N
104	9	test	Participant6	test.participant6@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Auvergne-Rhône-Alpes	ETUDIANT	Chercheur	Participant	2017-03-05 21:05:10	58bc7d86473fd7.26267296	t	t	t	PASS COMPLET	f	t	f	Université	\N	t	\N	Aucun	f	f	\N
120	1	test	Orga_Admin	test.participant@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Ile-de-France	ACADEMIQUE	Ingénieur	Organisateur	2017-03-06 16:10:37	58bd89fde263e4.76424125	t	f	t	PASS COMPLET	t	f	t	MOY1678	admin	t	1	Aucun	f	f	\N
150	1	test	Participant5	test.participant5@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ACADEMIQUE	Chercheur	Participant	2017-03-09 12:11:20	58c146688109c3.70924061	t	t	t	PASS COMPLET	f	t	t	UMS	\N	t	\N	Aucun	f	f	\N
206	1	test	Participant1	test.participant1@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Ile-de-France	ACADEMIQUE	Ingénieur	Participant	2017-03-15 08:40:49	58c8fe11f1dd18.29427935	t	t	t	PASS COMPLET	t	t	t	UMR8190	\N	t	\N	Aucun	f	f	\N
257	1	test	Participant4	test.participant4@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Auvergne-Rhône-Alpes	ACADEMIQUE	Ingénieur	Participant	2017-03-27 13:14:25	58d91031128d83.08769041	t	t	t	PASS COMPLET	t	f	t	UMR	\N	t	\N	Aucun	f	f	\N
296	9	test	Orga3	test.orga3@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Auvergne-Rhône-Alpes	ACADEMIQUE	Enseignant-Chercheur	Organisateur	2017-04-10 05:48:31	58eb1caf5b2678.69694373	t	f	t	PASS COMPLET	t	t	f	UMS	clo_admin	t	1	Aucun	f	f	\N
384	18	inscrip	test	inscrip@yahoo.fr	$2y$10$63Cqfg.EFVKy4/H6DNk6JOsiptCgWn8fRNYLFDJ5y2LA9cSLg541y	Bourgogne-Franche-Comté	ACADEMIQUE	Chercheur	Exposant	2017-05-11 08:28:28	591420ac5214d8.27484694	f	f	f	PASS COMPLET	f	f	f	tests	\N	\N	\N	Aucun	f	f	\N
396	1	test	Sponsor1	test.sponsor1@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ACADEMIQUE	Ingénieur	Sponsor	2017-05-15 14:05:58	5919b5c6d088b7.72597004	t	t	t	PASS SPONSOR	t	t	f	UMS	\N	\N	\N	Aucun	f	f	\N
397	9	test	Sponsor	test.sponsor@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ACADEMIQUE	Ingénieur	Sponsor	2017-05-15 14:08:37	5919b665563ce0.73045163	t	t	t	PASS SPONSOR	t	t	f	Sponsor1	\N	\N	\N	Aucun	f	f	\N
532	11	test	Invite1	test.invite1@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Nouvelle-Aquitaine	INDUSTRIEL	Chercheur	Invité	2017-06-16 07:20:31	594386bf7dac15.14445634	t	f	f	PASS INVITE	f	f	f	POIETIS	\N	t	\N	Aucun	f	f	\N
508	18	test	Orga1	test.orga1@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Hauts-de-France	ACADEMIQUE	Ingénieur	Organisateur	2017-06-12 15:42:11	593eb653654f03.01581797	t	t	t	PASS INVITE	t	f	f	AUTRE	clo	t	1	Aucun	f	f	\N
567	18	test	Exposant	test.exposant@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Bretagne	INDUSTRIEL	Ingénieur	Exposant	2017-06-21 15:25:45	594a8ff9505385.57263797	t	t	t	PASS COMPLET	t	t	f	Exposant1	\N	\N	\N	Aucun	f	f	\N
312	1	test	Invite	test.invite@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ACADEMIQUE	Ingénieur	Invité	2017-04-13 13:33:58	58ef7e46c3fb19.10390906	t	t	t	PASS COMPLET	t	f	f	UMR0000	\N	t	\N	Aucun	f	f	\N
2	1	test	Orga2	test.orga2@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ACADEMIQUE	Ingénieur	Organisateur	2017-02-27 11:50:20	58b4127c1a6ce3.44660360	t	t	t	PASS ORGA	t	f	f	UMR	clo_pgm	t	2	Aucun	f	f	\N
1	1	Chrystel	Moreau	chrystel.moreau@lam.fr	$2y$10$rGPEXEU2lNaxe6ZAjwVoCe.wnyifMWZBqEk4JG3OtwLF18z3hibJK	Provence-Alpes-Côte d'Azur	ACADEMIQUE	Ingénieur	Organisateur	2017-02-27 11:47:30	58b411d2b86926.75367671	t	t	t	PASS ORGA	t	t	f	UMR7326	admin	t	1	\N	\N	\N	\N
\.


--
-- TOC entry 3886 (class 0 OID 1422217)
-- Dependencies: 210
-- Data for Name: participant_x_agenda; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_agenda (id, id_participant, id_agenda, date_inscription, present) FROM stdin;
133	120	139	2017-06-20 13:32:16	t
126	120	6	2017-06-20 13:32:03	t
131	107	9	2017-06-20 13:32:15	t
161	206	124	2017-06-20 13:34:13	t
123	107	35	2017-06-20 13:31:38	t
139	206	3	2017-06-20 13:32:37	t
149	107	14	2017-06-20 13:33:08	t
136	107	11	2017-06-20 13:32:26	t
155	107	16	2017-06-20 13:33:28	t
143	107	162	2017-06-20 13:32:42	t
180	120	90	2017-06-20 13:35:41	t
148	107	139	2017-06-20 13:32:57	t
150	206	139	2017-06-20 13:33:10	t
1130	104	139	2017-06-21 08:39:05	t
2106	150	139	2017-06-22 07:56:22	t
2291	396	139	2017-06-23 08:35:09	t
3359	508	121	2017-07-05 21:56:11	t
278	120	75	2017-06-20 13:39:47	t
228	206	81	2017-06-20 13:37:36	t
334	206	12	2017-06-20 13:43:28	t
283	120	162	2017-06-20 13:40:02	t
3369	396	68	2017-07-06 05:52:53	t
3370	396	97	2017-07-06 05:54:59	t
692	206	28	2017-06-20 14:32:51	t
697	206	162	2017-06-20 14:33:27	t
3422	150	74	2017-07-06 11:50:29	t
2103	150	42	2017-06-22 07:55:56	t
1113	104	129	2017-06-21 08:29:08	t
1117	104	48	2017-06-21 08:32:29	t
1082	104	4	2017-06-21 08:05:59	t
1102	104	38	2017-06-21 08:19:05	t
1110	104	90	2017-06-21 08:26:00	t
1129	104	162	2017-06-21 08:38:58	t
1470	120	100	2017-06-21 11:29:59	t
1447	120	38	2017-06-21 11:25:47	t
1460	120	30	2017-06-21 11:28:16	t
1945	532	108	2017-06-21 16:10:02	\N
1949	532	107	2017-06-21 16:10:41	t
1952	532	10	2017-06-21 16:11:17	t
2062	257	20	2017-06-22 07:18:26	\N
2111	150	95	2017-06-22 08:02:37	t
2101	150	39	2017-06-22 07:53:29	t
2100	150	33	2017-06-22 07:49:07	t
2109	150	127	2017-06-22 08:00:21	t
2098	257	75	2017-06-22 07:47:45	t
2075	257	10	2017-06-22 07:27:23	t
2091	257	11	2017-06-22 07:39:58	t
2122	150	162	2017-06-22 08:07:26	t
2286	396	172	2017-06-23 08:29:50	t
2272	396	35	2017-06-23 07:58:31	\N
2288	396	117	2017-06-23 08:33:37	t
2289	396	162	2017-06-23 08:34:19	t
2573	257	24	2017-06-27 17:30:57	t
3026	150	207	2017-07-03 19:45:32	t
3191	396	95	2017-07-04 18:56:38	t
3192	396	9	2017-07-04 19:01:25	t
3190	396	127	2017-07-04 18:56:01	t
\.


--
-- TOC entry 3888 (class 0 OID 1422222)
-- Dependencies: 212
-- Data for Name: participant_x_communaute; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_communaute (id, id_participant, id_communaute) FROM stdin;
\.


--
-- TOC entry 3890 (class 0 OID 1422227)
-- Dependencies: 214
-- Data for Name: participant_x_contribution; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_contribution (id, id_participant, id_contribution) FROM stdin;
205	532	2
206	107	5
208	508	7
209	296	2
\.


--
-- TOC entry 3892 (class 0 OID 1422232)
-- Dependencies: 216
-- Data for Name: participant_x_formation; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_formation (id, id_participant, id_formation, date_inscription) FROM stdin;
\.


--
-- TOC entry 3894 (class 0 OID 1422237)
-- Dependencies: 218
-- Data for Name: participant_x_inscription; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_inscription (id, id_participant, id_atelier, date_inscription) FROM stdin;
\.


--
-- TOC entry 3896 (class 0 OID 1422242)
-- Dependencies: 220
-- Data for Name: participant_x_preagenda; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_preagenda (id, id_participant, id_agenda, date_inscription) FROM stdin;
3	120	6	2017-05-19 13:31:05
11	120	71	2017-05-19 13:35:00
15	120	73	2017-05-19 13:36:06
18	150	139	2017-05-19 13:36:45
21	120	131	2017-05-19 13:37:40
22	120	132	2017-05-19 13:38:07
23	150	32	2017-05-19 13:38:12
27	120	49	2017-05-19 13:39:34
29	120	30	2017-05-19 13:40:23
30	150	71	2017-05-19 13:41:08
33	150	81	2017-05-19 13:43:35
34	150	130	2017-05-19 13:44:56
211	104	128	2017-05-20 09:52:48
212	104	4	2017-05-20 09:57:19
214	104	73	2017-05-20 10:05:47
215	104	48	2017-05-20 10:10:48
216	104	116	2017-05-20 10:11:11
217	104	71	2017-05-20 10:34:07
218	104	138	2017-05-20 10:45:41
465	206	3	2017-05-23 10:24:37
466	206	139	2017-05-23 10:25:22
467	206	124	2017-05-23 10:27:43
470	206	81	2017-05-23 10:34:05
472	206	66	2017-05-23 10:38:44
473	206	30	2017-05-23 10:42:37
475	206	162	2017-05-23 10:42:49
1176	107	139	2017-06-07 07:56:16
1188	107	162	2017-06-07 08:06:28
1194	107	16	2017-06-07 08:07:51
1195	107	11	2017-06-07 08:09:18
1199	107	14	2017-06-07 08:14:54
1201	107	9	2017-06-07 08:15:58
1205	107	35	2017-06-07 08:19:24
\.


--
-- TOC entry 3898 (class 0 OID 1422247)
-- Dependencies: 222
-- Data for Name: participant_x_reseau; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_reseau (id, id_participant, id_reseau) FROM stdin;
\.


--
-- TOC entry 3900 (class 0 OID 1422252)
-- Dependencies: 224
-- Data for Name: participant_x_role; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.participant_x_role (id, id_participant, id_role) FROM stdin;
1	2	1
4	1	1
\.


--
-- TOC entry 3902 (class 0 OID 1422257)
-- Dependencies: 226
-- Data for Name: preagenda; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.preagenda (id, id_formation, id_salle, date_debut, date_fin, display) FROM stdin;
2	2	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
3	3	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
4	4	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
5	5	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
9	9	21	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
10	10	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
11	11	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
12	12	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
13	13	0	2017-07-05 14:00:00	2017-07-05 17:00:00	\N
14	14	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
15	15	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
16	16	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
18	20	0	2017-07-04 09:00:00	2017-07-04 12:30:00	\N
19	19	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
23	22	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
24	23	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
26	30	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
28	18	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
29	25	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
30	28	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
31	29	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
32	32	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
33	33	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
34	35	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
35	36	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
36	34	0	2017-07-05 10:00:00	2017-07-05 13:00:00	\N
38	40	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
41	37	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
42	38	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
43	41	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
46	42	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
50	49	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
51	50	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
52	51	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
53	56	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
54	59	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
55	60	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
58	57	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
59	58	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
66	55	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
8	8	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
68	86	0	2017-07-04 09:00:00	2017-07-04 10:30:00	\N
69	90	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
70	89	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
71	96	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
72	99	0	2017-07-05 11:00:00	2017-07-05 12:30:00	\N
73	88	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
77	98	0	2017-07-05 14:00:00	2017-07-05 15:30:00	\N
78	101	0	2017-07-05 16:00:00	2017-07-05 17:30:00	\N
80	93	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
82	97	0	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
83	100	0	2017-07-06 16:00:00	2017-07-06 17:30:00	\N
74	91	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
79	92	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
75	94	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
81	95	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
84	109	0	2017-07-04 10:00:00	2017-07-04 13:00:00	\N
85	108	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
88	115	0	2017-07-05 09:00:00	2017-07-05 10:30:00	\N
89	116	0	2017-07-05 11:00:00	2017-07-05 12:30:00	\N
90	111	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
92	113	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
95	121	0	2017-07-05 16:00:00	2017-07-05 17:30:00	\N
96	102	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
129	114	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
97	104	0	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
98	133	0	2017-07-06 16:00:00	2017-07-06 17:30:00	\N
100	127	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
101	103	0	2017-07-05 09:00:00	2017-07-05 10:30:00	\N
102	128	0	2017-07-05 11:00:00	2017-07-05 12:30:00	\N
103	129	0	2017-07-05 09:00:00	2017-07-05 10:30:00	\N
104	130	0	2017-07-05 11:00:00	2017-07-05 12:30:00	\N
105	132	0	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
106	131	0	2017-07-06 16:00:00	2017-07-06 17:30:00	\N
107	105	0	2017-07-05 14:00:00	2017-07-05 15:30:00	\N
108	106	0	2017-07-05 16:00:00	2017-07-05 17:45:00	\N
109	77	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
110	78	0	2017-07-05 14:00:00	2017-07-05 17:30:00	\N
111	74	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
112	76	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
114	79	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
115	80	0	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
116	81	0	2017-07-06 16:00:00	2017-07-06 17:30:00	\N
118	117	0	2017-07-04 11:30:00	2017-07-04 13:00:00	\N
121	75	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
6	6	0	2017-07-04 11:00:00	2017-07-04 12:30:00	\N
125	119	0	2017-07-05 09:00:00	2017-07-05 10:30:00	\N
124	107	0	2017-07-05 09:00:00	2017-07-05 12:30:00	\N
126	120	0	2017-07-05 11:00:00	2017-07-05 12:30:00	\N
127	122	0	2017-07-05 14:00:00	2017-07-05 15:30:00	\N
128	110	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
130	123	0	2017-07-06 09:00:00	2017-07-06 10:30:00	\N
131	125	0	2017-07-06 09:00:00	2017-07-06 10:30:00	\N
132	124	0	2017-07-06 11:00:00	2017-07-06 12:30:00	\N
133	136	0	2017-07-06 14:00:00	2017-07-06 17:30:00	\N
135	85	10	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
136	46	10	2017-07-06 16:00:00	2017-07-06 17:30:00	\N
91	112	0	2017-07-06 09:00:00	2017-07-06 12:30:00	\N
137	138	0	2017-07-06 11:00:00	2017-07-06 12:30:00	\N
48	47	0	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
49	48	0	2017-07-06 16:00:00	2017-07-06 17:30:00	\N
56	71	0	2017-07-05 09:00:00	2017-07-05 10:30:00	\N
62	70	0	2017-07-05 14:00:00	2017-07-05 15:30:00	\N
57	73	0	2017-07-05 11:00:00	2017-07-05 12:30:00	\N
63	72	0	2017-07-05 16:00:00	2017-07-05 17:30:00	\N
60	65	0	2017-07-05 14:00:00	2017-07-05 15:30:00	\N
64	64	0	2017-07-06 09:00:00	2017-07-06 10:30:00	\N
65	62	0	2017-07-06 11:00:00	2017-07-06 12:30:00	\N
138	139	0	2017-07-06 14:00:00	2017-07-06 15:30:00	\N
139	140	21	2017-07-04 14:00:00	2017-07-04 17:00:00	\N
159	143	0	2017-07-04 09:00:00	2017-07-04 10:30:00	40
160	144	0	2017-07-04 10:30:00	2017-07-04 12:00:00	50
161	67	0	2017-07-05 16:00:00	2017-07-05 17:30:00	50
162	145	21	2017-07-07 09:00:00	2017-07-07 12:00:00	10
164	146	7	2017-07-06 14:00:00	2017-07-06 17:30:00	20
99	126	0	2017-07-06 14:00:00	2017-07-06 17:30:00	30
165	147	0	2017-07-06 09:00:00	2017-07-06 12:30:00	30
168	68	0	2017-07-06 09:00:00	2017-07-06 10:30:00	40
113	83	16	2017-07-06 11:00:00	2017-07-06 12:30:00	50
120	135	25	2017-07-06 09:00:00	2017-07-06 10:30:00	40
20	21	0	2017-07-04 09:00:00	2017-07-04 12:30:00	20
22	27	0	2017-07-04 10:00:00	2017-07-04 11:30:00	40
25	24	0	2017-07-05 09:00:00	2017-07-05 12:30:00	20
21	26	0	2017-07-05 09:00:00	2017-07-05 10:30:00	40
17	17	0	2017-07-04 09:00:00	2017-07-04 12:30:00	20
39	44	0	2017-07-05 09:00:00	2017-07-05 10:30:00	40
40	45	0	2017-07-05 11:00:00	2017-07-05 12:30:00	50
122	118	0	2017-07-04 09:00:00	2017-07-04 10:30:00	40
169	52	0	2017-07-04 10:00:00	2017-07-04 13:00:00	30
170	53	0	2017-07-04 10:00:00	2017-07-04 13:00:00	30
67	69	0	2017-07-06 11:00:00	2017-07-06 12:30:00	50
171	82	10	2017-07-06 09:00:00	2017-07-06 10:30:00	40
172	148	11	2017-07-06 11:00:00	2017-07-06 12:30:00	50
117	84	20	2017-07-06 14:00:00	2017-07-06 15:30:00	40
173	149	20	2017-07-06 15:30:00	2017-07-06 17:00:00	50
175	150	0	2017-07-06 14:00:00	2017-07-06 15:30:00	30
\.


--
-- TOC entry 3904 (class 0 OID 1422262)
-- Dependencies: 228
-- Data for Name: reseau; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.reseau (id, label, display) FROM stdin;
1	Devlog	\N
2	Rbdd	\N
3	Calcul	\N
4	Resinfo	\N
5	Rde	\N
6	Rdm	\N
7	QeR	\N
8	Renatis	\N
9	Medici	\N
10	RDE ou RDEI	\N
11	Cogiter	\N
12	Mutec	\N
13	RI3	\N
14	Obtenir	\N
15	PEPI	\N
16	DBA	\N
17	CRI	\N
18	DSI	\N
19	Bioinformatique	\N
20	Plate-forme de calcul	\N
21	Statistique	\N
22	Science des données	\N
23	Développement web	\N
24	Web des données	\N
25	Logiciel	\N
26	Développement mobile	\N
27	Génie logiciel	\N
28	Business intelligence	\N
29	Architecte	\N
30	Cloud	\N
31	Intégration	\N
32	Fonctionnel	\N
33	MOA	\N
34	MOE	\N
35	Agilité	\N
36	Qualité	\N
37	CRM	\N
38	Sécurité	\N
39	Systèmes et réseaux	\N
\.


--
-- TOC entry 3906 (class 0 OID 1422267)
-- Dependencies: 230
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.role (id, label) FROM stdin;
1	CLO
2	CP
3	Contributeur
\.


--
-- TOC entry 3908 (class 0 OID 1422272)
-- Dependencies: 232
-- Data for Name: salle; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.salle (id, nom, etage, aile, description, quota_officiel, quota_physique, wifi, reseau, videoprojecteur) FROM stdin;
6	S-109	1	-	salle info	24	24	-	0	t
7	S-110	1	-	salle info	24	24	-	0	t
8	S-111	1	-	salle info	24	24	-	0	t
9	S-112	1	-	3 prises éléctriques	40	40	-	0	f
10	S-113	1	-	4 prises éléctriques	32	32	-	0	f
11	S-114	1	-	-	40	40	-	0	f
12	S-115	1	-	3 prises éléctriques	40	40	-	0	f
13	S-116	1	-	4 prises éléctriques	40	40	-	0	f
14	S-212	2	-	3 (aér. Poinso)	28	28	-	0	f
15	S-309	3	-	4 prises éléctriques	40	40	-	0	f
16	S-310	3	-	4 prises éléctriques	40	40	-	0	f
17	S-311	3	-	4 prises électriques	40	40	-	0	f
18	S-312	3	-	4 prises électriques	40	40	-	0	f
19	S-313	3	-	4 prises électriques	40	40	-	0	t
20	S-314	3	-	4 prises électriques	40	40	-	0	t
21	A-Emerigon	2	-	2+3 (local) prises électriques	600	600	-	0	f
22	A-Berryer	1	-	7+(4 117A)	300	300	-	0	f
23	A-Jourdan	0	-	7 prises électriques	250	250	-	0	f
25	A-Poinso	0	-	10 prises électrique	115	115	-	0	f
0	sans salle	0	-	-	0	0	-	0	f
\.


--
-- TOC entry 3910 (class 0 OID 1422280)
-- Dependencies: 234
-- Data for Name: thematique; Type: TABLE DATA; Schema: public; Owner: jdev
--

COPY public.thematique (id, nom, label) FROM stdin;
1	T1 - Systèmes embarqués, les réseaux de capteurs et l'internet des objets	T1
2	T2 - Ingénierie et web des données	T2
3	T3 - Programmation de la matière, fabriques personnelles	T3
4	T4 - Usines logicielles et outils de production de code	T4
5	T5 - Infrastructures logicielles et science ouverte	T5
6	T6 - Méthodes et techniques de production de logiciel	T6
7	T7 - Science des données et apprentissage automatique	T7
8	T8 - Parallélisme itinérant, virtualisation et reproductibilité	T8
\.


--
-- TOC entry 3920 (class 0 OID 0)
-- Dependencies: 197
-- Name: agenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.agenda_id_seq', 209, true);


--
-- TOC entry 3921 (class 0 OID 0)
-- Dependencies: 199
-- Name: atelier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.atelier_id_seq', 1, false);


--
-- TOC entry 3922 (class 0 OID 0)
-- Dependencies: 201
-- Name: communaute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.communaute_id_seq', 17, true);


--
-- TOC entry 3923 (class 0 OID 0)
-- Dependencies: 203
-- Name: contribution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.contribution_id_seq', 6, true);


--
-- TOC entry 3924 (class 0 OID 0)
-- Dependencies: 205
-- Name: formation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.formation_id_seq', 155, true);


--
-- TOC entry 3925 (class 0 OID 0)
-- Dependencies: 207
-- Name: organisme_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.organisme_id_seq', 19, true);


--
-- TOC entry 3926 (class 0 OID 0)
-- Dependencies: 209
-- Name: participant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_id_seq', 605, true);


--
-- TOC entry 3927 (class 0 OID 0)
-- Dependencies: 211
-- Name: participant_x_agenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_agenda_id_seq', 3441, true);


--
-- TOC entry 3928 (class 0 OID 0)
-- Dependencies: 213
-- Name: participant_x_communaute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_communaute_id_seq', 429, true);


--
-- TOC entry 3929 (class 0 OID 0)
-- Dependencies: 215
-- Name: participant_x_contribution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_contribution_id_seq', 209, true);


--
-- TOC entry 3930 (class 0 OID 0)
-- Dependencies: 217
-- Name: participant_x_formation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_formation_id_seq', 1, false);


--
-- TOC entry 3931 (class 0 OID 0)
-- Dependencies: 219
-- Name: participant_x_inscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_inscription_id_seq', 1, false);


--
-- TOC entry 3932 (class 0 OID 0)
-- Dependencies: 221
-- Name: participant_x_preagenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_preagenda_id_seq', 1512, true);


--
-- TOC entry 3933 (class 0 OID 0)
-- Dependencies: 223
-- Name: participant_x_reseau_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_reseau_id_seq', 1347, true);


--
-- TOC entry 3934 (class 0 OID 0)
-- Dependencies: 225
-- Name: participant_x_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.participant_x_role_id_seq', 4, true);


--
-- TOC entry 3935 (class 0 OID 0)
-- Dependencies: 227
-- Name: preagenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.preagenda_id_seq', 175, true);


--
-- TOC entry 3936 (class 0 OID 0)
-- Dependencies: 229
-- Name: reseau_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.reseau_id_seq', 39, true);


--
-- TOC entry 3937 (class 0 OID 0)
-- Dependencies: 231
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.role_id_seq', 3, true);


--
-- TOC entry 3938 (class 0 OID 0)
-- Dependencies: 233
-- Name: salle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.salle_id_seq', 25, true);


--
-- TOC entry 3939 (class 0 OID 0)
-- Dependencies: 235
-- Name: thematique_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jdev
--

SELECT pg_catalog.setval('public.thematique_id_seq', 1, false);


--
-- TOC entry 3663 (class 2606 OID 1422290)
-- Name: agenda agenda_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT agenda_pkey PRIMARY KEY (id);


--
-- TOC entry 3667 (class 2606 OID 1422292)
-- Name: atelier atelier_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.atelier
    ADD CONSTRAINT atelier_pkey PRIMARY KEY (id);


--
-- TOC entry 3671 (class 2606 OID 1422294)
-- Name: communaute communaute_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.communaute
    ADD CONSTRAINT communaute_pkey PRIMARY KEY (id);


--
-- TOC entry 3673 (class 2606 OID 1422296)
-- Name: contribution contribution_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.contribution
    ADD CONSTRAINT contribution_pkey PRIMARY KEY (id);


--
-- TOC entry 3675 (class 2606 OID 1422298)
-- Name: formation formation_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.formation
    ADD CONSTRAINT formation_pkey PRIMARY KEY (id);


--
-- TOC entry 3679 (class 2606 OID 1422300)
-- Name: organisme organisme_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.organisme
    ADD CONSTRAINT organisme_pkey PRIMARY KEY (id);


--
-- TOC entry 3682 (class 2606 OID 1422302)
-- Name: participant participant_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT participant_pkey PRIMARY KEY (id);


--
-- TOC entry 3686 (class 2606 OID 1422304)
-- Name: participant_x_agenda participant_x_agenda_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_agenda
    ADD CONSTRAINT participant_x_agenda_pkey PRIMARY KEY (id);


--
-- TOC entry 3690 (class 2606 OID 1422306)
-- Name: participant_x_communaute participant_x_communaute_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_communaute
    ADD CONSTRAINT participant_x_communaute_pkey PRIMARY KEY (id);


--
-- TOC entry 3694 (class 2606 OID 1422308)
-- Name: participant_x_contribution participant_x_contribution_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_contribution
    ADD CONSTRAINT participant_x_contribution_pkey PRIMARY KEY (id);


--
-- TOC entry 3698 (class 2606 OID 1422310)
-- Name: participant_x_formation participant_x_formation_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_formation
    ADD CONSTRAINT participant_x_formation_pkey PRIMARY KEY (id);


--
-- TOC entry 3702 (class 2606 OID 1422312)
-- Name: participant_x_inscription participant_x_inscription_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_inscription
    ADD CONSTRAINT participant_x_inscription_pkey PRIMARY KEY (id);


--
-- TOC entry 3706 (class 2606 OID 1422314)
-- Name: participant_x_preagenda participant_x_preagenda_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_preagenda
    ADD CONSTRAINT participant_x_preagenda_pkey PRIMARY KEY (id);


--
-- TOC entry 3710 (class 2606 OID 1422316)
-- Name: participant_x_reseau participant_x_reseau_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_reseau
    ADD CONSTRAINT participant_x_reseau_pkey PRIMARY KEY (id);


--
-- TOC entry 3714 (class 2606 OID 1422318)
-- Name: participant_x_role participant_x_role_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_role
    ADD CONSTRAINT participant_x_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3718 (class 2606 OID 1422320)
-- Name: preagenda preagenda_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.preagenda
    ADD CONSTRAINT preagenda_pkey PRIMARY KEY (id);


--
-- TOC entry 3720 (class 2606 OID 1422322)
-- Name: reseau reseau_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.reseau
    ADD CONSTRAINT reseau_pkey PRIMARY KEY (id);


--
-- TOC entry 3722 (class 2606 OID 1422324)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 3724 (class 2606 OID 1422326)
-- Name: salle salle_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.salle
    ADD CONSTRAINT salle_pkey PRIMARY KEY (id);


--
-- TOC entry 3726 (class 2606 OID 1422328)
-- Name: thematique thematique_pkey; Type: CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.thematique
    ADD CONSTRAINT thematique_pkey PRIMARY KEY (id);


--
-- TOC entry 3699 (class 1259 OID 1422329)
-- Name: idx_118373b03f26b153; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_118373b03f26b153 ON public.participant_x_inscription USING btree (id_atelier);


--
-- TOC entry 3700 (class 1259 OID 1422330)
-- Name: idx_118373b0cf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_118373b0cf8da6e6 ON public.participant_x_inscription USING btree (id_participant);


--
-- TOC entry 3711 (class 1259 OID 1422331)
-- Name: idx_28a19de2cf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_28a19de2cf8da6e6 ON public.participant_x_role USING btree (id_participant);


--
-- TOC entry 3712 (class 1259 OID 1422332)
-- Name: idx_28a19de2dc499668; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_28a19de2dc499668 ON public.participant_x_role USING btree (id_role);


--
-- TOC entry 3664 (class 1259 OID 1422333)
-- Name: idx_2cedc877a0123f6c; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_2cedc877a0123f6c ON public.agenda USING btree (id_salle);


--
-- TOC entry 3665 (class 1259 OID 1422334)
-- Name: idx_2cedc877c0759d98; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_2cedc877c0759d98 ON public.agenda USING btree (id_formation);


--
-- TOC entry 3695 (class 1259 OID 1422335)
-- Name: idx_3b03170bc0759d98; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_3b03170bc0759d98 ON public.participant_x_formation USING btree (id_formation);


--
-- TOC entry 3696 (class 1259 OID 1422336)
-- Name: idx_3b03170bcf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_3b03170bcf8da6e6 ON public.participant_x_formation USING btree (id_participant);


--
-- TOC entry 3676 (class 1259 OID 1422337)
-- Name: idx_404021bf9f04557f; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_404021bf9f04557f ON public.formation USING btree (id_thematique);


--
-- TOC entry 3691 (class 1259 OID 1422338)
-- Name: idx_4eabc9fdc9f46b67; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_4eabc9fdc9f46b67 ON public.participant_x_contribution USING btree (id_contribution);


--
-- TOC entry 3692 (class 1259 OID 1422339)
-- Name: idx_4eabc9fdcf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_4eabc9fdcf8da6e6 ON public.participant_x_contribution USING btree (id_participant);


--
-- TOC entry 3683 (class 1259 OID 1422340)
-- Name: idx_5c64203bada7d9a; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_5c64203bada7d9a ON public.participant_x_agenda USING btree (id_agenda);


--
-- TOC entry 3684 (class 1259 OID 1422341)
-- Name: idx_5c64203bcf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_5c64203bcf8da6e6 ON public.participant_x_agenda USING btree (id_participant);


--
-- TOC entry 3715 (class 1259 OID 1422342)
-- Name: idx_bc7e4b00a0123f6c; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_bc7e4b00a0123f6c ON public.preagenda USING btree (id_salle);


--
-- TOC entry 3716 (class 1259 OID 1422343)
-- Name: idx_bc7e4b00c0759d98; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_bc7e4b00c0759d98 ON public.preagenda USING btree (id_formation);


--
-- TOC entry 3707 (class 1259 OID 1422344)
-- Name: idx_bd6cc4f4cf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_bd6cc4f4cf8da6e6 ON public.participant_x_reseau USING btree (id_participant);


--
-- TOC entry 3708 (class 1259 OID 1422345)
-- Name: idx_bd6cc4f4ebd29955; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_bd6cc4f4ebd29955 ON public.participant_x_reseau USING btree (id_reseau);


--
-- TOC entry 3703 (class 1259 OID 1422346)
-- Name: idx_c73d7db4ada7d9a; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_c73d7db4ada7d9a ON public.participant_x_preagenda USING btree (id_agenda);


--
-- TOC entry 3704 (class 1259 OID 1422347)
-- Name: idx_c73d7db4cf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_c73d7db4cf8da6e6 ON public.participant_x_preagenda USING btree (id_participant);


--
-- TOC entry 3680 (class 1259 OID 1422348)
-- Name: idx_d79f6b115d3af914; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_d79f6b115d3af914 ON public.participant USING btree (id_organisme);


--
-- TOC entry 3668 (class 1259 OID 1422349)
-- Name: idx_e1bb1823476556af; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_e1bb1823476556af ON public.atelier USING btree (thematique_id);


--
-- TOC entry 3669 (class 1259 OID 1422350)
-- Name: idx_e1bb1823dc304035; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_e1bb1823dc304035 ON public.atelier USING btree (salle_id);


--
-- TOC entry 3687 (class 1259 OID 1422351)
-- Name: idx_edbe733a8443c74e; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_edbe733a8443c74e ON public.participant_x_communaute USING btree (id_communaute);


--
-- TOC entry 3688 (class 1259 OID 1422352)
-- Name: idx_edbe733acf8da6e6; Type: INDEX; Schema: public; Owner: jdev
--

CREATE INDEX idx_edbe733acf8da6e6 ON public.participant_x_communaute USING btree (id_participant);


--
-- TOC entry 3677 (class 1259 OID 1422353)
-- Name: uniq_404021bf6c6e55b5; Type: INDEX; Schema: public; Owner: jdev
--

CREATE UNIQUE INDEX uniq_404021bf6c6e55b5 ON public.formation USING btree (nom);


--
-- TOC entry 3742 (class 2606 OID 1422354)
-- Name: participant_x_inscription fk_118373b03f26b153; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_inscription
    ADD CONSTRAINT fk_118373b03f26b153 FOREIGN KEY (id_atelier) REFERENCES public.atelier(id);


--
-- TOC entry 3741 (class 2606 OID 1422359)
-- Name: participant_x_inscription fk_118373b0cf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_inscription
    ADD CONSTRAINT fk_118373b0cf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3748 (class 2606 OID 1422364)
-- Name: participant_x_role fk_28a19de2cf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_role
    ADD CONSTRAINT fk_28a19de2cf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3747 (class 2606 OID 1422369)
-- Name: participant_x_role fk_28a19de2dc499668; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_role
    ADD CONSTRAINT fk_28a19de2dc499668 FOREIGN KEY (id_role) REFERENCES public.role(id);


--
-- TOC entry 3728 (class 2606 OID 1422374)
-- Name: agenda fk_2cedc877a0123f6c; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT fk_2cedc877a0123f6c FOREIGN KEY (id_salle) REFERENCES public.salle(id);


--
-- TOC entry 3727 (class 2606 OID 1422379)
-- Name: agenda fk_2cedc877c0759d98; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT fk_2cedc877c0759d98 FOREIGN KEY (id_formation) REFERENCES public.formation(id);


--
-- TOC entry 3740 (class 2606 OID 1422384)
-- Name: participant_x_formation fk_3b03170bc0759d98; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_formation
    ADD CONSTRAINT fk_3b03170bc0759d98 FOREIGN KEY (id_formation) REFERENCES public.formation(id);


--
-- TOC entry 3739 (class 2606 OID 1422389)
-- Name: participant_x_formation fk_3b03170bcf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_formation
    ADD CONSTRAINT fk_3b03170bcf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3731 (class 2606 OID 1422394)
-- Name: formation fk_404021bf9f04557f; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.formation
    ADD CONSTRAINT fk_404021bf9f04557f FOREIGN KEY (id_thematique) REFERENCES public.thematique(id);


--
-- TOC entry 3738 (class 2606 OID 1422399)
-- Name: participant_x_contribution fk_4eabc9fdc9f46b67; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_contribution
    ADD CONSTRAINT fk_4eabc9fdc9f46b67 FOREIGN KEY (id_contribution) REFERENCES public.contribution(id);


--
-- TOC entry 3737 (class 2606 OID 1422404)
-- Name: participant_x_contribution fk_4eabc9fdcf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_contribution
    ADD CONSTRAINT fk_4eabc9fdcf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3734 (class 2606 OID 1422409)
-- Name: participant_x_agenda fk_5c64203bada7d9a; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_agenda
    ADD CONSTRAINT fk_5c64203bada7d9a FOREIGN KEY (id_agenda) REFERENCES public.agenda(id);


--
-- TOC entry 3733 (class 2606 OID 1422414)
-- Name: participant_x_agenda fk_5c64203bcf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_agenda
    ADD CONSTRAINT fk_5c64203bcf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3750 (class 2606 OID 1422419)
-- Name: preagenda fk_bc7e4b00a0123f6c; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.preagenda
    ADD CONSTRAINT fk_bc7e4b00a0123f6c FOREIGN KEY (id_salle) REFERENCES public.salle(id);


--
-- TOC entry 3749 (class 2606 OID 1422424)
-- Name: preagenda fk_bc7e4b00c0759d98; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.preagenda
    ADD CONSTRAINT fk_bc7e4b00c0759d98 FOREIGN KEY (id_formation) REFERENCES public.formation(id);


--
-- TOC entry 3746 (class 2606 OID 1422429)
-- Name: participant_x_reseau fk_bd6cc4f4cf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_reseau
    ADD CONSTRAINT fk_bd6cc4f4cf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3745 (class 2606 OID 1422434)
-- Name: participant_x_reseau fk_bd6cc4f4ebd29955; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_reseau
    ADD CONSTRAINT fk_bd6cc4f4ebd29955 FOREIGN KEY (id_reseau) REFERENCES public.reseau(id);


--
-- TOC entry 3744 (class 2606 OID 1422439)
-- Name: participant_x_preagenda fk_c73d7db4ada7d9a; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_preagenda
    ADD CONSTRAINT fk_c73d7db4ada7d9a FOREIGN KEY (id_agenda) REFERENCES public.preagenda(id);


--
-- TOC entry 3743 (class 2606 OID 1422444)
-- Name: participant_x_preagenda fk_c73d7db4cf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_preagenda
    ADD CONSTRAINT fk_c73d7db4cf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


--
-- TOC entry 3732 (class 2606 OID 1422449)
-- Name: participant fk_d79f6b115d3af914; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT fk_d79f6b115d3af914 FOREIGN KEY (id_organisme) REFERENCES public.organisme(id);


--
-- TOC entry 3730 (class 2606 OID 1422454)
-- Name: atelier fk_e1bb1823476556af; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.atelier
    ADD CONSTRAINT fk_e1bb1823476556af FOREIGN KEY (thematique_id) REFERENCES public.thematique(id);


--
-- TOC entry 3729 (class 2606 OID 1422459)
-- Name: atelier fk_e1bb1823dc304035; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.atelier
    ADD CONSTRAINT fk_e1bb1823dc304035 FOREIGN KEY (salle_id) REFERENCES public.salle(id);


--
-- TOC entry 3736 (class 2606 OID 1422464)
-- Name: participant_x_communaute fk_edbe733a8443c74e; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_communaute
    ADD CONSTRAINT fk_edbe733a8443c74e FOREIGN KEY (id_communaute) REFERENCES public.communaute(id);


--
-- TOC entry 3735 (class 2606 OID 1422469)
-- Name: participant_x_communaute fk_edbe733acf8da6e6; Type: FK CONSTRAINT; Schema: public; Owner: jdev
--

ALTER TABLE ONLY public.participant_x_communaute
    ADD CONSTRAINT fk_edbe733acf8da6e6 FOREIGN KEY (id_participant) REFERENCES public.participant(id);


-- Completed on 2020-01-27 14:49:19 CET

--
-- PostgreSQL database dump complete
--

