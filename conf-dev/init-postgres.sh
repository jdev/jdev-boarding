#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER jdev LOGIN PASSWORD 'jdev';
    CREATE DATABASE jdevdb;
    GRANT ALL PRIVILEGES ON DATABASE jdevdb TO jdev;
EOSQL
psql -v ON_ERROR_STOP=1 -f /sql/jdevdb.sql --username "$POSTGRES_USER" --dbname "jdevdb"