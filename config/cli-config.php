<?php

/*
 * This file is part of JDEV-BOARDING
 *
 * (c) François Agneray <francois.agneray@lam.fr>
 * (c) Chrystel Moreau <chrystel.moreau@lam.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require 'vendor/autoload.php';

$settings = require './app/settings.php';
$database = $settings['settings']['database'];

$c = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(array('app/src/Entity'), $database['dev_mode']);
$c->setProxyDir(getcwd() . '/' . $database['path_proxy']);
if ($database['dev_mode']) {
    $c->setAutoGenerateProxyClasses(true);
} else {
    $c->setAutoGenerateProxyClasses(false);
}
$em = \Doctrine\ORM\EntityManager::create($database['connection_options'], $c);

$helpers = new Symfony\Component\Console\Helper\HelperSet(array(
  'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
  'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);
