$(document).ready(function() {
    
    // $( "#programme" ).tabs({
    //   collapsible: true
    // });
    
    
    $('#statut').change(function() {
        var statut = $(this).val();

        switch(statut) {
            case 'ETUDIANT':
                // $('#organisme option').remove();
                $('#emploi option').remove();
                $('#emploi')
                    .append('<option value="Doctorant">Doctorant</option>')
                    .append('<option value="Autre">Autre</option>');
                changePass();
                break;
            case 'ACADEMIQUE':
                // $('#organisme option').remove();
                $('#emploi option').remove();
                $('#emploi')
                    .append('<option value="Chercheur">Chercheur</option>')
                    .append('<option value="Enseignant-Chercheur">Enseignant-Chercheur</option>')            
                    .append('<option value="Ingénieur">Ingénieur</option>')
                    .append('<option value="Post-Doctorant">Post-Doctorant</option>')
                    .append('<option value="Stagiaire">Stagiaire</option>')            
                    .append('<option value="Technicien">Technicien</option>');
                changePass();
                break;
            case 'INDUSTRIEL':
                // $('#organisme option').remove();
                $('#emploi option').remove();
                $('#emploi')
                    .append('<option value="Ingénieur">Ingénieur</option>')
                    .append('<option value="Technicien">Technicien</option>')
                    .append('<option value="Autre">Autre</option>');
                // $('#organisme')
                //     .append('<option value="11">SOCIETE PRIVEE</option>');
                changePass();
                break;
            default:
                break;
        }
    });
    
    
    $('#organisme').change(function() {
        var org = $(this).val();

        switch(org) {
            case '2':
                $("#unite").attr("placeholder", "Ex: Unité; Votre centre et département de rattachement; Pépi, CATI");
                break;          
            default:
                $("#unite").attr("placeholder", "Ex: UMRxxx, UMSccc, UPRfff, UPSggg");
                break;
        }
    });
    
    $('input[name="role"]').change(changePass);  

    $('#new-passwdForm').validator();

    $('#myForm').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var form = $(this);

            $.get('verif-email?email=' + $('#email').val(), function(data) {
                if (data) {
                    $.ajax({
                        url: 'inscription',
                        type: 'POST',
                        data: form.serialize(),
                        success: function(text) {
                            document.location.href = 'valid-email';
                        },
                        error: function() {
                            alert('Votre inscription n\'a pas pu être effectuée!');
                            form.trigger('reset');
                        }
                    });
                } else {
                    alert('Vous êtes déjà inscrit aux JDEVs!');
                    form.trigger('reset');
                }
            });
        }
    });
});

var changePass = function () {
    var role = $('input[name="role"]:checked').val();
    var statut = $('#statut').val();

    switch(role) {
        case 'Participant':
            $('#type_inscription option').remove();
            $('#type_inscription').append('<option value="PASS COMPLET">PASS COMPLET</option>');
            if (statut === 'INDUSTRIEL') {
                $('#type_inscription').append('<option value="PASS SOUTIEN">PASS SOUTIEN</option>');
                $('#type_inscription').append('<option value="PASS LIBRE">PASS LIBRE</option>');
            }
            break;
        case 'Accompagnant':
            $('#type_inscription option').remove();
            $('#type_inscription').append('<option value="PASS ACCOMPAGNANT">PASS ACCOMPAGNANT</option>');
            break;
        case 'Organisateur':
            $('#type_inscription option').remove();
            $('#type_inscription').append('<option value="PASS ORGA">PASS ORGA</option>');
            break;
        case 'Invité':
            $('#type_inscription option').remove();
            $('#type_inscription').append('<option value="PASS INVITE">PASS INVITE</option>');
            break;
        case 'Exposant':
            $('#type_inscription option').remove();
            $('#type_inscription').append('<option value="PASS EXPOSANT">PASS EXPOSANT</option>');
            break;  
        case 'Sponsor':
            $('#type_inscription option').remove();
            $('#type_inscription').append('<option value="PASS SPONSOR">PASS SPONSOR</option>');
            break;            
        default:
            break;
    }
} 

    