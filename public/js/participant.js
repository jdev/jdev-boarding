var intervalPointer;
var jdev_annee='2017';


$(document).ready(function(){

    // ======================================================================
    // Gestion edition du profil
    // ======================================================================
    $('#editProfilForm').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var form = $(this);
            var token = $("#token").val();
            $.ajax({
                url: 'dashboard-participant-edit-form?token=' + token,
                type: 'POST',
                data: form.serialize(),
                success: function(text) {
                    document.location.href = 'dashboard-home?token=' + token;
                },
                error: function() {
                    alert('La modification de votre profil n\'a pas pu être effectué!');
                    form.trigger('reset');
                }
            });
        }
    });

    // ======================================================================
    // Gestion du sondage
    // ======================================================================
    $( "#tabs_FormByThem" ).tabs({
        collapsible: true,
        activate: function (event, ui) {
            window.location.hash = 'tab=' + $(this).tabs('option', 'active');
        },
        create: function () {
            if (window.location.hash) {
                var active = parseInt(window.location.hash.replace('#tab=', ''));
                $(this).tabs("option", "active", active);
            }
        }
    });
    

    $("#tabs_FormByThem_loading").hide();
    $("#tabs_FormByThem").show();
    
    $('.btn-sondage-formation').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous la sélection de cette formation ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('PB sélection');
                }
            });
        }
    });
    
    $('.btn-delete-sondage-formation').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous la suppression de la sélection de cette formation ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('PB suppression sélection');
                }
            });
        }
    });

    $('#tab_sondage').DataTable({
        "columnDefs":[
            { 
                "targets" : 0,
                "render": function (data) {					       
                    return "<b><a href=\"http://devlog.cnrs.fr/jdev" + jdev_annee + "/" + data + "\" target=_blank><i class=\"fa fa-external-link\"></i> " + data.toUpperCase()+"</a></b>";
                } 
            }          
        ]      
    });
    
    

    // ======================================================================
    // Affichage de l'Agenda
    // ====================================================================== 
    
    $('#tab_programme').DataTable( {
        dom: 'Brt',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Exporter en PDF',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdevs-agenda', 
                customize: function ( doc ) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            if ( (data !== 'Matin') &&  (data !== 'Après-Midi') &&  (data.length !== 0)){
                                data=data.replace(/<a href="http:\/\/devlog.cnrs.fr/g,'' );
                                data=data.replace(/<\/a>/g,'' );

                                var formation=data.split('/jdev'+jdev_annee+'/');
                                var djournee='';

                                for (i=1;i<formation.length;i++){    
                                    var label=formation[i].split('<br>">');

                                    // verification que la thématique est selectionnée (checkbox)
                                    for (var nb=0; nb<=$('#nb_them').val(); nb++) {   
                                        if ($('#chkb_t' + nb).is(':checked')) {
                                            if (label[1][1] == nb){ 
                                                label[1]=label[1].replace(/\n/g,'' );
                                                label[0]=label[0].replace(/\n/g,'' );
                                                // ajout des infos sur la formation (intitulé, ...)
                                                var info=label[0].split('<p>');
                                                if (info.length !== 0){
                                                    info[1]=info[1].replace(/<b>/g,'' );
                                                    info[1]=info[1].replace(/<\/b>/g,'' );
                                                    info[1]=info[1].replace(/<br>/g,'\n');
                                                    console.log (label[1]+'\n'+info[1]);
                                                }                                                
                                                djournee+=label[1].toUpperCase()+'\n'+info[1]+'\n\n';
                                            }
                                        }
                                    }                                 
                                }                               
                                
                                // gestion de l'affichage si 0 session sur 1/2 journée
                                if (djournee.length === 0){ 
                                    djournee='-' + '\n';
                                }  
                                
                                data=djournee; 
                            }
                                                  
                            return data;                        
                        }
                    }
                }                
            }            
        ],      
        "ordering": false
    });    

    // Affichage des sessions d'une thématique donnée
    $(':checkbox').click(function() {
        for (var i=1; i<=$('#nb_them').val(); i++) {   
            if ($('#chkb_t' + i).is(':checked')) {
                $('a.t' + i).show();        
            }
            else {
                $('a.t' + i).hide();
            }
        }
    });
  

    // ======================================================================
    // Gestion du parcours
    // ======================================================================

    var tab_agendas = $('#tab_agenda');
    if (tab_agendas) {
        intervalPointer = setInterval(refreshSessions, 5000);
    }
    
    $('.btn-inscription-formation').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous l'inscription à cette formation ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.responseText);
                    alert('PB inscription');
                }
            });
        }
    });
    
    $('.btn-delete-inscription-formation').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous la suppression de l'inscription à cette formation ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('PB suppression inscription');
                }
            });
        }
    });

   
    
    $('#tab_mon_parcours').DataTable( {
        dom: 'Brt',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Exporter en PDF',
                orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdevs-parcours', 
                customize: function ( doc ) {
                    doc.defaultStyle.fontSize = 10;
                    doc.defaultStyle.lineHeight = 1;
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (data.indexOf('Matin') != -1){
                                data='Matin';                               
                            }
                            if (data.indexOf('Après-Midi') != -1){
                                data='Après-Midi';                               
                            }   
                            data=data.replace(/<b>/g,'' );
                            data=data.replace(/<\/b>/g,'' );
                            data=data.replace(/\n/g,'' );
                            data=data.replace(/<br>/g,'\n' );
                            data=data.replace(/<p><\/p>/g,'\n\n' );
                            return data;                        
                        }
                    }
                }                
            },            
            {
                extend: 'pdfHtml5',
                text: 'Exporter au format Badge',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdevs-parcours-badge',
                customize: function ( doc ) {
                    doc.pageMargins = [ 40, 0, 800, 1 ]; //pageMargins [left, top, right, bottom]
                    doc.defaultStyle.fontSize = 8;
                    doc.defaultStyle.lineHeight = 0.8;
                    doc.styles.title.fontSize = 9;
                    doc.styles.title.bold = true;
                    doc.styles.tableHeader.fontSize = 9;                    
                    doc.styles.tableHeader.alignment = 'left';
                    doc.styles.tableHeader.noWrap = true;
                    doc.styles.tableHeader.lineHeight = 1;                    
                    doc.styles.tableHeader.bold = true;
                    doc.styles.tableHeader.color = '#ffffff';
                    doc.styles.tableHeader.fillColor = '#666666';
                    doc.styles.tableBodyOdd.fillColor = '#ffffff';
                    doc.styles.tableBodyEven.fillColor = '#e9e9e9';                    
                },              
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) {                             
                            if ( (data.indexOf('Matin') != -1) || (data.indexOf('Après-Midi') != -1) ){
                                data='';                               
                            }
                            else {
                                var tab=data.split("<br>");
                                for (var i=0; i<tab.length; i++) {                                    
                                    tab[i]=tab[i].replace(/<b>Formation<\/b> :/g,'' );
                                    tab[i]=tab[i].replace(/<b>Intitulé<\/b> :/g,'' );
                                    tab[i]=tab[i].replace(/<b>Horaire<\/b> :/g,'' );
                                    tab[i]=tab[i].replace(/<b>Salle<\/b> :/g,'' );
                                    tab[i]=tab[i].replace(/\n/g,'' );                                    
                                    tab[i]=tab[i].replace(/<br>/g,'\n' );
                                    tab[i]=tab[i].replace(/<p><\/p>/g,'' );
                                }
                                
                                if (tab[0].indexOf('inscription') != -1){
                                    data='-';                               
                                }
                                else {
                                    if (tab.length <= 5){
                                        data=tab[0].toUpperCase()+'\n'+tab[2]+'\n'+tab[3]+'\n'; 
                                    }
                                    else if  (tab.length <= 9){
                                        data=tab[0].toUpperCase()+'\n'+tab[2]+'\n'+tab[3]+'\n\n'+tab[4].toUpperCase()+'\n'+tab[6]+'\n'+tab[7]+'\n'; 
                                    }
                                    else {
                                        data=tab[0].toUpperCase()+'\n'+tab[2]+'\n'+tab[3]+'\n\n'+tab[4].toUpperCase()+'\n'+tab[6]+'\n'+tab[7]+'\n\n'+tab[8].toUpperCase()+'\n'+tab[10]+'\n'+tab[11]+'\n'; 
                                    }
                                }                                  
                            }
                            return data;                        
                        }
                    }
                }                
            }            
        ],      
        "ordering": false
    });    

    $('#tab_agenda').DataTable({
        "pageLength": 50,
        "dom": 'frtilp',   
        "columnDefs":[
            { 
            "targets" : 0,
                "render": function (data) {					       
                    return "<b><a href=\"http://devlog.cnrs.fr/jdev" + jdev_annee + "/" + data + "\" target=_blank><i class=\"fa fa-external-link\"></i> " + data.toUpperCase()+"</a></b>";
                }
            }    
        ],    
        "ordering": true,
        "order": [[3, 'asc']]
    } );    
        
});

var refreshSessions = function(token, jour, debut, fin) {
    var token = $('#tab_agenda').data('token');
    var jour = $('#tab_agenda').data('jour');
    var debut = $('#tab_agenda').data('debut');
    var fin = $('#tab_agenda').data('fin');

    if (!token || !jour || !debut || !fin) {
        console.log('Manque des valeurs pour lancer le refresh parcours');
        clearInterval(intervalPointer);
        return ;
    }

    $.ajax({
        url: 'dashboard-participant-sessions-refresh?token=' + token + '&jour=' + jour + '&debut=' + debut + '&fin=' + fin,
        type: 'GET',
        success: function(data) {
            data.agendas.forEach(function(agenda) {
                // gestion du Nb inscrits / quota
                $('#nb_inscrits_' + agenda.id).text(agenda.nb_inscrits);
                
                // gestion du Badge
                var prct = Math.round(((agenda.nb_inscrits / agenda.quota) * 100));
                var badge = '';
                if (prct <= 50) {
                    badge = 'vert';
                } else if (prct > 50 && prct <= 75) {
                    badge = 'orange';
                } else {
                    badge = 'rouge';
                }
                $('#nb_inscrits_prct_' + agenda.id).html('<span class="badge badge-' + badge + '">' + prct + '%</span>');

                // gestion du bouton Inscription/Desinscription
                if (agenda.nb_inscrits >= agenda.quota){
                    if ( $('#vinscrit_' + agenda.id).val() == 1)    { $('#action_' + agenda.id + " a").show();}
                    else                                            { $('#action_' + agenda.id + " a").hide();}
                }
                else {
                    $('#action_' + agenda.id + " a").show(); 
                   //$('#action_' + agenda.id).show(); 
                }
            });
        },
        error: function() {
            console.log('PB refresh sessions');
            clearInterval(intervalPointer);
        }
    });
}