$(document).ready(function() {
    $('#new-passwdForm').validator();

    $('#new-passwdForm').on('submit', function(e) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            url: 'new-passwd',
            type: 'POST',
            data: form.serialize(),
            dataType: 'text',
            success: function() {
                document.location.href = 'connexion';
            },
            error: function(response) {
                $('#errors').html(response.responseText);
                form.trigger('reset');
            }
        });
    });
});