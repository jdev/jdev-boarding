var intervalAllPointer;
var today=new Date();
var annee_jdev = today.getFullYear();


$(document).ready(function() {

    $( "#datepicker_badge" ).datepicker({
        showAnim: "slideDown",
        dateFormat: 'dd/mm/yy'
    });

    // GenrateBadgeList avec datepicker
    $('#generateBadgeList').on('click', function(e) {
        e.preventDefault();
        
        var role=$( "#badge_role option:selected" ).val();

        var href = $(this).attr('href')+'&role='+role;
        console.log(href);

        var datepicker = $('#datepicker_badge').val();

        if(datepicker != '') {
            var a = datepicker.split('/');
            var date = a[2] + '-' + a[1] + '-' + a[0];
            window.location = href + '&date=' + date;
        } else {
            window.location = href;
        }
    });


    //$('[data-toggle="tooltip"]').tooltip();
    $("[data-toggle=tooltip]").tooltip();
    


    
    // ======================================================================
    // Validation d'un participants Organisateur, Invité, Sponsor, Exposant
    // ======================================================================
      
    $('.admin-valid-btn').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la validation du participant en tant que organisateur ?")) {
            var href = $(this).attr('href');
            $.ajax({
                url: href,
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('Problème serveur !');
                }
            });
        }
    });   
    

    
    $( "#admin_home" ).tabs({
        collapsible: true,
        activate: function (event, ui) {
            window.location.hash = 'tab=' + $(this).tabs('option', 'active');
        },
        create: function () {
            if (window.location.hash) {
                var active = parseInt(window.location.hash.replace('#tab=', ''));
                $(this).tabs("option", "active", active);
            }
        }
    });
    
    $("#admin_home_loading").hide();
    $("#admin_home").show();
      
      
    $('#Torganisateurs').DataTable( {
        dom: 'ilfrtp',
        pageLength: 25
    });
    $('#Tinvites').DataTable( {
        dom: 'ilfrtp'
    });    
    $('#Tsponsors').DataTable( {
        dom: 'ilfrtp'
    });        
    $('#Texposants').DataTable( {
        dom: 'ilfrtp'
    });  
    $('#Taccompagnants').DataTable( {
        dom: 'ilfrtp'
    });      
    

    // ======================================================================
    // Formulaire de gestion des participants
    // ======================================================================


    $( "#tabs_participants" ).tabs({
        collapsible: true,
        activate: function (event, ui) {
            window.location.hash = 'tab=' + $(this).tabs('option', 'active');
        },
        create: function () {
            if (window.location.hash) {
                var active = parseInt(window.location.hash.replace('#tab=', ''));
                $(this).tabs("option", "active", active);
            }
        }
    });

    $("#tabs_participants_loading").hide();
    $("#tabs_participants").show();

    
    // Suppression d'un participant 
    $('.del-participant').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la suppression du participant ?")) {
            var href = $(this).attr('href');
            $.ajax({
                url: href,
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('Problème serveur !');
                }
            });
        }
    });
    
    // formulaire edition d'un participant
    $('#form_admin_participant').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: 'PUT',
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-admin-participants').attr('href');
                },
                error: function() {
                    alert('Pb rencontré');
                    form.trigger('reset');
                }
            });
        }
    });
            
    $('#tab-admin-participants thead th').each( function () {
        var title = $(this).text();
        if ( (title === 'Participant') || (title === 'Email') || (title === 'Role')){
        $(this).html('<br><input style="width: 100%;" type="text" placeholder="Search" /><br>' + title);
    }
    } );
    
    var tab_admin_participants = $('#tab-admin-participants').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ participants / page",
            //"info": "Showing page _PAGE_ of _PAGES_",
            "info": "Nombre d'inscrits sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ Inscrits au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Exporter en Excel',filename:'jdev-inscrits',
                exportOptions: {
                    columns: [ 0, 2, 3],
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (data.indexOf('fa-check-circle-o') != -1){
                                data='-';                               
                            } 
                            if (data.indexOf('fa-credit-card') != -1){
                                data='pb';                               
                            }
                            return data;
                        }
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Feuille d\'émargement',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev-emargement-' + annee_jdev, 
                customize: function ( doc ) {
                    doc.defaultStyle.fontSize = 12;
                    doc.defaultStyle.lineHeight = 2;
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {
                    columns: [ 0, 2, 3, 7 ],
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (data.indexOf('present=false') !== -1){
                                data='x';                               
                            } 
                            if (data.indexOf('present=true') !== -1){
                                data='';                               
                            }
                            if (data.indexOf('fa-check-circle-o') !== -1){
                                data='-';                               
                            } 
                            if (data.indexOf('fa-credit-card') !== -1){
                                data='pb';                               
                            }
                            return data;
                        }
                    }
                }                
            }    
        ]
    }); 
    
    tab_admin_participants.columns().every( function () {
        var that = this;
        
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    
    
    $('#tab-liste-inscrits').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ inscrits / page",
            //"info": "Showing page _PAGE_ of _PAGES_",
            "info": "Nombre d'inscrits sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ inscrits au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-inscrits',
                exportOptions: {
                    columns: [ 0, 1, 2, 3],
                    format: {
                        body: function ( data) { 
                            if (data.indexOf('fa-check-circle-o') !== -1){
                                data='-';                               
                            } 
                            if (data.indexOf('fa-credit-card') !== -1){
                                data='pb';                               
                            }
                            return data;
                        }
                    }
                }
            }
        ]
    });  
    
    $('#tab-liste-nonactif').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ non actif / page",
            //"info": "Showing page _PAGE_ of _PAGES_",
            "info": "Nombre de compte non actif sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ comptes non actifs au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-nonactifs',
                exportOptions: {
                    columns: [ 0, 1, 2]
                }}
        ]
    });                
    $('#tab-liste-participants-P').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ participants / page",
            //"info": "Showing page _PAGE_ of _PAGES_",
            "info": "Nombre de participants sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ participants au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-participants',
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            data=data.replace(/<i class="fa fa-check-circle-o"><\/i>/g,'x -');
                            data=data.replace(/<i class="fa fa-credit-card"><\/i>/g,'pb -');

                            return data;
                        }
                    }
                }
            }
        ]
    });
    $('#tab-liste-participants-O').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ organisateurs / page",
            "info": "Nombre d'organisateurs sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ organisateurs au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-organisateurs',
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            data=data.replace(/<i class="fa fa-check-circle-o"><\/i>/g,'x');
                            data=data.replace(/<i class="fa fa-pause"><\/i>/g,'pb');
                            return data;
                        }
                    }
                }
            }
        ]
    });    
    $('#tab-liste-participants-I').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ invités / page",
            "info": "Nombre d'invités sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ invités au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-invites',
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            data=data.replace(/<i class="fa fa-check-circle-o"><\/i>/g,'x');
                            data=data.replace(/<i class="fa fa-pause"><\/i>/g,'pb');
                            return data;
                        }
                    }
                }
            }
        ]
    });     
    $('#tab-liste-participants-S').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ sponsors / page",
            "info": "Nombre de sponsors sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ sponsors au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-sponsors',
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            data=data.replace(/<i class="fa fa-check-circle-o"><\/i>/g,'x');
                            data=data.replace(/<i class="fa fa-pause"><\/i>/g,'pb');
                            return data;
                        }
                    }
                }
            }
        ]
    }); 
    $('#tab-liste-participants-E').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ exposants / page",
            "info": "Nombre d'exposants sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ exposants au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-exposants',
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            data=data.replace(/<i class="fa fa-check-circle-o"><\/i>/g,'x');
                            data=data.replace(/<i class="fa fa-pause"><\/i>/g,'pb');
                            return data;
                        }
                    }
                }
            }
        ]
    }); 
    $('#tab-liste-participants-A').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ accompagnants / page",
            "info": "Nombre d'accompagnants sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ accompagnants au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {extend: 'excel',text: 'Sauver au format Excel',filename:'jdev-accompagnants',
                exportOptions: {
                    format: {
                        body: function ( data, row, column, node ) { 
                            data=data.replace(/<i class="fa fa-check-circle-o"><\/i>/g,'x');
                            data=data.replace(/<i class="fa fa-credit-card"><\/i>/g,'pb');
                            return data;
                        }
                    }
                }
            }
        ]
    }); 
    
    
    // ======================================================================
    // gestion des paiements des participants
    // ======================================================================

    $('.admin-invalid-btn').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous l'invalidation de la place Pré-Payée du participant ?")) {
            var href = $(this).attr('href');
            $.ajax({
                url: href,
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('Problème serveur !');
                }
            });
        }
    });
    
    $('.admin-valid-paiment-btn').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la validation du paiement du participant ?")) {
            var href = $(this).attr('href');
            $.ajax({
                url: href,
                type: 'POST',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('Problème serveur !');
                }
            });
        }
    });

    $('#tparticipants').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ participants / page",
            //"info": "Showing page _PAGE_ of _PAGES_",
            "info": "Nombre de participants sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ participants au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {
                extend: 'excel',
                text: 'Sauver au format Excel',
                filename:'jdev-paiements-participants', 
                exportOptions: {
                    format: {
                    body: function ( data, row, column, node ) {                   
                        if (column === 7){
                            data=data.replace('<i class="fa fa-check-circle-o"></i>','x' );  
                            if (data.indexOf('admin-valid-paiement') !== -1){
                                data='';                               
                            }
                        }
                        if (column === 8){ 
                            if (data.indexOf('Pre-payée') !== -1){
                                data='Pre-payée';                               
                            }
                        }
                        return data;
                        
                        }
                    }
                }                
            }            
        ]
    } );


    
    $("#tabs_paiement_loading").hide();
    $("#tabs_paiement").show();
    
   
    
    
    // ======================================================================
    // Gestion de l'Emargement
    // ======================================================================
    
                
    $('.btn-admin-present').on('click', function(e) {
        e.preventDefault();
         if (confirm("Confirmez-vous la présence de ce participant ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'GET',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La mise à jour n\'a pas pu être effectuée');
                }
            });
        }       
    });
        
    $('.btn-admin-nopresent').on('click', function(e) {
        e.preventDefault();
         if (confirm("Confirmez-vous l'invalidation de la présence de ce participant ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'GET',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La mise à jour n\'a pas pu être effectuée');
                }
            });
        }       
    });    
        
    var tab_jdevs = $('#tab-jdev').DataTable( {
        "language": {
            "lengthMenu": "Visualiser _MENU_ inscrits / page",
            //"info": "Showing page _PAGE_ of _PAGES_",
            "info": "Nombre d'inscrits sélectionnés : <b>_TOTAL_</b>",
            "infoFiltered": "<i>(sur  _MAX_ Inscrits au total)</i>"
        },
        dom: 'ilfBrtp',
        buttons: [
            {   
                extend: 'excel',
                text: 'Exporter en Excel',
                filename:'jdev-inscrits-'+annee_jdev,
                exportOptions: {
                    columns: [ 0, 2, 3],
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (data.indexOf('present=false') !== -1){
                                data='x';                               
                            } 
                            if (data.indexOf('present=true') !== -1){
                                data='';                               
                            }
                            if (data.indexOf('fa-check-circle-o') != -1){
                                data='-';                               
                            } 
                            if (data.indexOf('fa-credit-card') != -1){
                                data='pb';                               
                            }
                            return data;
                        }
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Feuille d\'emargement',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev-emargement-'+annee_jdev, 
                customize: function ( doc ) {
                    doc.defaultStyle.fontSize = 12;
                    doc.defaultStyle.lineHeight = 2;
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {
                    columns: [ 0, 2, 3],
                    format: {
                        body: function (data) { 
                            if (data.indexOf('present=false') !== -1){
                                data='x';                               
                            } 
                            if (data.indexOf('present=true') !== -1){
                                data='';                               
                            }
                            if (data.indexOf('fa-check-circle-o') !== -1){
                                data='-';                               
                            } 
                            if (data.indexOf('fa-credit-card') !== -1){
                                data='pb';                               
                            }
                            return data;
                        }
                    }
                }                
            }    
        ]
    }); 
    
    tab_jdevs.columns().every( function () {
        var that = this;
        
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    
    
    
    // ======================================================================
    // Formulaire de gestion d'un organisme
    // ======================================================================

    $('#form_organisme').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var organisme_id = $('#organisme_id').val();
            if (organisme_id > 0) {
                var vtype = 'PUT';
            } else {
                var vtype = 'POST';
            }
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-organismes').attr('href');
                },
                error: function() {
                    alert('L\'organisme n\'a pas pu être ajouté');
                    form.trigger('reset');
                }
            });
        }
    });

    var nbPlaces = $('#nb_places').val();
    var nbLibres = $('#nb_libres').val();

    $('#nb_places').on('keyup', function(e) {
        var newNbPlaces = $(this).val();
        var diff = newNbPlaces - nbPlaces;
        $('#nb_libres').val(parseInt(nbLibres) + diff);
    });

    $('.btn-delete-organisme').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous la suppression de cet organisme ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('L\'organisme n\'a pas pu être supprimé');
                }
            });
        }
    });



    // ======================================================================
    // Formulaire de gestion d'une thematique
    // ======================================================================

    $('#form_add_edit_thematique').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var thematique_id = $('#thematique_id').val();
            if (thematique_id > -1) {
                var vtype = 'PUT';
            } else {
                var vtype = 'POST';
            }
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-thematiques').attr('href');
                },
                error: function() {
                    alert('La thématique n\'a pas pu être ajoutée ou modifiée');
                    form.trigger('reset');
                }
            });
        }
    });

    $('.btn-delete-thematique').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous la suppression de cette thematique ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La thématique n\'a pas pu être supprimée');
                }
            });
        }
    });
    
    $('#tab_thematique').DataTable( {
        dom: 'Bfrtilp',
        buttons: [
            {extend: 'excel',text: 'Save as Excel',filename:'jdev-thematiques'}
        ]
    });
//
//
//    $("#tabs_thematiques_loading").hide();
//    $("#tabs_thematiques").show();



    // ======================================================================
    // Formulaire de gestion d'une salle
    // ======================================================================

    $('#form_add_edit_salle').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var salle_id = $('#salle_id').val();
            if (salle_id > 0) {
                var vtype = 'PUT';
            } else {
                var vtype = 'POST';
            }
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-salles').attr('href');
                },
                error: function() {
                    alert('La salle n\'a pas pu être ajoutée');
                    form.trigger('reset');
                }
            });
        }
    });

    $('.btn-delete-salle').on('click', function(e) {
        e.preventDefault();
        
        if (confirm("Confirmez-vous la suppression de cette salle ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La salle n\'a pas pu être supprimée');
                }
            });
        }
    });
    
    $('#tab_salle').DataTable( {
        dom: 'Bfrtilp',
        buttons: [
            {extend: 'excel',text: 'Save as Excel',filename:'jdev-salles'}
        ]
    });


    $("#tabs_salles_loading").hide();
    $("#tabs_salles").show();

    // ======================================================================
    // Formulaire de gestion des Roles
    // ======================================================================
    //$( "#tabs_roles" ).tabs();
    $( "#accordion_roles" ).accordion({
      heightStyle: "content"
    });

    $( "#tabs_roles" ).tabs({
      collapsible: true
    });
    
    $('#form_role').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var role_id = $('#role_id').val();
            if (role_id > 0) {
                var vtype = 'PUT';
            } else {
                var vtype = 'POST';
            }
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-roles').attr('href');
                },
                error: function() {
                    alert('La rôle n\'a pas pu être ajouté');
                    form.trigger('reset');
                }
            });
        }
    });

    $('.btn-delete-role').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la suppression de cet rôle ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La rôle n\'a pas pu être supprimé');
                }
            });
        }
    });
    
    $('.btn-choix-role').on('click', function(e) {
        e.preventDefault();
         if (confirm("Confirmez-vous le choix de ce rôle ?")) {
            //console.log($(this).attr('href')+'&role_id='+$('#role', $(this).parent()).val());
            $.ajax({
                url: $(this).attr('href')+'&role_si='+$('#droit', $(this).parent()).val()+'&role_orga='+$('#role', $(this).parent()).val(),
                type: 'GET',
                success: function(text) {
                    //console.log(text);
                    location.reload();
                },
                error: function() {
                    alert('La rôle n\'a pas pu être modifié');
                }
            });
        }       
    });
    
    $('#tab_role').DataTable( {
        dom: 'tp'
    });
    
    $('#tab_organisateurs').DataTable( {
        dom: 'lfrtip',
    });   

    $("#tabs_roles_loading").hide();
    $("#tabs_roles").show();
    

    // ======================================================================
    // Formulaire de gestion des Formations
    // ======================================================================
    var form_them='';
    var form_type='';
    var form_label='';
    $('#thematique').change(function() {
        form_them = $( "#thematique option:selected" ).text();
        form_them=form_them.slice(0,2);
        form_them=form_them.toLowerCase();
        
        form_type = $( "#type option:selected" ).text();
        form_type=form_type.slice(0,2);
        form_type=form_type.replace(/ /g,'' );
        
        form_label=form_them+'.'+form_type;
        $("#nom_val").val(form_label);
    });
    $('#type').change(function() {
        form_them = $( "#thematique option:selected" ).text();
        form_them=form_them.slice(0,2);
        form_them=form_them.toLowerCase();

        form_type = $( "#type option:selected" ).text();
        form_type=form_type.slice(0,2);
        form_type=form_type.replace(/ /g,'' );
        
        form_label=form_them+'.'+form_type;
        $("#nom_val").val(form_label);
    });
    
    $('#intervenant_plus').click(function() {
        $('#nb_intervenant').val(function(i, oldval) { return ++oldval;});
        $('#intervenant_1').clone().prop("selectedIndex", 0).attr('id','intervenant_'+$('#nb_intervenant').val()).appendTo('#liste_intervenant');
        console.log($('#nb_intervenant').val());
    });    
    
    $('#intervenant_moins').click(function() {
        if ( $('#nb_intervenant').val() > 1){
            $('#intervenant_'+$('#nb_intervenant').val()).remove(); 
            $('#nb_intervenant').val(function(i, oldval) { return --oldval;});
        }
        console.log($('#nb_intervenant').val());
    });   
    
    $('#form_formation').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var formation_id = $('#formation_id').val();
            var duplicate = $('#duplicate').val();
            
            $('#nom').val($('#nom_val').val()+$('#nom_num').val());  
            var liste_intervenant=$('#intervenant_1 option:selected').val();
            if ($('#nb_intervenant').val() > 1){
                for (i=2;i<=$('#nb_intervenant').val();i++){ 
                    liste_intervenant+=','+$('#intervenant_'+i+' option:selected').val();
                }
            }
            $('#intervenant').val(liste_intervenant);
            
            if (formation_id > 0 && !duplicate) {
                var vtype = 'PUT';
            } else {
                var vtype = 'POST';
            }
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-formations').attr('href');
                },
                error: function() {
                    alert('La formation n\'a pas pu être ajoutée');
                    form.trigger('reset');
                }
            });
        }
    });

    $('.btn-delete-formation').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la suppression de cette formation ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La formation n\'a pas pu être supprimée');
                }
            });
        }
    });
    
    $('#tab_formation').DataTable( {
        dom: 'Bfrtilp',
        buttons: [
            {extend: 'excel',text: 'Save as Excel',filename:'jdev-formations'}
        ]
    });

    $("#tabs_formations_loading").hide();
    $("#tabs_formations").show();
    
    
    // ======================================================================
    // Formulaire de gestion des Pré-Agendas
    // ======================================================================

    $( "#tabs_preagenda" ).tabs({
      collapsible: true
    });

    $('#form_preagenda').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var preagenda_id = $('#preagenda_id').val();
            if (preagenda_id > 0) {
                var vtype = 'PUT';
            } else {
                var vtype = 'POST';
            }
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-preagendas').attr('href');
                },
                error: function() {
                    alert('Le pré-agenda n\'a pas pu être ajouté');
                    form.trigger('reset');
                }
            });
        }
    });

    $('.btn-delete-preagenda').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la suppression de ce pré-agenda ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('Le pré-agenda n\'a pas pu être supprimé');
                }
            });
        }
    });
    
    $('#tab_preagendas').DataTable( {
        dom: 'Bfrtilp',
        buttons: [
            {extend: 'excel',text: 'Save as Excel',filename:'jdev-preagenda'}
        ],   
        "columnDefs":[
            { 
            "targets" : 0,
                "render": function (data) {					       
                    return "<b><a href=\"http://devlog.cnrs.fr/jdev" + annee_jdev + "/" + data + "\" target=\"_blank\" style=\"color:#337ab7;\"><i class=\"fa fa-external-link\"></i> " + data.toUpperCase()+"</a></b>";
                }
            }    
        ]   
    });
    
    $('#tab_preagenda_visu').DataTable( {
        dom: 'Brt',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Exporter en PDF',
                orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev-preagenda-' + annee_jdev, 
                customize: function ( doc ) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {
                    format: {
                        body: function (data) {                             
                            var formation=data.split('<br>">');
                            var djournee='';

                            for (i=1;i<formation.length;i++){
                                var label=formation[i].split('</a>'); 
                                label[0]=label[0].replace('<i class="fa fa-user" aria-hidden="true"></i>','p' );
                                djournee+=label[0]+'\n';
                            }
                            if ( (djournee.length !== 0) && (djournee.charAt(0) === 't') ) { 
                                data=djournee; 
                            }                        
                            return data;                        
                        }
                    }
                }                
            }            
        ],      
        "ordering": false
    });
    
    
    // ======================================================================
    // Formulaire de gestion des emargements
    // ======================================================================
      
    $('.btn-em-present').on('click', function(e) {
        e.preventDefault();
         if (confirm("Confirmez-vous la présence de ce participant ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'GET',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La mise à jour n\'a pas pu être effectuée');
                }
            });
        }       
    });
        
    $('.btn-em-nopresent').on('click', function(e) {
        e.preventDefault();
         if (confirm("Confirmez-vous l'invalidation de la présence de ce participant ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'GET',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La mise à jour n\'a pas pu être effectuée');
                }
            });
        }       
    });    
    
     $('.btn-tous-present').on('click', function(e) {
        e.preventDefault();
        if (confirm("Confirmez-vous la présence de tous ces participants ?")) {
            var participants_id = [];
            $.each($("input[name='chbx_presence']:checked"), function(){ 
                participants_id.push($(this).val());
            });            
            $.ajax({
                url: $(this).attr('href')+'&participants='+participants_id.join(","),
                type: 'GET',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('La mise à jour n\'a pas pu être effectuée');
                }
            });
        }       
    });   
    
    $('#tab-inscrits-session').DataTable( {
        "pageLength": 100,
        dom: 'Bflrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Feuille d\'émargement',
                title: $('#form-id').val() + '\n Intervenant: ' + $('#intervenant').val() + '\n Salle : ' + $('#salle').val() + ', Date: ' + $('#horaire').val() + ' \n Code formation :' + $('#form-code').val() + '\n ',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev-emargement-' + $('#form-id').val(), 
                customize: function ( doc ) {
                    doc.defaultStyle.fontSize = 12;
                    doc.defaultStyle.lineHeight = 2;
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split(''); 
                },
                exportOptions: {
                    columns: [ 0, 2, 3, 4 ],
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (data.indexOf('present=false') != -1){
                                data='x';                               
                            } 
                            if (data.indexOf('present=true') != -1){
                                data='';                               
                            }
                            return data;
                        }
                    }
                }                
            }            
        ],      
        "ordering": true
    }); 

    $("#tabs_preagenda_loading").hide();
    $("#tabs_preagenda").show();
    
    
  
    $('#em-thematique').change(function() {
        var thematique = $(this).val();
        var letoken=$("#token").val();
        $('#em-formation option').remove();

        $.ajax({
            url: 'dashboard-admin-emargement?token='+letoken+'&thematique_id='+thematique,
            type: 'GET',
            success: function(formations) {
                    $('#em-formation').append('<option>sélectionnez une formation</option>');
                    $.each(formations, function (key, val) {
                        $('#em-formation').append($('<option></option>', {
                            value: val['id'],
                            text: val['nom']+'('+val['id']+')'
                        }));
                    });
                $('#em-formation').attr('disabled', false);
            },
            error: function() {
                alert('PB recupération des formations');
            }
        });
      
    });
    
    $('#em-formation').change(function() {
        var formation = $(this).val();
        var letoken=$("#token").val();

        $.ajax({
            url: 'dashboard-admin-emargement?token='+letoken+'&formation_id='+formation,
            type: 'GET',
            success: function(data) {
                document.location.href = 'dashboard-admin-emargement?token='+letoken+'&formation_id='+formation;
            },
            error: function() {
                alert('PB recupérartion des participants');
            }
        }); 
    });   


        
    
    $('#tab-emargement-participants').DataTable( {
        dom: 'Brt',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Exporter en PDF',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev-emargement-'+$('#form-id').val(), 
                customize: function ( doc ) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {
                    columns: [ 0, 2, 3, 5 ],
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (data.indexOf('present=false') != -1){
                                data='x';                               
                            } 
                            if (data.indexOf('present=true') != -1){
                                data='';                               
                            }
                            return data;
                        }
                    }
                }                
            }            
        ],      
        "ordering": false
    }); 
 

    // ======================================================================
    // Gestion de l'Agenda
    // ======================================================================
    

    // Suppression d'une session de formation
    $('#annuler-formation').on('click', function(e) {
        if(!confirm('Souhaitez-vous vraiment supprimer cette session de formation ?')){
            e.preventDefault();
        }     
    });

    $('.btn-delete-agenda').on('click', function(e) {
        e.preventDefault();

        if (confirm("Confirmez-vous la suppression de cet agenda ?")) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE',
                success: function(text) {
                    location.reload();
                },
                error: function() {
                    alert('L\'agenda n\'a pas pu être supprimé');
                }
            });
        }
    });
    
    // -------------------------------------------------------
    // Formulaire d'ajout / edition d'une session de formation
    // -------------------------------------------------------
    
    // gestion du display d'un agenda
    $('#form_agenda #formation').change(function() {
        var formation=$('#form_agenda #formation option:selected').text();
        if (formation != ''){
            var label=formation.split('('); 
            var type=label[1].split(' /'); 
            if (type[0] === 'Plénière'){ $('#form_agenda #ordre').val('10');}
            if (type[0] === 'Atelier'){ $('#form_agenda #ordre').val('20');}
            if (type[0] === 'Atelier Préparatoire'){ $('#form_agenda #ordre').val('30');}
            if (type[0] === 'Groupe de Travail'){ $('#form_agenda #ordre').val('40');}
            
            type[1]=type[1].replace('participants)','');
            type[1]=type[1].replace(/\n/g,'' );
            type[1]=type[1].replace(/ /g,'' );
            $('#form_agenda #quota').val(type[1]);
        }
        else { 
            $('#form_agenda #ordre').val('');
            $('#form_agenda #quota').val('');
        }
    });   

    var changeTimepickerFin = function (){
        //console.log($('#debut-timepicker').val());
        //console.log($('#fin-timepicker').val());
        //console.log($('#datepicker').val());
        if ($('#debut-timepicker').val() !== '') {
            $('#fin-timepicker').timepicker('option', 'minTime', $('#debut-timepicker').val());
        }
    } 
    
    $( "#datepicker" ).datepicker({
        showAnim: "slideDown",
        minDate: new Date($("#date_debut").val()),
        maxDate: new Date($("#date_fin").val()),
        dateFormat: 'dd/mm/yy'
    });
    $('#debut-timepicker').timepicker({
        'timeFormat': 'H:i',
        'minTime': '8:00',
        'maxTime': '18:00'
    });
    $('#fin-timepicker').timepicker({
        'timeFormat': 'H:i',
        'minTime': '8:00',
        'maxTime': '19:00'
    });

    changeTimepickerFin();

    $('#debut-timepicker').on('change', function () {
        $('#fin-timepicker').val('');
        changeTimepickerFin();
    });
    
    $('#datepicker,#fin-timepicker,#debut-timepicker').on('change', function () {     
        if ( ($('#debut-timepicker').val() !== '') && ($('#fin-timepicker').val() !== '') && ($('#datepicker').val() !== '')){            
            $.ajax({
                //url: "dashboard-admin-agenda-form?token="+$("#token").val()+"&agenda_id="+$('#agenda_id').val()+"&quota="+$('#quota').val()+"&jour="+$('#datepicker').val()+"&debut="+$('#debut-timepicker').val()+"&fin="+$('#fin-timepicker').val(),
                url: "dashboard-admin-agenda-form?token="+$("#token").val()+"&quota="+$('#quota').val()+"&jour="+$('#datepicker').val()+"&debut="+$('#debut-timepicker').val()+"&fin="+$('#fin-timepicker').val(),
                type: 'GET',
                success: function(salles) {
                    $('#salle option').remove();

                    var val_proj=0;
                    //console.log(salles);
                    $.each(salles, function (key, val) {
                        val_proj=val['videoprojecteur']? 1 : 0;
                        $('#salle').append($('<option></option>', {
                            value: val['id'],
                            text: val['nom']+' (capacité: '+val['quotaPhysique']+', wifi: '+val['wifi']+', nb reseau fil: '+val['reseau']+', vidéopro: '+val_proj+')'
                        }));
                    });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    //console.log(xhr.responseText);
                    alert('PB recupérartion des salles');
                }
            });     
        }
    });  

    // Validation du formulaire
    $('#form_agenda').validator().on('submit', function(e) {
        if (e.isDefaultPrevented())
        {
            alert('Veuillez remplir tous les champs obligatoires');
        }
        else
        {
            e.preventDefault();
            var agenda_id = $('#agenda_id').val();
            var vtype = 'POST';
            if (agenda_id > 0) {
                vtype = 'PUT';
            }
            //else {
            //    var vtype = 'POST';
            //}
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: vtype,
                data: form.serialize(),
                success: function(text) {
                    document.location.href = $('#btn-back-agendas').attr('href');
                },
                error: function() {
                    if (vtype == 'POST'){
                    alert('L\'agenda n\'a pas pu être ajouté');
                    }
                    else{
                    alert('L\'agenda n\'a pas pu être modifié');
                    }                    
                    form.trigger('reset');
                }
            });
        }
    });
    
    
    // DataTable de gestion de l'agenda par 1/ journée
    $('#tab_agendas').DataTable( {
        dom: 'Bfrtilp',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Exporter en PDF',
                title: 'Agenda JDEVs '+$("#day").val(),
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev' + annee_jdev + '-agenda' + $("#day").val(),
                customize: function ( doc ) {
                    doc.defaultStyle.fontSize = 10;
                    doc.defaultStyle.lineHeight = 1;
                    doc.styles.tableHeader.alignment = 'left';
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');                  
                },              
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 5, 6 ],
                    format: {
                        body: function ( data, row, column, node ) { 
                            if (column == 0){
                                var formation=data.split('</i>');
                                formation[1]=formation[1].replace('</a></b>','' );
                                data=formation[1];
                                console.log(data);
                            } 
                            if (column == 3){
                                data=data.replace('<span style="display:none">','' );
                                data=data.replace('</span>','' );
                                var salle=data.split('<select name="salle"');
                                data=salle[0];                               
                                console.log(data);
                            }                             
                            return data;                        
                        }
                    }
                }                
            }            
        ],      
        "ordering": false,
        "columnDefs":[
            { 
            "targets" : 0,
                "render": function (data) {					       
                    return "<b><a href=\"http://devlog.cnrs.fr/jdev" + annee_jdev + "/" + data + "\" target=\"_blank\" style=\"color:#337ab7;\"><i class=\"fa fa-external-link\"></i> " + data.toUpperCase()+"</a></b>";
                }
            }    
        ]   
    });
            
            
    // Affichage des sessions d'une thématique donnée
    $(':checkbox').click(function() {
        for (var i=1; i<=$('#nbThem').val(); i++) {   
            if ($('#chkbox_t' + i).is(':checked')) {
                $('span.t-t' + i).show(); 
            }
            else {
                $('span.t-t' + i).hide();
            }
        }
    });
   
    // DataTable de visualisation complète de l'agenda    
    $('#tab_agenda_visu').DataTable( {
        dom: 'Brt',
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Exporter en PDF',
                //orientation: 'landscape',
                //download: 'open',
                pageSize: 'A4',
                header: true,
                filename:'jdev-agenda-' + annee_jdev, 
                customize: function ( doc ) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },
                exportOptions: {

                    format: {
                        body: function (data) { 
                            if ( (data !== 'Matin') &&  (data !== 'Après-Midi') &&  (data.length !== 0)){
                                
//                                data=data.replace(/<a href="http:\/\/devlog.cnrs.fr/g,'' );
//                                data=data.replace(/<\/a>/g,'' );

                                var formation=data.split('title="<b>');
                                var djournee='';

                                for (i=1;i<formation.length;i++){                                        
                                    var data1=formation[i].split('<i class="fa fa-user"');
                                    var data2=data1[0].split(' (<span');
                                    var data3=data2[0].split('">t');
                                    var label=data3[0].split('</b><p>');
                                    
                                    
                                    // verification que la thématique est selectionnée (checkbox)
                                    for (var nb=0; nb<=$('#nbThem').val(); nb++) {   
                                        if ($('#chkbox_t' + nb).is(':checked')) {
                                            if (label[0][1] == nb){                                                 
                                                // ajout des infos sur la formation (intitulé, ...)
                                                if (label[1].length !== 0){
                                                    label[1]=label[1].replace(/<b>/g,'' );
                                                    label[1]=label[1].replace(/<\/b>/g,'' );
                                                    label[1]=label[1].replace(/<br>/g,'\n');
                                                    //console.log (label[0]+'\n'+label[1]);
                                                }                                                
                                                djournee+=label[0].toUpperCase()+'\n'+label[1]+'\n\n';
                                            }
                                        }
                                    }                                 
                                }                              
                                
                                // gestion de l'affichage si 0 session sur 1/2 journée
                                if (djournee.length === 0){ 
                                    djournee='-' + '\n';
                                }
                                
                                data=djournee; 
                            }
                                                  
                            return data;                                            
                        }
                    }
                }                
            }            
        ],      
        "ordering": false
    });               

            
    // gestion des onglets le la page Agenda   
    $( "#tabs_agenda" ).tabs({
      collapsible: true
    });    
    $("#tabs_agenda_loading").hide();
    $("#tabs_agenda").show();
    
    var tab_admin_agendas = $('#tabs_agenda_visu');
    if (tab_admin_agendas) {
        intervalAllPointer = setInterval(refreshAllSessions, 3000);
    }
    
});



var refreshAllSessions = function(token) {
    var token = $('#tab_agenda_visu').data('token');

    if (!token) {
        console.log('Manque le token pour lancer le refresh Agenda');
        clearInterval(intervalAllPointer);
        return ;
    }

    $.ajax({
        url: 'dashboard-participant-sessions-refresh?token=' + token,
        type: 'GET',
        success: function(data) {
            $('#tot_inscrits').text(data.tot_inscrits);
            data.agendas.forEach(function(agenda) {
                $('#nb_inscrits_' + agenda.id).text(agenda.nb_inscrits);
                var prct = Math.round(((agenda.nb_inscrits / agenda.quota) * 100));
                var badge = '';
                if (prct <= 50) {
                    badge = 'vert';
                } else if (prct > 50 && prct <= 75) {
                    badge = 'orange';
                } else {
                    badge = 'rouge';
                }
                $('#nb_inscrits_prct_' + agenda.id).attr('class', 'badge badge-' + badge);
                $('#nb_inscrits_prct_' + agenda.id).text(prct + '%');
                
                //$('#nb_inscrits_prct_' + agenda.id).html('<span class="badge badge-' + badge + '">' + prct + '%</span>');

            });
        },
        error: function() {
            console.log('PB refresh sessions');
            clearInterval(intervalAllPointer);
        }
    });
}