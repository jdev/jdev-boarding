$(document).ready(function() {
    $('#connexionForm').validator();

     $('#connexionForm').on('submit', function(e) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            url: 'connexion',
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            success: function(response) {
                switch (response.roleSI) {
                    case  'admin' : 
                        document.location.href = 'dashboard-admin?token=' + response.token;
                        break;
                    case 'clo_admin' :
                        document.location.href = 'dashboard-admin-participant-gestion?token=' + response.token;
                        break;
                    case 'clo_pgm' :
                        document.location.href = 'dashboard-admin-profil?token=' + response.token;
                        break;
                    case 'clo' :
                        document.location.href = 'dashboard-admin-profil?token=' + response.token;
                        break;                        
                    default :
                        document.location.href = 'dashboard-home?token=' + response.token;
                        break;
                }
            },
            error: function() {
                alert('Mauvais login et mot de passe!');
                form.trigger('reset');
            }
        });
     });
});

$(document).ready(function() {
    $('#change-passwdForm').validator();

    $('#change-passwdForm').on('submit', function(e) {
        e.preventDefault();

        var changePasswdForm = $(this);

        $.ajax({
            url: 'change-passwd',
            type: 'POST',
            data: changePasswdForm.serialize(),
            dataType: 'json',
            success: function(response) {
                alert('Mot de passe changé avec succès !');
                document.location.href = 'connexion';
            },
            error: function() {
                alert('Mauvais login et mot de passe!');
                changePasswdForm.trigger('reset');
            }
        });
    });
});