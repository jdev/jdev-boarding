FROM php:7.3-apache

# Install modules
RUN apt-get update \ 
    && apt-get install -y zlib1g zlib1g-dev libpq-dev libpq5 libzip-dev zip unzip libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd pgsql pdo_pgsql zip bcmath

RUN a2enmod rewrite

COPY . /srv/app
COPY ./conf-dev/vhost.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /srv/app

CMD ["apache2-foreground"]
